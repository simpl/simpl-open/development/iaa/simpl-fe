import { Route } from '@angular/router';

import { EchoResolver} from '../resolvers/app-resolver.service';
import { HasCredentialGuard } from '../guards/hasCredentialGuard.guard';
import { canActivateAuthRole } from '../guard/auth-guard';


export const remoteRoutes: Route[] = [
  {
    path: '',
    loadComponent: () => import('../echo/echo-tier-home/echo-tier-home.component').then((m) => m.EchoTierHomeComponent),
    canActivate: [canActivateAuthRole],
  },
  {
    path: 'echo',
    loadComponent: () => import('../echo/echo-tier/echo-tier.component').then((m) => m.EchoTierComponent),
    canActivate: [canActivateAuthRole],
    resolve: {
      echo: EchoResolver
    }
  },
  {
    path: 'agent-configuration',
    loadComponent: () => import('../agent-configuration/agent-configuration.component').then((m) => m.AgentConfigurationComponent),
    canActivate: [canActivateAuthRole],
    data: { roles: ["ONBOARDER_M"] },
  },
  {
    path: 'ping',
    loadComponent: () => import('../ping/ping.component').then((m) => m.PingComponent),
    canActivate: [canActivateAuthRole, HasCredentialGuard],
  },
  {
    path: "unauthorized",
    loadComponent: () =>
      import("@fe-simpl/landing-page").then((m) => m.UnauthorizedPageComponent)
  }
];
