import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AlertBannerComponent } from '@fe-simpl/alert-banner';
import { EuiAllModule } from '@eui/components';
import Keycloak from 'keycloak-js';

@Component({
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    AlertBannerComponent,
    EuiAllModule,
  ],
  selector: 'app-participant-utility-entry',
  template: `
      <lib-alert-banner class="position-fixed w-100 p-4"></lib-alert-banner>
      <eui-app>

        <eui-app-toolbar>
          <eui-toolbar>

            <eui-toolbar-logo logoUrl="assets/images/logo/simpl-logo.svg" logoHeight="60px" logoWidth="150px">
            </eui-toolbar-logo>

            <eui-toolbar-items euiPositionRight="">

              <eui-toolbar-item>
                <eui-user-profile>
                  <eui-user-profile-menu>
                    <eui-user-profile-menu-item class=" eui-u-p-s"
                                                (click)="logOut()"
                                                (keydown.enter)="logOut()">
                      <eui-icon-svg icon="eui-logout-thin" fillColor="danger" class="eui-u-mr-m"></eui-icon-svg>
                      <span euiLabel>Sign out</span>
                    </eui-user-profile-menu-item>
                  </eui-user-profile-menu>
                </eui-user-profile>
              </eui-toolbar-item>

            </eui-toolbar-items>

          </eui-toolbar>
        </eui-app-toolbar>

      </eui-app>
      <router-outlet></router-outlet>
  `
})
export class RemoteEntryComponent {
  loading = false;
  private readonly keycloak = inject(Keycloak);

  constructor() {
    if (this.keycloak.authenticated) {
      document.body.classList.add('simpl-splash-screen-hidden');
    }
  }

  logOut() {
    this.loading = true;

    const baseUrl = window.location.origin;
    const onboardingPath = window.location.pathname.includes(
      '/participant-utility'
    )
      ? '/participant-utility'
      : '';

    this.keycloak.logout({redirectUri:`${baseUrl}${onboardingPath}`}).then(() => {
      this.loading = false;
    });
  }
}
