import { render } from "@testing-library/angular";
import { AppDomain, translocoTestConfiguration } from "@test-environment";
import { EchoService } from "../echo.service";
import { EchoTierHomeComponent } from "./echo-tier-home.component";
import { TranslocoTestingModule } from "@jsverse/transloco";

jest.mock("../echo.service");

beforeEach(() => {
  jest.resetAllMocks();
});

async function renderComponent(role?: string) {
  return await render(EchoTierHomeComponent, {
    imports: [
      TranslocoTestingModule.forRoot(
        translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)
      ),
    ],
    providers: [
      EchoService,
    ],
  });
}

describe("EchoTierHomeComponent", () => {
  it("should render", async () => {
    await renderComponent("ONBOARDER_M");
  });
});
