import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslocoDirective } from '@jsverse/transloco';
import { RouterLink } from '@angular/router';
import { EuiButtonModule } from '@eui/components/eui-button';

@Component({
  selector: 'app-echo-tier-home',
  standalone: true,
  imports: [CommonModule, TranslocoDirective, RouterLink, EuiButtonModule],
  templateUrl: './echo-tier-home.component.html',
  styleUrl: './echo-tier-home.component.css',
})
export class EchoTierHomeComponent {}
