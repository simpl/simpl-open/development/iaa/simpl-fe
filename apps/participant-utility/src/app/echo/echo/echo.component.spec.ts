import { render, screen } from "@testing-library/angular";
import { AppDomain, translocoTestConfiguration } from "@test-environment";
import { EchoTResponseDTO } from "../echo.service";
import { EchoComponent } from "./echo.component";
import { TranslocoTestingModule } from "@jsverse/transloco";

const baseEchoRespone: EchoTResponseDTO = {
  id: "UNIT_12345",
  username: "t_foo",
  userRole: ["custom_role"],
  commonName: "foo_name",
  connectionStatus: "CONNECTED",
  mtlsStatus: "SECURED",
  email: "foo@email.com",
  userEmail: "fooSecond@email.com",
  participantType: "CONSUMER",
  organization: "Foo Organization",
  status: "status_OK",
  outcomeUserEmail: "faa_outcome@email.com",
  certificateId: "cert_id_00001",
  userIdentityAttributes: [
    "CodeIA_004",
    "CodeIA_005",
  ],
  identityAttributes: [
    {
      code: "ia_code",
      name: "ia_name",
      description: "first testing ia",
      assignableToRoles: true,
      enabled: true,
      participantTypes: ["CONSUMER"],
      used: true,
    },
  ],
};

describe("EchoComponent", () => {
  it("should create", async () => {
    const component = await render(EchoComponent, {
      imports: [
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)
        ),
      ],
    });
    expect(component).toBeTruthy();
  });

  it("should render echo respone", async () => {
    await render(EchoComponent, {
      imports: [
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)
        ),
      ],
      inputs: {
        echoResponse: {
          ...baseEchoRespone,
          ...{
            username: "one_bar_username",
            email: "one_bar@email.com",
          },
        },
      },
    });

    // Verify echoOneResponseData rendering data
    screen.getByText("one_bar_username");
    screen.getByText("one_bar@email.com");

    // Verify echoResponseData rendering data
    expect(screen.getByText("UNIT_12345")).toBeTruthy();

    screen.getByText("UNIT_12345");
    screen.getByText(/connected/i);
    screen.getByText(/secured/i);
    screen.getByText("CONSUMER");
    screen.getByText("Foo Organization");

    // Verfy render identityAttributes
    screen.getByText(/CodeIA_004/);
    screen.getByText(/CodeIA_005/);
    screen.getByText("ia_name");
  });
});
