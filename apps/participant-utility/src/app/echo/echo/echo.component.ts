import { Component, computed, input} from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslocoDirective } from '@jsverse/transloco';
import { EchoTResponseDTO } from '../echo.service';
import { RemoveUnderscorePipe } from '@fe-simpl/core/pipes';

@Component({
  selector: 'app-echo',
  standalone: true,
  imports: [CommonModule, TranslocoDirective, RemoveUnderscorePipe],
  templateUrl: './echo.component.html',
  styleUrl: './echo.component.css',
})
export class EchoComponent {

  echoResponse = input<EchoTResponseDTO | null>(null);

  userIdentityAttributes = computed<string | undefined>(() => {
    const response = this.echoResponse();
    return response?.userIdentityAttributes?.join(", ") ?? "";
  });
}
