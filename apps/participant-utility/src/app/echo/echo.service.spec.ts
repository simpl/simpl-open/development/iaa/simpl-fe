import { TestBed } from "@angular/core/testing";
import { ApiService } from "@fe-simpl/api";
import { EchoService } from "./echo.service";
import { of } from "rxjs";

const mockApiService = {
  get: jest.fn(),
  uploadFile: jest.fn(),
};

describe('EchoService', () => {

    var echoService: EchoService;

    beforeAll(() => {
      TestBed.configureTestingModule({
        providers: [
          EchoService,
          { provide: ApiService, useValue: mockApiService},
        ]
      });

      echoService = TestBed.inject(EchoService);

      jest.clearAllMocks();
    });

    it('testing method tier1', () => {
      const dto = {};
      mockApiService.get.mockReturnValue(of(dto));
      const resp = echoService.tier1();
      expect(resp).toBe(resp);
      expect(mockApiService.get.mock.lastCall[0]).toBe("/user-api/mtls/echo-t1");
    });

    it('testing method echo', () => {
      const dto = {};
      mockApiService.get.mockReturnValue(of(dto));
      const resp = echoService.echo();
      expect(resp).toBe(resp);
      expect(mockApiService.get.mock.lastCall[0]).toBe("/user-api/agent/echo");
    });

    it('testing method hasCredential', () => {
      const dto = true;
      mockApiService.get.mockReturnValue(of(dto));
      const resp = echoService.hasCredential();
      expect(resp).toBe(resp);
      expect(mockApiService.get.mock.lastCall[0]).toBe("/user-api/credential");
    });

    it('testing method loadCredential', () => {
      const dto = true;
      mockApiService.get.mockReturnValue(of(dto));
      const file = new File([''], "mock-file-test");
      const resp = echoService.loadCredential(file);
      expect(resp).toBe(resp);
      expect(mockApiService.uploadFile.mock.lastCall[0]).toBe("/user-api/credential");
      const uploadFileName = ((mockApiService.uploadFile.mock.lastCall[1] as FormData).get("file") as File).name;
      expect(uploadFileName).toBe("mock-file-test");

    });

    it('testing method triggerDownloadIdentityAttributeInParticipantCluster', () => {
      const dto = true;
      mockApiService.get.mockReturnValue(of(dto));
      const resp = echoService.triggerDownloadIdentityAttributeInParticipantCluster();
      expect(resp).toBe(resp);
      expect(mockApiService.get.mock.lastCall[0]).toBe("/user-api/agent/identity-attributes");
    });

});
