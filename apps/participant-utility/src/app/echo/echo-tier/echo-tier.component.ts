import {
  Component,
  OnInit,
  inject,
  signal,
} from "@angular/core";
import { CommonModule } from "@angular/common";
import { EchoTResponseDTO } from "../echo.service";
import { EchoComponent } from "../echo/echo.component";
import {
  FormsModule,
  ReactiveFormsModule,
} from "@angular/forms";
import { LoaderPageComponent } from "@fe-simpl/loader";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: "app-echo-tier",
  standalone: true,
  imports: [
    CommonModule,
    EchoComponent,
    FormsModule,
    ReactiveFormsModule,
    LoaderPageComponent,
  ],
  templateUrl: "./echo-tier.component.html",
  styleUrl: "./echo-tier.component.css",
})
export class EchoTierComponent implements OnInit {
  public echoResponse = signal<EchoTResponseDTO | null>(null);
  public loader = signal(true);
  private readonly route = inject(ActivatedRoute);

  ngOnInit() {

    if (this.route.snapshot.data['echo']) {
      this.echoResponse.set(this.route.snapshot.data['echo'])
    }
  }
}
