import { ComponentFixture } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { ActivatedRoute } from "@angular/router";
import { TranslocoTestingModule } from "@jsverse/transloco";
import { AppDomain, translocoTestConfiguration } from "@test-environment";
import { render } from "@testing-library/angular";
import { screen, waitFor } from "@testing-library/dom";
import { EchoService, EchoTResponseDTO } from "../echo.service";
import { EchoComponent } from "../echo/echo.component";
import {
    EchoTierComponent,
} from "./echo-tier.component";

jest.mock("../echo.service");
jest.mock("../echo/echo.component");

beforeEach(() => {
  jest.resetAllMocks();
});


function renderEchoTierComponent(role?: string, echoResponse?: EchoTResponseDTO) {
  return render(EchoTierComponent, {
    imports: [
      TranslocoTestingModule.forRoot(translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)),
    ],
    providers: [
      EchoService,
      { provide: ActivatedRoute, useValue: { snapshot: { data: { echo: echoResponse }}}},
    ],
  });
}

async function getEchoComponent(fixture: ComponentFixture<EchoTierComponent>): Promise<EchoComponent> {
  return waitFor(() => {
    const component = fixture.debugElement.query(By.directive(EchoComponent)).componentInstance ;
    expect(component).toBeTruthy();
    return component;
  });
}


function mockEchoResponse(): EchoTResponseDTO {
  return {
    id: "mock-simple-id",
  }
}

describe("EchoTierComponent", () => {
  it("should create", async () => {
    await renderEchoTierComponent("ONBOARDER_M", mockEchoResponse());
    await screen.findByTestId("mock-echo-component");
  });

  it("should load certificate story - show upload modal", async () => {
    const mockResponse = mockEchoResponse();
    const render = await renderEchoTierComponent("ONBOARDER_M", mockResponse);
    screen.findByTestId("upload-credential-section");
    const echoComponent = await getEchoComponent(render.fixture);
    expect(echoComponent.echoResponse).toEqual(mockResponse);
  });

  it("should load certificate story - show error modal", async () => {
    await renderEchoTierComponent();
    screen.findByText(/currently unable/i);
  });
});
