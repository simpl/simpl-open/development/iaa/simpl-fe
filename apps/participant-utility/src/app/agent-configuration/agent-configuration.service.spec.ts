import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import {
  AgentConfigurationService,
  KeyPair,
  CsrDetails,
} from './agent-configuration.service';
import { API_URL } from '@fe-simpl/api';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { AppDomain, translocoTestConfiguration } from '@test-environment';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('AgentConfigurationService', () => {
  let service: AgentConfigurationService;
  let httpMock: HttpTestingController;
  const mockApiUrl = 'https://mock-api-url.com';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)
        ),
        NoopAnimationsModule,
      ],
      providers: [
        AgentConfigurationService,
        { provide: API_URL, useValue: mockApiUrl },
      ],
    });

    service = TestBed.inject(AgentConfigurationService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return true if status is not 204', () => {
    service.hasKeypair().subscribe((result) => {
      expect(result).toBe(true);
    });

    const req = httpMock.expectOne(`${mockApiUrl}/auth-api/keypair`);
    expect(req.request.method).toBe('HEAD');
    req.flush({}, { status: 200, statusText: 'OK' });
  });

  it('should return false if status is 204', () => {
    service.hasKeypair().subscribe((result) => {
      expect(result).toBe(false);
    });

    const req = httpMock.expectOne(`${mockApiUrl}/auth-api/keypair`);
    expect(req.request.method).toBe('HEAD');
    req.flush({}, { status: 204, statusText: 'No Content' });
  });

  it('should call the correct API endpoint and return response', () => {
    const mockResponse = { success: true };

    service.generateKeypair().subscribe((response) => {
      expect(response).toEqual(mockResponse);
    });

    const req = httpMock.expectOne(`${mockApiUrl}/auth-api/keypair/generate`);
    expect(req.request.method).toBe('POST');
    req.flush(mockResponse);
  });

  it('should call the correct API endpoint', () => {
    const mockKeyPair: KeyPair = {
      privateKey: 'mock-private-key',
      publicKey: 'mock-public-key',
    };
    const mockResponse = { success: true };

    service.importKeyPair(mockKeyPair).subscribe((response) => {
      expect(response).toEqual(mockResponse);
    });

    const req = httpMock.expectOne(`${mockApiUrl}/auth-api/keypair`);
    expect(req.request.method).toBe('POST');
    req.flush(mockResponse);
  });

  it('should call the correct API endpoint and return a file blob and filename', () => {
    const mockCsrDetails: CsrDetails = {
      commonName: 'example.com',
      organization: 'Example Org',
      organizationalUnit: 'IT',
      country: 'US',
    };
    const mockBlob = new Blob(['mock-content'], {
      type: 'application/octet-stream',
    });
    const mockFileName = 'mock-csr.pem';

    service.generateCsr(mockCsrDetails).subscribe((response) => {
      expect(response.file).toEqual(mockBlob);
      expect(response.fileName).toEqual(mockFileName);
    });

    const req = httpMock.expectOne(`${mockApiUrl}/auth-api/csr/generate`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual(mockCsrDetails);
    req.flush(mockBlob, {
      headers: {
        'Content-Disposition': `attachment; filename="${mockFileName}"`,
      },
    });
  });

  it('should call the correct API endpoint and upload the file', () => {
    const mockFile = new File(['mock-content'], 'mock-file.txt', {
      type: 'text/plain',
    });
    const mockResponse = { success: true };

    service.uploadCredentials(mockFile).subscribe((response) => {
      expect(response).toEqual(mockResponse);
    });

    const req = httpMock.expectOne(`${mockApiUrl}/user-api/credential`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body instanceof FormData).toBe(true);
    expect(req.request.body.has('file')).toBe(true);
    expect(req.request.body.get('file')).toEqual(mockFile);
    req.flush(mockResponse);
  });

  it('should extract the filename from Content-Disposition header', () => {
    const contentDisposition = 'attachment; filename="example.pem"';
    const result = service['extractFileName'](contentDisposition);
    expect(result).toBe('example.pem');
  });

  it('should return default filename if Content-Disposition is null', () => {
    const result = service['extractFileName'](null);
    expect(result).toBe('csr.pem');
  });

  it('should return default filename if filename is not present in Content-Disposition', () => {
    const contentDisposition = 'attachment';
    const result = service['extractFileName'](contentDisposition);
    expect(result).toBe('csr.pem');
  });
});
