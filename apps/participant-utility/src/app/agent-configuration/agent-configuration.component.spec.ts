import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AgentConfigurationComponent } from './agent-configuration.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { translocoTestConfiguration, AppDomain } from '@test-environment';
import { ActivatedRoute } from '@angular/router';

describe('AgentConfigurationComponent', () => {
  let component: AgentConfigurationComponent;
  let fixture: ComponentFixture<AgentConfigurationComponent>;

  const mockActivatedRoute = {
    snapshot: {
      queryParams: {
        tab: 'GENERATE_KEYPAIR'
      }
    }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        AgentConfigurationComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)
        ),
        NoopAnimationsModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: mockActivatedRoute }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgentConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
