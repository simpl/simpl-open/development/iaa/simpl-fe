import { ComponentFixture, TestBed } from '@angular/core/testing';
import { KeypairGenerationComponent } from './keypair-generation.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { translocoTestConfiguration, AppDomain } from '@test-environment';
import { AgentConfigurationService } from '../agent-configuration.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';

describe('KeypairGenerationComponent', () => {
  let component: KeypairGenerationComponent;
  let fixture: ComponentFixture<KeypairGenerationComponent>;

  const mockAgentConfigurationService = {
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        KeypairGenerationComponent,
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)
        ),
        NoopAnimationsModule
      ],
      providers: [
        provideHttpClient(),
        { provide: AgentConfigurationService, useValue: mockAgentConfigurationService }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KeypairGenerationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
