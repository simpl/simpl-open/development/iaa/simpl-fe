import { Component } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { CommonModule } from '@angular/common';
import { KeypairGenerationComponent } from './keypair-generation/keypair-generation.component';
import { KeypairImportComponent } from './keypair-import/keypair-import.component';
import { CsrGenerationComponent } from './csr-generation/csr-generation.component';
import { CredentialsImportComponent } from "./credentials-import/credentials-import.component";
import { EuiTabsModule } from '@eui/components/eui-tabs';

@Component({
  selector: 'app-agent-configuration',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoDirective,
    KeypairGenerationComponent,
    KeypairImportComponent,
    CsrGenerationComponent,
    CredentialsImportComponent,
    EuiTabsModule,
  ],
  templateUrl: './agent-configuration.component.html'
})
export class AgentConfigurationComponent {
}
