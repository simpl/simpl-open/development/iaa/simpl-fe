import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CredentialsImportComponent } from './credentials-import.component';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { translocoTestConfiguration, AppDomain } from '@test-environment';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { AgentConfigurationService } from '../agent-configuration.service';

describe('CredentialsImportComponent', () => {
  let component: CredentialsImportComponent;
  let fixture: ComponentFixture<CredentialsImportComponent>;
  const mockAgentConfigurationService = {
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CredentialsImportComponent,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)
        ),
        NoopAnimationsModule
      ],
      providers: [
        provideHttpClient(),
        { provide: AgentConfigurationService, useValue: mockAgentConfigurationService }
      ]
    })
      .compileComponents();


    fixture = TestBed.createComponent(CredentialsImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
