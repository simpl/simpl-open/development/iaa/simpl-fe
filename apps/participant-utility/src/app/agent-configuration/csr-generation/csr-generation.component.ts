import { Component, inject, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { AgentConfigurationService } from '../agent-configuration.service';
import { EuiButtonModule } from '@eui/components/eui-button';
import { EuiIconModule } from '@eui/components/eui-icon';
import { EuiLabelModule } from '@eui/components/eui-label';
import { EuiDialogComponent, EuiDialogModule } from '@eui/components/eui-dialog';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { EuiInputGroupModule } from '@eui/components/eui-input-group';
import { EuiInputTextModule } from '@eui/components/eui-input-text';
import { EuiProgressBarModule } from '@eui/components/eui-progress-bar';

@Component({
  selector: 'app-csr-generation',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoDirective,
    MatButtonModule,
    EuiButtonModule,
    EuiIconModule,
    EuiLabelModule,
    EuiDialogModule,
    FormsModule,
    ReactiveFormsModule,
    EuiInputGroupModule,
    EuiInputTextModule,
    EuiProgressBarModule,
  ],
  templateUrl: './csr-generation.component.html',
})
export class CsrGenerationComponent implements OnInit {
  status: 'loading' | 'prompt' | 'success' | 'error' = 'prompt';
  @ViewChild('dialog') dialog: EuiDialogComponent;
  private readonly agentConfigurationService = inject(
    AgentConfigurationService
  );
  private readonly translocoService = inject(TranslocoService);
  hasKeypair = false;

  csrDetailsForm: FormGroup = new FormGroup({
    commonName: new FormControl('', [Validators.required]),
    organization: new FormControl('', [Validators.required]),
    organizationalUnit: new FormControl('', [Validators.required]),
    country: new FormControl('', [Validators.required]),
  });

  ngOnInit(): void {
    this.csrDetailsForm.statusChanges.subscribe((status) => {
      if (status === 'VALID') {
        this.dialog.enableAcceptButton();
      } else {
        this.dialog.disableAcceptButton();
      }
    });

    this.agentConfigurationService.hasKeypair().subscribe({
      next: (keyPair) => {
        this.hasKeypair = keyPair;
      },
    });
  }

  public launchCertificateSigningRequest(): void {
    this.dialog.openDialog();
    this.dialog.disableAcceptButton();
  }

  generateCsr() {
    this.dialog.openDialog();
    this.dialog.hasDismissButton = false;
    this.dialog.hasAcceptButton = false;
    this.status = 'loading';
    this.agentConfigurationService
      .generateCsr(this.csrDetailsForm.value)
      .subscribe({
        next: ({ file, fileName }) => {
          this.dialog.closeDialog();
          this.status = 'success';
          this.dialog.acceptLabel = this.translocoService.translate('agentConfiguration.csrGeneration.ok')
          this.dialog.hasAcceptButton = true;
          this.dialog.hasDismissButton = false;
          this.csrDetailsForm.reset()
          this.dialog.openDialog();
          this.downloadDocument(file, fileName);
        },
        error: () => {
          this.dialog.closeDialog();
          this.status = 'error';
          this.dialog.acceptLabel = this.translocoService.translate('agentConfiguration.csrGeneration.ok')
          this.dialog.hasAcceptButton = true;
          this.dialog.hasDismissButton = false;
          this.csrDetailsForm.reset()
          this.dialog.openDialog();
        },
      });
  }

  downloadDocument(file: Blob, fileName: string) {
    const objectURL = URL.createObjectURL(file);
    const fileLink = document.createElement('a');
    fileLink.href = objectURL;
    fileLink.download = fileName;
    fileLink.click();
    URL.revokeObjectURL(objectURL);
  }

  onAccept() {
    if (this.status !== 'success' && this.status !== 'error') {
      this.generateCsr();
    }

    if (this.status === 'success' || this.status === 'error') {
      this.status = 'prompt';
      this.dialog.hasDismissButton = true;
      this.dialog.acceptLabel = this.translocoService.translate(
        'agentConfiguration.csrGeneration.ok'
      );
    }
  }

  onDismiss() {
    this.status = 'prompt';
    this.csrDetailsForm.reset()
    this.dialog.acceptLabel = this.translocoService.translate(
      'agentConfiguration.csrGeneration.confirm'
    );
  }
}
