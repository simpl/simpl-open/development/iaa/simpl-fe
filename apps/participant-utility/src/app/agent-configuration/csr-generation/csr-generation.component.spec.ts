import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CsrGenerationComponent } from './csr-generation.component';
import { provideHttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { translocoTestConfiguration, AppDomain } from '@test-environment';
import { of } from 'rxjs';
import { AgentConfigurationService } from '../agent-configuration.service';

describe('CsrGenerationComponent', () => {
  let component: CsrGenerationComponent;
  let fixture: ComponentFixture<CsrGenerationComponent>;

  const mockAgentConfigurationService = {
    hasKeypair: jest.fn().mockReturnValue(of(false))
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CsrGenerationComponent,
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)
        ),
        NoopAnimationsModule
      ],
      providers: [
        provideHttpClient(),
        { provide: AgentConfigurationService, useValue: mockAgentConfigurationService }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CsrGenerationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call hasKeypair on ngOnInit', () => {
    expect(mockAgentConfigurationService.hasKeypair).toHaveBeenCalled();
    expect(component.hasKeypair).toBe(false);
  });
});
