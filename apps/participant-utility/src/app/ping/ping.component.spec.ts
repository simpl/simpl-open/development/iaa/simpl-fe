import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PingComponent } from './ping.component';
import { PingService } from './ping.service';
import { HttpParams } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { AppDomain, translocoTestConfiguration } from '@test-environment';

describe('PingComponent', () => {
  let component: PingComponent;
  let fixture: ComponentFixture<PingComponent>;
  let pingServiceMock: any;

  beforeEach(async () => {
    pingServiceMock = {
      ping: jest.fn()
    };

    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        PingComponent,
        TranslocoTestingModule.forRoot(translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)),
      ],
      providers: [
        { provide: PingService, useValue: pingServiceMock }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PingComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not call ping service if form is invalid', () => {
    component.onPing();
    expect(pingServiceMock.ping).not.toHaveBeenCalled();
  });

  it('should call ping service if form is valid', () => {
    const testFqdn = 'example.com';
    const params = new HttpParams().append('fqdn', testFqdn);

    component.fqdnControl.setValue(testFqdn);

    pingServiceMock.ping.mockReturnValue(of({} as any));

    component.onPing();

    expect(pingServiceMock.ping).toHaveBeenCalledWith(params);
  });


  it('should handle error from ping service', () => {
    const testFqdn = 'example.com';
    const errorResponse = new Error('Ping failed');
    const params = new HttpParams().append('fqdn', testFqdn);

    component.fqdnControl.setValue(testFqdn);
    pingServiceMock.ping.mockReturnValue(throwError(() => errorResponse));

    const consoleLogSpy = jest.spyOn(console, 'log').mockImplementation();

    component.onPing();

    expect(pingServiceMock.ping).toHaveBeenCalledWith(params);
    expect(consoleLogSpy).not.toHaveBeenCalledWith(errorResponse); // errore non loggato
  });
});
