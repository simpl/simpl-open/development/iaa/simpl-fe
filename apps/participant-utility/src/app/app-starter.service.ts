import { inject, Injectable } from '@angular/core';
import {
    I18nService,
    EuiServiceStatus,
} from '@eui/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class AppStarterService {

    private readonly i18nService: I18nService = inject(I18nService);

    start(): Observable<EuiServiceStatus> {
      return this.i18nService.init();
    }
}
