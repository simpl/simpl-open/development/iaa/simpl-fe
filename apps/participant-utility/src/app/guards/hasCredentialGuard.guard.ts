import { inject, Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { EchoService } from '../echo/echo.service';
import { Router, UrlTree } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class HasCredentialGuard {
  private readonly api = inject(EchoService);
  private readonly router = inject(Router);

  canActivate(): Observable<boolean | UrlTree> {
    return this.api.hasCredential().pipe(
        map((response: boolean) => response || this.router.parseUrl('/')),
      catchError(() => {
        return of(false);
      })
    );
  }
}
