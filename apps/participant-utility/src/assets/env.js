(function(window) {
  window.env = window.env || {};

  // Environment variables
  window["env"]["api_Url"] = "https://t1.iaa-dsdev-consumer.dev.simpl-europe.eu";
  window["env"]["keycloakConfig_url"] ="https://t1.iaa-dsdev-consumer.dev.simpl-europe.eu/auth";
  window["env"]["keycloakConfig_realm"] = "participant";
  window["env"]["keycloakConfig_clientId"] ="frontend-cli";

})(this);
