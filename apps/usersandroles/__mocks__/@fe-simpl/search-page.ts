import { Component, Input } from "@angular/core";
import { Filter, MultipleData } from '@fe-simpl/search-page';
import { FormControl } from '@angular/forms';


@Component({
  selector: "lib-search-page",
  standalone: true,
  imports: [ ],
  template: "<div>mock-search-page</div>",
})
export class SearchPageComponent {

  @Input()
  pageInfo: unknown

  @Input()
  clickable: boolean

  @Input()
  checkboxToCheck: any[]

  @Input() offlinePaginator = false;
}

export class TextFilter extends Filter {
  constructor(name: string, label: string) {
    super(name, label);
  }

  override value(){return ''}
  override reset() {}
}

export class DateRangeFilter extends Filter<MultipleData<{ [key: string]: Date }>> {
  readonly nameFrom: string;
  readonly nameTo: string;
  readonly controllerFrom = new FormControl();
  readonly controllerTo = new FormControl();

  constructor(name: string, label: string, nameFrom: string, nameTo: string) {
    super(name, label);
    this.nameFrom = nameFrom;
    this.nameTo = nameTo;
  }

  override value(): MultipleData<{ [key: string]: Date }> | undefined {
    if (this.controllerFrom.value) {
      const from = this.controllerFrom.value;
      const to = this.controllerTo.value ? new Date(this.controllerTo.value) : undefined;
      if (to) {
        to.setDate(to.getDate() + 1);
      }
      return new MultipleData({
        [this.nameFrom]: from,
        ...(to ? { [this.nameTo]: to } : {}),
      });
    } else {
      return undefined;
    }
  }

  override reset(): void {
    this.controllerFrom.reset();
    this.controllerTo.reset();
  }
}
