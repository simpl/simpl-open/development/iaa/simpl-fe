import { EuiAppConfig } from '@eui/core';
import { GLOBAL } from './global';

export const appConfig: EuiAppConfig = {
    global: GLOBAL
};
