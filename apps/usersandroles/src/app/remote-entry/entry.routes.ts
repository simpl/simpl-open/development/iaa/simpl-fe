import { Route } from '@angular/router';
import { canActivateAuthRole } from '../guard/auth-guard';

export const remoteRoutes: Route[] = [
  {
    path: "",
    redirectTo: "identity-attributes-info",
    pathMatch: "full",
  },
  {
    path: "identity-attributes-info",
    loadComponent: () =>
      import("../identity-attributes-info/identity-attributes-info.component").then(
        (m) => m.IdentityAttributesInfoComponent
      ),
    canActivate: [canActivateAuthRole],
    data: { roles: ["T1UAR_M"] },
  },
  {
    path: "users",
    loadComponent: () =>
      import("../users/users-information-page/users-information-page.component").then(
        (m) => m.UsersInformationPageComponent
      ),
    canActivate: [canActivateAuthRole],
    data: { roles: ["T1UAR_M"] },
  },
  {
    path: "users/new-user",
    loadComponent: () =>
      import("../users/user-creation-page/user-creation-page.component").then(
        (m) => m.UserCreationPageComponent
      ),
    canActivate: [canActivateAuthRole],
    data: { roles: ["T1UAR_M"] },
  },
  {
    path: "roles",
    loadComponent: () =>
      import("../roles/roles-information-page/roles-information-page.component").then(
        (m) => m.RolesInformationPageComponent
      ),
    canActivate: [canActivateAuthRole],
    data: { roles: ["T1UAR_M"] },
  },
  {
    path: "role/:id",
    loadComponent: () =>
      import("../roles/role-detail/role-detail.component").then(
        (m) => m.RoleDetailComponent
      ),
    canActivate: [canActivateAuthRole],
    data: { roles: ["T1UAR_M"] },
  },
  {
    path: "unauthorized",
    loadComponent: () =>
      import("@fe-simpl/landing-page").then((m) => m.UnauthorizedPageComponent)
  },
  {
    path: "error",
    loadComponent: () =>
      import("@fe-simpl/landing-page").then((m) => m.ErrorPageComponent)
  }
];
