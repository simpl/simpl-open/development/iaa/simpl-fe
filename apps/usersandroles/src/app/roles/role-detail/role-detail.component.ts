import { Component, inject, OnInit, signal, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IdentityAttributeDTO, RolesService } from '../roles.service';
import { ActivatedRoute } from '@angular/router';
import { finalize, tap } from 'rxjs';
import { TranslocoDirective } from '@jsverse/transloco';
import { SearchDataSource } from '@fe-simpl/search-page';
import { RemovePlaceholderPipe } from '../../pipes/remove-placeholder.pipe';
import { EuiButtonModule } from '@eui/components/eui-button';
import { EuiIconModule } from '@eui/components/eui-icon';
import { EuiLabelModule } from '@eui/components/eui-label';
import { EuiTableV2Module, Sort } from '@eui/components/eui-table-v2';
import {
  EuiPaginationEvent,
  EuiPaginatorModule,
} from '@eui/components/eui-paginator';
import { EuiProgressBarModule } from '@eui/components/eui-progress-bar';
import {
  EuiDialogComponent,
  EuiDialogModule,
} from '@eui/components/eui-dialog';
import { EuiAlertModule } from '@eui/components/eui-alert';
import { EuiPageModule } from '@eui/components/eui-page';
import { EuiButtonGroupModule } from '@eui/components/eui-button-group';
import { EuiCardModule } from '@eui/components/eui-card';
import {
  EuiDateRangeSelectorDates,
  EuiDateRangeSelectorModule,
} from '@eui/components/eui-date-range-selector';
import { EuiInputGroupModule } from '@eui/components/eui-input-group';
import { EuiInputTextModule } from '@eui/components/eui-input-text';
import { EuiResizableDirectiveModule } from '@eui/components/directives';
import { EuiChipListModule } from '@eui/components/eui-chip-list';
import { EuiChipModule } from '@eui/components/eui-chip';
import { HttpParams } from '@angular/common/http';
import { Role } from '../../users/users.service';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-role-detail',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoDirective,
    RemovePlaceholderPipe,
    EuiButtonModule,
    EuiIconModule,
    EuiLabelModule,
    EuiTableV2Module,
    EuiPaginatorModule,
    EuiProgressBarModule,
    EuiDialogModule,
    EuiAlertModule,
    EuiPageModule,
    EuiButtonGroupModule,
    EuiCardModule,
    EuiDateRangeSelectorModule,
    EuiInputGroupModule,
    EuiInputTextModule,
    EuiResizableDirectiveModule,
    EuiChipListModule,
    EuiChipModule,
    ReactiveFormsModule,
  ],
  templateUrl: './role-detail.component.html',
  styleUrl: './role-detail.component.css',
})
export class RoleDetailComponent implements OnInit {
  private readonly service = inject(RolesService);
  private readonly activatedRoute = inject(ActivatedRoute);

  @ViewChild('dialog') confirmDialog: EuiDialogComponent;

  loading = signal(false);
  tableLoading = signal(false);
  roleId: string;
  roleDetail: Role;
  attributes: string[] = [];
  preSelected: Array<IdentityAttributeDTO> = [];

  saveStatus: '' | 'success' | 'error' = '';
  alertTimeout = 6000;

  public pagination: EuiPaginationEvent;
  data: Array<IdentityAttributeDTO> = [];
  totalElements = 0;

  sortingCriteria: Array<string> = [];
  filtersForm: FormGroup;
  chips: Array<{field: string, value: string}> = [];

  constructor() {
    this.roleId = this.activatedRoute.snapshot.params['id'];
  }

  ngOnInit() {
    this.filtersForm = new FormGroup({
      name: new FormControl(''),
      code: new FormControl(''),
      updateTimestamp: new FormControl<EuiDateRangeSelectorDates>({
        value: {
          startRange: null,
          endRange: null
        },
        disabled: false,
      }),
    });

    this.pagination = {
      page: 0,
      pageSize: 5,
      nbPage: 5,
    };

    this.service.getRoleDetail(this.roleId).subscribe();

    this.service.roleDetail$
      .pipe(
        tap((r) => {
          this.roleDetail = { ...r };
          this.attributes = this.roleDetail.assignedIdentityAttributes?.slice();
          this.getIdentityAttributeList();
        })
      )
      .subscribe();
  }

  getIdentityAttributeList() {
    this.tableLoading.set(true);
    let params: HttpParams = new HttpParams()
      .set('page', this.pagination.page)
      .set('size', this.pagination.pageSize);

    if (this.filtersForm.value) {
      for (const [key, value] of Object.entries(
        this.filtersForm.getRawValue()
      )) {
        if (value) {
          if (key === 'updateTimestamp') {
            const dateRange = value as EuiDateRangeSelectorDates;
            if (dateRange.startRange) {
              params = params.append(
                'updateTimestampFrom',
                dateRange.startRange.toISOString()
              );
            }
            if (dateRange.endRange) {
              params = params.append(
                'updateTimestampTo',
                dateRange.endRange.toISOString()
              );
            }
          } else {
            params = params.append(key, value as string);
          }
        }
      }
    }

    if (this.sortingCriteria) {
      this.sortingCriteria.forEach((criteria) => {
        params = params.append('sort', criteria);
      });
    }

    this.updateChips();

    this.service
      .getIdentityAttributeList(params)
      .pipe(finalize(() => this.tableLoading.set(false)))
      .subscribe({
        next: (response: SearchDataSource) => {
          this.data = response.content;
          this.totalElements = response.page.totalElements;
          this.preSelected = this.getPreSelected();
        },
        error: (err) => {
          console.error(err);
        },
      });
  }

  updateChips(){
    this.chips = Object.keys(this.filtersForm.controls).flatMap(field => {
      if (field === 'updateTimestamp') {
        const chipsArray = [];
        const dateRange = this.filtersForm.get(field)?.value as EuiDateRangeSelectorDates;
        if (dateRange?.startRange) {
          chipsArray.push({ field: 'updateTimestampFrom', value: dateRange.startRange.format('DD/MM/YYYY') });
        }
        if (dateRange?.endRange) {
          chipsArray.push({ field: 'updateTimestampTo', value: dateRange.endRange.format('DD/MM/YYYY') });
        }
        return chipsArray;
      } else {
        const value = this.filtersForm.get(field)?.value;
        return value ? [{ field, value }] : [];
      }
    });
  }

  removeChip(event: any) {
    if(['updateTimestampFrom', 'updateTimestampTo'].includes(event.removed.id)){
      this.filtersForm.get('updateTimestamp')?.reset();
    } else {
      this.filtersForm.get(event.removed.id)?.reset();
    }
    this.filtersForm.updateValueAndValidity();
    this.getIdentityAttributeList();
  }

  onSortChange(newSortCriteria: Array<Sort>) {
    this.sortingCriteria = newSortCriteria.map((sort) => {
      return sort.sort + ',' + sort.order;
    });

    this.getIdentityAttributeList();
  }

  getPreSelected(): Array<IdentityAttributeDTO> {
    const allSelected = this.attributes?.concat(
      this.roleDetail?.assignedIdentityAttributes
    );
    const preSelected = allSelected?.flatMap((attributeCode) => {
      const roleFound = this.data?.find(
        (attribute) => attribute.code === attributeCode
      );
      return roleFound || [];
    });

    return preSelected || [];
  }

  onSelectedAttributes(attributes: Array<IdentityAttributeDTO>) {
    this.attributes = [...attributes].map((attribute) => attribute.code);
  }

  onSaveAttributes() {
    this.confirmDialog.openDialog();
  }

  confirmSave() {
    this.loading.set(true);
    this.saveStatus = '';
    this.service
      .assignIdentityAttributesToARole(this.roleId, this.attributes)
      .pipe(
        finalize(() => {
          this.loading.set(false);
          this.confirmDialog.closeDialog();
        })
      )
      .subscribe({
        next: (_: SearchDataSource) => {
          this.getIdentityAttributeList();
          this.saveStatus = 'success';

          setTimeout(() => {
            this.saveStatus = '';
          }, this.alertTimeout);
        },
        error: (_) => {
          this.saveStatus = 'error';

          setTimeout(() => {
            this.saveStatus = '';
          }, this.alertTimeout);
        },
      });
  }

  onResetAttributes() {
    this.attributes = this.roleDetail.assignedIdentityAttributes.slice();
    this.getIdentityAttributeList();
  }

  resetFilters() {
    this.filtersForm.patchValue({
      name: '',
      code: '',
      updateTimestamp: {
        startRange: null,
        endRange: null,
      },
    });
    this.filtersForm.updateValueAndValidity();
    this.getIdentityAttributeList();
  }

  onPageChange(e: EuiPaginationEvent): void {
    this.pagination = e;
    this.getIdentityAttributeList();
  }
}
