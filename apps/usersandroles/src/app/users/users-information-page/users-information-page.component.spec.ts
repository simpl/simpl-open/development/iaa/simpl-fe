import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UsersInformationPageComponent } from './users-information-page.component';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { AppDomain, translocoTestConfiguration } from '@test-environment';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SearchPageComponent } from '@fe-simpl/search-page';
import { CommonModule } from '@angular/common';
import { of } from 'rxjs';
import { UsersService } from '../users.service';
import { MatDialog } from '@angular/material/dialog';
import { provideHttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { API_URL } from '@fe-simpl/api';

describe('UsersInformationPageComponent', () => {
  let component: UsersInformationPageComponent;
  let fixture: ComponentFixture<UsersInformationPageComponent>;
  let httpTestingController: HttpTestingController;
  const mockApiUrl = 'http://mock-api-url';
  const mockUsersService = {
    getUserRoles: jest.fn().mockReturnValue(of([])),
    getUserList: jest.fn().mockReturnValue(of([])),
  };

  const mockDialog = {
    open: jest.fn().mockReturnValue({
      afterClosed: jest.fn().mockReturnValue(of(true))
    })
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CommonModule,
        UsersInformationPageComponent,
        SearchPageComponent,
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.USERS_AND_ROLES)
        ),
        NoopAnimationsModule
      ],
      providers: [
        provideHttpClient(),
        { provide: UsersService, useValue: mockUsersService },
        { provide: MatDialog, useValue: mockDialog },
        { provide: ActivatedRoute, useValue: { snapshot: {} } },
        { provide: API_URL, useValue: mockApiUrl },
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(UsersInformationPageComponent);
    component = fixture.componentInstance;
    httpTestingController = TestBed.inject(HttpTestingController);
    fixture.detectChanges();
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should call getUserRoles and open dialog when showUserRoles is called', () => {
    const mockRow = { id: '123' };
    component.showUserRoles(mockRow);

    expect(mockUsersService.getUserRoles).toHaveBeenCalledWith(mockRow.id);
  });
})
