import { Component, inject, OnInit, signal, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { finalize } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService, UserRole, User } from '../users.service';
import {
  RoleListModalComponent,
  UserRolesDialogData,
} from './role-list-modal/role-list-modal.component';
import { EuiButtonModule } from '@eui/components/eui-button';
import { EuiIconModule } from '@eui/components/eui-icon';
import { EuiLabelModule } from '@eui/components/eui-label';
import { EuiTableV2Module } from '@eui/components/eui-table-v2';
import {
  EuiPaginationEvent,
  EuiPaginatorModule,
} from '@eui/components/eui-paginator';
import { EuiProgressBarModule } from '@eui/components/eui-progress-bar';
import {
  EuiDialogComponent,
  EuiDialogModule,
} from '@eui/components/eui-dialog';
import { EuiPageModule } from '@eui/components/eui-page';
import { EuiInputGroupModule } from '@eui/components/eui-input-group';
import { EuiInputTextModule } from '@eui/components/eui-input-text';
import { EuiButtonGroupModule } from '@eui/components/eui-button-group';
import { EuiChipListModule } from '@eui/components/eui-chip-list';
import { EuiChipModule } from '@eui/components/eui-chip';
import { EuiResizableDirectiveModule } from '@eui/components/directives';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { HttpParams } from '@angular/common/http';
@Component({
  selector: 'app-users-information-page',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoDirective,
    EuiButtonModule,
    EuiButtonGroupModule,
    EuiIconModule,
    EuiLabelModule,
    EuiTableV2Module,
    EuiPaginatorModule,
    EuiProgressBarModule,
    EuiDialogModule,
    EuiPageModule,
    RoleListModalComponent,
    ReactiveFormsModule,
    EuiInputGroupModule,
    EuiInputTextModule,
    EuiChipListModule,
    EuiChipModule,
    EuiResizableDirectiveModule
  ],
  templateUrl: './users-information-page.component.html',
})
export class UsersInformationPageComponent implements OnInit {
  private readonly usersService = inject(UsersService);
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);
  private readonly translocoService = inject(TranslocoService);

  data: Array<User> = [];
  loading = signal(false);

  public pagination: EuiPaginationEvent;
  public paginatedItems: Array<User> = [];

  filtersForm: FormGroup;
  chips: Array<{field: string, value: string}> = [];

  @ViewChild('dialog') dialog: EuiDialogComponent;
  roleDialogData: UserRolesDialogData = {
    roles: [],
    title: '',
    cancel: '',
    noRoles: '',
  };

  ngOnInit(): void {
    this.filtersForm = new FormGroup({
      email: new FormControl(''),
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      username: new FormControl(''),
    });

    this.pagination = {
      page: 0,
      pageSize: 5,
      nbPage: 5,
    };

    this.search();
  }

  search() {
    this.loading.set(true);

    let searchFilters: HttpParams = new HttpParams()
      .set('page', this.pagination.page)
      .set('size', this.pagination.pageSize);

    if (this.filtersForm.value) {
      for (const [key, value] of Object.entries(
        this.filtersForm.getRawValue()
      )) {
        if (value) {
          searchFilters = searchFilters.append(key, value as string);
        }
      }
    }

    this.updateChips();

    this.usersService
      .getUserList(searchFilters)
      .pipe(finalize(() => this.loading.set(false)))
      .subscribe({
        next: (response: Array<User>) => {
          this.data = response;
          this.paginatedItems = this.data.slice(0, this.pagination.pageSize);
        },
        error: (err) => {
          console.error(err);
        },
      });
  }

  updateChips(){
    this.chips = Object.keys(this.filtersForm.controls)
      .filter(field => !!this.filtersForm.get(field)?.value)
      .map(field => ({ field, value: this.filtersForm.get(field)?.value }));
  }

  removeChip(event: any) {
    this.filtersForm.get(event.removed.id)?.reset();
    this.filtersForm.updateValueAndValidity();
    this.search();
  }

  onPageChange(e: EuiPaginationEvent): void {
    this.pagination = e;
    this.paginatedItems = this.data.slice(
      e.page * e.pageSize,
      e.page * e.pageSize + e.pageSize
    );
  }

  resetFilters(){
    this.filtersForm.reset();
    this.filtersForm.updateValueAndValidity();
    this.search();
  }

  showUserRoles(row: any) {
    this.loading.set(true);

    this.roleDialogData.title = this.translocoService.translate(
      'userInformationPage.userRoles'
    );
    this.roleDialogData.noRoles = 'userInformationPage.noRoles';
    this.roleDialogData.cancel = this.translocoService.translate(
      'userInformationPage.cancel'
    );

    this.usersService
      .getUserRoles(row.id)
      .pipe(finalize(() => this.loading.set(false)))
      .subscribe({
        next: (roles: Array<UserRole>) => {
          this.roleDialogData.roles = roles;
          this.dialog.openDialog();
        },
      });
  }

  navigateToUserCreation() {
    this.router.navigate(['/users/new-user'], { relativeTo: this.route });
  }
}
