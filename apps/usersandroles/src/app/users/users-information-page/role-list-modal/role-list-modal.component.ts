import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { TranslocoDirective } from '@jsverse/transloco';
import { UserRole } from '../../users.service';
import { RemovePlaceholderPipe } from '../../../pipes/remove-placeholder.pipe';

export interface UserRolesDialogData {
  title: string,
  cancel: string,
  noRoles: string,
  roles: Array<UserRole>
}

@Component({
  selector: 'app-role-list-modal',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoDirective,
    RemovePlaceholderPipe
  ],
  templateUrl: './role-list-modal.component.html',
  styleUrl: './role-list-modal.component.css'
})
export class RoleListModalComponent  {
  @Input() data: UserRolesDialogData = {
    title: '',
    cancel: '',
    noRoles: '',
    roles: []
  };

  constructor() {}
}
