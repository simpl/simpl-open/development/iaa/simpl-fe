import { CommonModule } from '@angular/common';
import { Component, inject, NgZone, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ReactiveFormsModule, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { UsersService } from '../users.service';
import { RoleDto } from '@fe-simpl/api-types';
import { AlertBannerService } from '@fe-simpl/alert-banner';
import { EuiInputGroupModule } from '@eui/components/eui-input-group';
import { EuiInputTextModule } from '@eui/components/eui-input-text';
import { EuiProgressBarModule } from '@eui/components/eui-progress-bar';
import { EuiLabelModule } from '@eui/components/eui-label';
import { EuiIconModule } from '@eui/components/eui-icon';
import { EuiFeedbackMessageModule } from '@eui/components/eui-feedback-message';
import { EuiButtonModule } from '@eui/components/eui-button';
import { EuiSelectModule } from '@eui/components/eui-select';
import { EuiAutoCompleteItem, EuiAutocompleteModule } from '@eui/components/eui-autocomplete';
import { EuiPageModule } from '@eui/components/eui-page';

@Component({
  selector: 'app-user-creation-page',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslocoDirective,
    EuiInputGroupModule,
    EuiInputTextModule,
    EuiProgressBarModule,
    EuiLabelModule,
    EuiIconModule,
    EuiFeedbackMessageModule,
    EuiButtonModule,
    EuiSelectModule,
    EuiAutocompleteModule,
    EuiPageModule
  ],
  templateUrl: './user-creation-page.component.html',
  styleUrl: './user-creation-page.component.css'
})
export class UserCreationPageComponent implements OnInit {
  private readonly router = inject(Router);
  private readonly service = inject(UsersService);
  private readonly alertBannerService = inject(AlertBannerService);
  private readonly translocoService = inject(TranslocoService);
  private readonly route = inject(ActivatedRoute);
  userForm: FormGroup;
  loading: boolean = false;
  roles: Array<RoleDto> = [];
  selectableRoles: EuiAutoCompleteItem[] = [];
  passwordVisible = false;
  passwordConfirmVisible = false;
  private readonly ngZone = inject(NgZone);

  ngOnInit(): void {
    this.userForm = new FormGroup(
      {
        email: new FormControl("", [
          Validators.required,
          Validators.email,
          Validators.pattern("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$"),
        ]),
        firstName: new FormControl("", [Validators.required]),
        lastName: new FormControl("", [Validators.required]),
        username: new FormControl("", [Validators.required]),
        password: new FormControl("", [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(64),
          detailedPasswordValidation,
        ]),
        confirmPassword: new FormControl("", [Validators.required]),
        roles: new FormControl([], [Validators.required]),
      },
      { validators: confirmPasswordValidator }
    );

    this.service.getRoleList().subscribe(
      res => {
        this.roles = res
        this.selectableRoles = this.roles.map(role => {
          return {
            id: role.name,
            label: role.name
          }
        })
      }
    )
  }

  onSubmit() {
    if (this.userForm.valid) {
      this.loading = true;

      let newUser = this.userForm.getRawValue();
      const selectedRoles = this.userForm.get('roles')?.value || [];

      newUser.roles = selectedRoles.map((role: EuiAutoCompleteItem) => role.id)

      this.service.createUser(newUser)
      .subscribe({
        next: () => {
          this.loading = false;
          this.alertBannerService.show({
            message: this.translocoService.translate("newUser.saveSuccess"),
            type: "info",
          });
          this.ngZone.run(() => {
            this.router.navigate(['/users'], { relativeTo: this.route });
          })
        },
        error: (err) => {
          this.loading = false;
        }
      });
    }
  }
}

export const confirmPasswordValidator: ValidatorFn = (
  control: AbstractControl
): ValidationErrors | null => {
  const passwordControl = control.get("password");
  const confirmPasswordControl = control.get("confirmPassword");

  if (!passwordControl || !confirmPasswordControl) {
    return null;
  }

  const match = passwordControl.value === confirmPasswordControl.value;

  if (!match) {
    confirmPasswordControl.setErrors({
      ...confirmPasswordControl.errors,
      noMatch: true,
    });
  } else {
    confirmPasswordControl.setErrors(
      confirmPasswordControl.errors
        ? { ...confirmPasswordControl.errors }
        : null
    );
  }

  return null;
};

export const detailedPasswordValidation: ValidatorFn = (
  control: AbstractControl
): ValidationErrors | null => {
  const value = control.value;
  const errors: any = {};

  if (!value || value.length < 10) {
    errors.minLength = true;
  }

  if (value.length > 64) {
    errors.maxLength = true;
  }

  if (!/[A-Z]/.test(value)) {
    errors.uppercase = true;
  }

  if (!/[a-z]/.test(value)) {
    errors.lowercase = true;
  }

  if (!/\d/.test(value)) {
    errors.numeric = true;
  }

  if (!/[!"#$%&'()+,-./:;<=>?@[\]^_`{|}~]/.test(value)) {
    errors.specialChar = true;
  }

  const forbiddenValues = [
    control.root.get("email")?.value,
    control.root.get("firstName")?.value,
    control.root.get("lastName")?.value,
    control.root.get("username")?.value,
  ];

  forbiddenValues.forEach((valueToCheck) => {
    if (valueToCheck) {
      for (let i = 0; i < valueToCheck.length; i++) {
        for (let j = i + 4; j <= valueToCheck.length; j++) {
          const portion = valueToCheck.substring(i, j);
          if (value.includes(portion)) {
            errors.forbidden = true;
            break;
          }
        }
      }
    }
  });

  return Object.keys(errors).length > 0 ? errors : null;
};
