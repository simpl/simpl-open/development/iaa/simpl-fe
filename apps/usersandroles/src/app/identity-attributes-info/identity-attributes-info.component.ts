import { Component, inject, OnInit, signal } from '@angular/core';
import { SearchDataSource } from '@fe-simpl/search-page';
import { TranslocoDirective } from '@jsverse/transloco';
import { EuiTableV2Module } from '@eui/components/eui-table-v2';
import { IdentityAttributesInfoService } from './identity-attributes-info.service';
import {
  EuiPaginationEvent,
  EuiPaginatorModule,
} from '@eui/components/eui-paginator';
import { EuiButtonModule } from '@eui/components/eui-button';
import { EuiLabelModule } from '@eui/components/eui-label';
import { EuiIconModule } from '@eui/components/eui-icon';
import { EuiDropdownModule } from '@eui/components/eui-dropdown';
import { EuiSelectModule } from '@eui/components/eui-select';
import { EuiInputTextModule } from '@eui/components/eui-input-text';
import {
  EuiDateRangeSelectorDates,
  EuiDateRangeSelectorModule,
} from '@eui/components/eui-date-range-selector';
import { EuiInputGroupModule } from '@eui/components/eui-input-group';
import { EuiPageModule } from '@eui/components/eui-page';
import { EuiButtonGroupModule } from '@eui/components/eui-button-group';
import { IdentityAttributeDTO } from '../roles/roles.service';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { HttpParams } from '@angular/common/http';
import { finalize } from 'rxjs';
import { EuiResizableDirectiveModule } from '@eui/components/directives';
import { EuiChipListModule } from '@eui/components/eui-chip-list';
import { EuiChipModule } from '@eui/components/eui-chip';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-identity-attributes-info',
  standalone: true,
  imports: [
    CommonModule,
    EuiTableV2Module,
    EuiPaginatorModule,
    EuiButtonGroupModule,
    EuiButtonModule,
    EuiLabelModule,
    EuiIconModule,
    EuiDropdownModule,
    EuiSelectModule,
    EuiInputTextModule,
    EuiDateRangeSelectorModule,
    EuiPageModule,
    TranslocoDirective,
    EuiDateRangeSelectorModule,
    EuiInputGroupModule,
    EuiInputTextModule,
    EuiResizableDirectiveModule,
    EuiChipListModule,
    EuiChipModule,
    ReactiveFormsModule,
  ],
  templateUrl: './identity-attributes-info.component.html',
})
export class IdentityAttributesInfoComponent implements OnInit {
  private readonly service = inject(IdentityAttributesInfoService);

  loading = signal(false);

  public pagination: EuiPaginationEvent;
  data: Array<IdentityAttributeDTO> = [];
  totalElements: number = 0;

  sortingCriteria: Array<string> = [];
  filtersForm: FormGroup;
  chips: Array<{field: string, value: string}> = [];

  ngOnInit() {
    this.filtersForm = new FormGroup({
      name: new FormControl(''),
      code: new FormControl(''),
      updateTimestamp: new FormControl<EuiDateRangeSelectorDates>({
        value: {
          startRange: null,
          endRange: null,
        },
        disabled: false,
      }),
    });

    this.pagination = {
      page: 0,
      pageSize: 5,
      nbPage: 5,
    };

    this.getIdentityAttributeList();
  }

  getIdentityAttributeList() {
    this.loading.set(true);
    let params: HttpParams = new HttpParams()
      .set('page', this.pagination.page)
      .set('size', this.pagination.pageSize);

    if (this.filtersForm.value) {
      for (const [key, value] of Object.entries(
        this.filtersForm.getRawValue()
      )) {
        if (value) {
          if (key === 'updateTimestamp') {
            const dateRange = value as EuiDateRangeSelectorDates;
            if (dateRange.startRange) {
              params = params.append(
                'updateTimestampFrom',
                dateRange.startRange.toISOString()
              );
            }
            if (dateRange.endRange) {
              params = params.append(
                'updateTimestampTo',
                dateRange.endRange.toISOString()
              );
            }
          } else {
            params = params.append(key, value as string);
          }
        }
      }
    }

    if (this.sortingCriteria) {
      this.sortingCriteria.forEach((criteria) => {
        params = params.append('sort', criteria);
      });
    }

    this.updateChips();

    this.service
      .getIdentityAttributeList(params)
      .pipe(finalize(() => this.loading.set(false)))
      .subscribe({
        next: (response: SearchDataSource) => {
          this.data = response.content;
          this.totalElements = response.page.totalElements;
        },
        error: (err) => {
          console.error(err);
        },
      });
  }

  updateChips(){
    this.chips = Object.keys(this.filtersForm.controls).flatMap(field => {
      if (field === 'updateTimestamp') {
        const chipsArray = [];
        const dateRange = this.filtersForm.get(field)?.value as EuiDateRangeSelectorDates;
        if (dateRange?.startRange) {
          chipsArray.push({ field: 'updateTimestampFrom', value: dateRange.startRange.format('DD/MM/YYYY') });
        }
        if (dateRange?.endRange) {
          chipsArray.push({ field: 'updateTimestampTo', value: dateRange.endRange.format('DD/MM/YYYY') });
        }
        return chipsArray;
      } else {
        const value = this.filtersForm.get(field)?.value;
        return value ? [{ field, value }] : [];
      }
    });
  }

  removeChip(event: any) {
    if(['updateTimestampFrom', 'updateTimestampTo'].includes(event.removed.id)){
      this.filtersForm.get('updateTimestamp')?.reset();
    } else {
      this.filtersForm.get(event.removed.id)?.reset();
    }
    this.filtersForm.updateValueAndValidity();
    this.getIdentityAttributeList();
  }

  onChangeDropdownStatus(event: boolean) {
    if (!event) {
      console.log('closed');
    }
  }

  onPageChange(e: EuiPaginationEvent): void {
    this.pagination = e;
    this.getIdentityAttributeList();
  }

  resetFilters() {
    this.filtersForm.patchValue({
      name: '',
      code: '',
      updateTimestamp: {
        startRange: null,
        endRange: null,
      },
    });
    this.filtersForm.updateValueAndValidity();
    this.getIdentityAttributeList();
  }
}
