import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { AppDomain, translocoTestConfiguration } from '@test-environment';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SearchPageComponent } from '@fe-simpl/search-page';
import { CommonModule } from '@angular/common';
import { Observable, of } from 'rxjs';
import { provideHttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { IdentityAttributesInfoComponent } from './identity-attributes-info.component';
import { API_URL } from '@fe-simpl/api';
import { CONFIG_TOKEN, EuiAppConfig, I18nService, I18nState } from '@eui/core';
import { EuiAppModule } from '@eui/components/layout';
import { TranslateModule } from '@ngx-translate/core';

describe('IdentityAttributesInfo', () => {
  let component: IdentityAttributesInfoComponent;
  let fixture: ComponentFixture<IdentityAttributesInfoComponent>;
  let httpTestingController: HttpTestingController;
  const mockApiUrl = 'http://mock-api-url';
  let i18nServiceMock: jest.Mocked<I18nService>;
  let configMock: EuiAppConfig;

  configMock = { global: {}, modules: { core: { base: 'localhost:3000', userDetails: 'dummy' } } };

  beforeEach(async () => {
    type GetStateReturnType<T> = T extends keyof I18nState ? Observable<I18nState[T]> : Observable<I18nState>;
    configMock = {global: {}, modules: {core: {base: 'localhost:3000', userDetails: 'dummy'}}};
    i18nServiceMock = {
      init: jest.fn(),
      getState: jest.fn(
        <K extends keyof I18nState>(key?: K): GetStateReturnType<K> => {
          if (typeof key === 'string') {
            return of({ activeLang: 'en' }[key]) as GetStateReturnType<K>;
          }
          return of({ activeLang: 'en' }) as GetStateReturnType<K>;
        }
      ),
    } as unknown as jest.Mocked<I18nService>;

    configMock = { global: {}, modules: { core: { base: 'localhost:3000', userDetails: 'dummy' } } };

    await TestBed.configureTestingModule({
      imports: [
        EuiAppModule,
        TranslateModule.forRoot(),
        CommonModule,
        IdentityAttributesInfoComponent,
        SearchPageComponent,
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.USERS_AND_ROLES)
        ),
        NoopAnimationsModule,
        EuiAppModule
      ],
      providers: [
        provideHttpClient(),
        { provide: ActivatedRoute, useValue: { snapshot: {} } },
        { provide: API_URL, useValue: mockApiUrl },
        {provide: I18nService, useValue: i18nServiceMock},
        {provide: CONFIG_TOKEN, useValue: configMock},
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(IdentityAttributesInfoComponent);
    component = fixture.componentInstance;
    httpTestingController = TestBed.inject(HttpTestingController);
    fixture.detectChanges();
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

})
