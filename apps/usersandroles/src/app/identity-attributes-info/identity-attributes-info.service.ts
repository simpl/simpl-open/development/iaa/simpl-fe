import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API_URL } from '@fe-simpl/api';
import { SearchDataSource } from '@fe-simpl/search-page';

@Injectable({
  providedIn: 'root',
})
export class IdentityAttributesInfoService {
  private readonly api_url = inject(API_URL);
  private readonly httpClient = inject(HttpClient);

  getIdentityAttributeList(params: HttpParams) {
    return this.httpClient.get<SearchDataSource>(
      this.api_url + `/user-api/identity-attribute/search`,
      { params }
    );
  }
}
