import { Route } from "@angular/router";
import { authGuard, NoAuthGuard } from '@fe-simpl/auth';

export const remoteRoutes: Route[] = [
  {
    path: "",
    redirectTo: "application/info",
    pathMatch: "full",
  },
  {
    path: "application/info",
    loadComponent: () =>
      import("../application/info-page/info-page.component").then((m) => m.InfoPageComponent),
    //canActivate: [NoAuthGuard],
  },
  {
    path: "application/request",
    loadComponent: () =>
      import("../application/request-credentials/request-credentials.component").then(
        (m) => m.RequestCredentialsComponent
      ),
    //canActivate: [NoAuthGuard],
  },
  {
    path: "application/additional-request",
    loadComponent: () =>
      import("../application/onboarding-request/onboarding-request.component").then((m) => m.OnboardingRequestComponent),
    canActivate: [authGuard],
    data: { roles: ["APPLICANT"]}
  },
  {
    path: "administration",
    loadComponent: () =>
      import("../administration/requests/requests.component").then((m) => m.RequestsComponent),
    canActivate: [authGuard],
    data: { roles: ["NOTARY"] },
  },
  {
    path: "administration/:email",
    loadComponent: () =>
      import("../administration/request-detail/request-detail.component").then((m) => m.RequestDetailComponent),
    canActivate: [authGuard],
    data: {
      roles: ["NOTARY"],
      reloadComponent: true,
    },
  },
  {
    path: "administration/management/onboarding-procedures",
    loadComponent: () =>
      import("../administration/onboarding-procedures-management/onboarding-procedures-management.component").then(
        (m) => m.OnboardingProceduresManagementComponent
      ),
    canActivate: [authGuard],
    data: { roles: ["T2IAA_M"] },
  },
  {
    path: "administration/management/participant",
    loadComponent: () =>
      import("../administration/participant-management/participant-management.component").then(
        (m) => m.ParticipantManagementComponent
      ),
    canActivate: [authGuard],
    data: { roles: ["IATTR_M"] },
  },
  {
    path: "administration/management/participant/detail/:id",
    loadComponent: () =>
      import("../administration/participant-management/participant-detail/participant-detail.component").then(
        (m) => m.ParticipantDetailComponent
      ),
    canActivate: [authGuard],
    data: { roles: ["IATTR_M"] },
  },
  {
    path: "unauthorized",
    loadComponent: () =>
      import("@fe-simpl/landing-page").then((m) => m.UnauthorizedPageComponent)
  },
  {
    path: "error",
    loadComponent: () =>
      import("@fe-simpl/landing-page").then((m) => m.ErrorPageComponent)
  },
  {
    path: "**",
    redirectTo: "application/info",
    pathMatch: "full",
  },
];
