import { Component, Inject, OnInit } from '@angular/core';
import { CommonModule, DOCUMENT } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AlertBannerComponent } from '@fe-simpl/alert-banner';
import { KeycloakService } from 'keycloak-angular';
import { MatIconButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { MatMenu, MatMenuItem, MatMenuTrigger } from '@angular/material/menu';
import { MatToolbar } from '@angular/material/toolbar';

@Component({
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    AlertBannerComponent,
    MatIcon,
    MatMenu,
    MatMenuItem,
    MatIconButton,
    MatMenuTrigger,
    MatToolbar,
  ],
  selector: 'app-onboarding-entry',
  template: ` <div>
    <lib-alert-banner class="position-fixed w-100 p-4 z-1"></lib-alert-banner>
    <mat-toolbar class="h-100">
      <div
        class="d-flex flex-row justify-content-between align-items-center flex-grow-1"
      >
        <div class="font-bold">
          <div>
            <img src="assets/images/logo/simpl-logo.svg" alt="simpl-logo" />
          </div>
        </div>
        <div class="d-flex flex-row gap-2 align-items-center">
          <div>
            <button
              mat-icon-button
              [matMenuTriggerFor]="menu"
              *ngIf="isLoggedIn"
            >
              <mat-icon>account_circle</mat-icon>
            </button>
            <mat-menu #menu="matMenu">
              <button mat-menu-item (click)="logOut()">
                <mat-icon>logout</mat-icon>
                <span>Logout</span>
              </button>
            </mat-menu>
          </div>
        </div>
      </div>
    </mat-toolbar>
    <router-outlet style="background-color: #9bc4fd"></router-outlet>
    <div class="ribbon" *ngIf="role">{{ role }}</div>
  </div>`,
  styles: [
    `
      .ribbon {
        font-size: 28px;
        font-weight: bold;
        color: #fff;

        --r: 0.4em; /* control the ribbon shape (the radius) */
        --c: #002B6E;
        --a: 0.001em;
        position: absolute;
        bottom: 20px;
        right: calc(-1 * var(--a));
        line-height: 1.8;
        padding: calc(2 * var(--r)) 0.5em 0;
        border-radius: 0 var(--r) var(--r) 0;
        background: radial-gradient(100% 50% at right, var(--c) 98%, #0000 101%)
            0 100%/0.5lh calc(100% - 2 * var(--r)),
          radial-gradient(100% 50% at left, #0005 98%, #0000 101%) 100% 0 /
            var(--r) calc(2 * var(--r)),
          conic-gradient(
              at calc(100% - var(--r)) calc(2 * var(--r)),
              var(--c) 75%,
              #0000 0
            )
            100% 0 / calc(101% - 0.5lh) 100%;
        background-repeat: no-repeat;
      }
    `,
  ],
})
export class RemoteEntryComponent implements OnInit {
  loading = false;
  role = '';
  isLoggedIn = false;
  roles: string[] = [];

  constructor(private readonly _keycloakService: KeycloakService) {}

  ngOnInit() {
    this.isLoggedIn = this._keycloakService.isLoggedIn();
    this.roles = this._keycloakService.getUserRoles();
    this.ribbonText();
  }

  logOut() {
    this.loading = true;

    const baseUrl = window.location.origin;
    const onboardingPath = window.location.pathname.includes('/onboarding')
      ? '/onboarding/application/info'
      : '/application/info';

    this._keycloakService.logout(`${baseUrl}${onboardingPath}`).then(() => {
      this.loading = false;
    });
  }

  ribbonText() {
    if (this.roles.includes('APPLICANT')) {
      this.role = 'APPLICANT';
    } else if (this.roles.includes('NOTARY')) {
      this.role = 'NOTARY';
    } else if (this.roles.includes('T1UAR_M')) {
      this.role = 'T1UAR_M';
    } else if (this.roles.includes('IATTR_M')) {
      this.role = 'IATTR_M';
    } else if (this.roles.includes('T2IAA_M')) {
      this.role = 'T2IAA_M';
    } else {
      this.role = 'NO ROLE';
    }
  }
}
