import { APP_INITIALIZER, ApplicationConfig, isDevMode } from '@angular/core';
import {
  provideRouter,
} from '@angular/router';
import { appRoutes } from './app.routes';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { provideTransloco } from '@jsverse/transloco';
import { provideHttpClient, withInterceptors, HTTP_INTERCEPTORS, withInterceptorsFromDi } from '@angular/common/http';
import { environment } from '../environments/environment';
import { I18nService } from '@fe-simpl/i18n';
import { API_URL } from '@fe-simpl/api';
import { provideStore } from '@ngrx/store';
import { errorHandlingInterceptor } from '@fe-simpl/error-handler';
import { KeycloakBearerInterceptor, KeycloakService } from 'keycloak-angular';

export const initializeKeycloak = (keycloak: KeycloakService) => {
  return () =>
    keycloak.init({
      config: {
        clientId: environment.keycloakConfig_clientId,
        realm: environment.keycloakConfig_realm,
        url: environment.keycloakConfig_url,
      },
      initOptions: {
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri: document.baseURI + 'assets/silent-check-sso.html'
      },
      bearerExcludedUrls: ['/public'],
    }).then(r => {
        document.body.classList.add('simpl-splash-screen-hidden');
    });
};

export const appConfig: ApplicationConfig = {
  providers: [
    KeycloakService,
    provideStore(),
    provideRouter(appRoutes),
    provideAnimationsAsync(),
    provideHttpClient(withInterceptorsFromDi(), withInterceptors([errorHandlingInterceptor])),
    provideTransloco({
      config: {
        availableLangs: ['en', 'es'],
        defaultLang: 'en',
        prodMode: !isDevMode(),
      },
      loader: I18nService,
    }),
    { provide: API_URL, useValue: environment.api_Url },
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: KeycloakBearerInterceptor,
      multi: true,
    }
  ],
};
