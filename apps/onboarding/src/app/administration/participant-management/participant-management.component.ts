import { Component, inject, NgZone, signal } from '@angular/core';
import { CommonModule } from "@angular/common";
import { TranslocoDirective, TranslocoService } from "@jsverse/transloco";
import {
  ButtonActions,
  DateFilter,
  OptionFilter,
  PilotType,
  SearchPageComponent,
  SearchPageInfo,
  TextFilter,
} from "@fe-simpl/search-page";
import { ActivatedRoute, Router } from '@angular/router';
import { ParticipantDto, ParticipantTypeEnum } from "@fe-simpl/api-types";
import { MatButtonModule } from "@angular/material/button";
import { MatDialog } from "@angular/material/dialog";
import { ConfirmDialogComponent } from "@fe-simpl/shared-ui/confirm-dialog";
import { AlertBannerService } from "@fe-simpl/alert-banner";
import { ParticipantManagementService } from "./participant-management.service";
import { finalize } from "rxjs";

@Component({
  selector: "app-participant-management",
  standalone: true,
  imports: [CommonModule, TranslocoDirective, SearchPageComponent, MatButtonModule],
  templateUrl: "./participant-management.component.html",
  styles: ``
})
export class ParticipantManagementComponent {
  router = inject(Router);
  private readonly ngZone = inject(NgZone);
  private readonly dialog = inject(MatDialog);
  private readonly translocoService = inject(TranslocoService);
  private readonly banner = inject(AlertBannerService);
  private readonly participantManagementService = inject(ParticipantManagementService);
  private readonly route = inject(ActivatedRoute);

  loading = signal(false);
  pilot: PilotType = {};

  pageInfo: SearchPageInfo = {
    endpoint: "/identity-api/participant/search?hasCredential=true",
    columns: {
      organization: {
        header: "administration.participantName",
      },
      participantType: {
        header: "administration.participant",
        sort: true,
      },
      updateTimestamp: {
        header: "administration.onboardingDate",
        mapper: (date) => new Date(date),
        sort: true,
      },
      actions: {
        header: "simpleInput.actions",
        mapper: () =>
          new ButtonActions([
            {
              icon: "delete",
              theme: "secondary",
              action: (row) => this.revokeAccess(row),
            },
          ]),
      },
    },
    filters: {
      organization: new TextFilter("organization", "filters.organization"),
      participantType: new OptionFilter(
        "participantType",
        "filters.participantType",
        [
          {
            value: "APPLICATION_PROVIDER",
            label: ParticipantTypeEnum.APPLICATION_PROVIDER,
          },
          {
            value: "CONSUMER",
            label: ParticipantTypeEnum.CONSUMER,
          },
          {
            value: "DATA_PROVIDER",
            label: ParticipantTypeEnum.DATA_PROVIDER,
          },
          {
            value: "INFRASTRUCTURE_PROVIDER",
            label: ParticipantTypeEnum.INFRASTRUCTURE_PROVIDER,
          },
        ]
      ),
      dateFrom: new DateFilter("dateFrom", "filters.onboardingDate"),
    },
  };

  rowAction(clickedRow: ParticipantDto) {
    this.ngZone.run(() => {
      this.router.navigate(
        ['/administration/management/participant/detail', clickedRow.id],
        { relativeTo: this.route }
      );
    })

  }

  revokeAccess(row: any) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "simpleInput.confirmDeletion",
        message: this.translocoService.translate(
          "simpleInput.confirmRevokeMessage"
        ),
        confirm: "simpleInput.revoke",
        cancel: "simpleInput.cancel",
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.confirmRevokeAccess(row);
      }
    });
  }

  confirmRevokeAccess(row: any) {
    this.loading.set(true);
    this.participantManagementService
      .revokeAccess(row.id)
      .pipe(finalize(() => this.loading.set(false)))
      .subscribe({
        next: () => {
          console.log('HERE')
          this.banner.show({
            type: "info",
            message: this.translocoService.translate(
              "identityAttributesManagementPage.revoke"
            ),
          });
          this.pilot?.["reload"]();
        },
        error: (err) => {
          console.log('ERROR')
          console.log(err)
          if (err.status === 404) {
            this.banner.show({
              type: "warning",
              message: this.translocoService.translate(
                "identityAttributesManagementPage.alreadyRevoked"
              ),
            });
          }

        },
      });
  }
}
