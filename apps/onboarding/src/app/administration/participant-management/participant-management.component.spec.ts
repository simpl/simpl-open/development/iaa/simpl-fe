import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ParticipantManagementComponent } from './participant-management.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { AppDomain, translocoTestConfiguration } from 'test-environment';
import { By } from '@angular/platform-browser';
import enTranslations from "../../../assets/i18n/en.json";
import { of } from 'rxjs';
import { ApiService } from '@fe-simpl/api';
import { KeycloakService } from 'keycloak-angular';
import { ParticipantDto } from '@fe-simpl/api-types';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { ConfirmDialogComponent } from '@fe-simpl/shared-ui/confirm-dialog';

describe('ParticipantManagementComponent', () => {
  let component: ParticipantManagementComponent;
  let fixture: ComponentFixture<ParticipantManagementComponent>;
  let httpTestingController: HttpTestingController;

  const mockList: Array<ParticipantDto> = [];

  const mockRequestListService = {
    search: jest.fn().mockReturnValue(of({
      content: mockList,
      page: {
        number: 0,
        totalPages: 1,
        totalElements: mockList.length,
        size: 5
      }
    }))
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ParticipantManagementComponent,
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
        NoopAnimationsModule
      ],
      providers: [
        KeycloakService,

        { provide: ApiService, useValue: mockRequestListService },
        { provide: ActivatedRoute, useValue: { snapshot: {} } },
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ParticipantManagementComponent);
    component = fixture.componentInstance;
    httpTestingController = TestBed.inject(HttpTestingController);
    fixture.detectChanges();
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should render correctly', fakeAsync(() => {
    tick();
    fixture?.detectChanges();

    const compiled = fixture?.nativeElement as HTMLElement;
    const title = compiled.querySelector('h1')?.textContent?.trim();
    expect(title).toBe(enTranslations?.administration?.participantList);

    const columnHeaders = fixture?.debugElement?.queryAll(By.css('th'));
    const expectedColumnTitles: Array<string> = [
      enTranslations?.administration?.participantName,
      enTranslations?.administration?.participant,
      enTranslations?.administration?.onboardingDate,
      enTranslations?.simpleInput?.actions
    ];

    expect(columnHeaders?.length).toBe(expectedColumnTitles?.length);

    columnHeaders.forEach((header, index) => {
      const headerText = header.nativeElement.textContent.trim();
      expect(headerText).toBe(expectedColumnTitles[index]);
    });
  }));

  it('should create filters correctly', () => {
    expect(component?.pageInfo?.filters?.['organization']).toBeTruthy();
    expect(component?.pageInfo?.filters?.['participantType']).toBeTruthy();
    expect(component?.pageInfo?.filters?.['dateFrom']).toBeTruthy();
  });

  it('should map date correctly in updateTimestamp column', () => {
    const date = '2023-09-01T12:00:00Z';
    const mapper = component?.pageInfo?.columns?.['updateTimestamp']?.mapper;
    if (mapper) {
      const mappedDate = mapper(date);
      expect(mappedDate).toEqual(new Date(date));
    } else {
      fail('Mapper is undefined');
    }
  });

  it('should navigate to participant detail page on rowAction', () => {
    const routerSpy = jest.spyOn(component.router, 'navigate');
    const mockRow = { id: '123' } as ParticipantDto;
  
    component.rowAction(mockRow);
  
    expect(routerSpy).toHaveBeenCalledWith(
      ['/administration/management/participant/detail', '123'],
      { relativeTo: expect.any(Object) }
    );
  });

  it('should open confirm dialog on revokeAccess', () => {
    const dialogSpy = jest.spyOn(component['dialog'], 'open');
    const mockRow = { id: '123', organization: 'Test' };
  
    component.revokeAccess(mockRow);
  
    expect(dialogSpy).toHaveBeenCalledWith(ConfirmDialogComponent, expect.any(Object));
  });
  
  it('should call revokeAccess service', () => {
    const mockRow = { id: '123' };
    jest.spyOn(component['participantManagementService'], 'revokeAccess').mockReturnValue(of());
  
    component.confirmRevokeAccess(mockRow);
    expect(component['participantManagementService'].revokeAccess).toHaveBeenCalledWith('123');
  });
  
  it('should set loading signal correctly during confirmRevokeAccess', () => {
    const mockRow = { id: '123' };
    jest.spyOn(component['participantManagementService'], 'revokeAccess').mockReturnValue(of());
  
    component.confirmRevokeAccess(mockRow);
  
    expect(component.loading()).toBe(false);
  });

  it('should inject dependencies correctly', () => {
    expect(component.router).toBeTruthy();
    expect(component['dialog']).toBeTruthy();
    expect(component['translocoService']).toBeTruthy();
    expect(component['banner']).toBeTruthy();
    expect(component['participantManagementService']).toBeTruthy();
  });
});
