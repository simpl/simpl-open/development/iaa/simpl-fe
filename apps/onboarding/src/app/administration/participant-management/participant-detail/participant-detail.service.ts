import { HttpClient } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { API_URL, ApiService } from "@fe-simpl/api";

@Injectable({ providedIn: "root" })
export class ParticipantDetailService {
  private readonly http = inject(HttpClient);
  private readonly api_url = inject(API_URL);
  private api = inject(ApiService);

  removeIdentityMultipleAttributeFromParticipant(participantId: string, identityAttributeIds: string[]) {
    return this.api.deleteWithBody(`/sap-api/identity-attribute/unassign-participant/${participantId}`, identityAttributeIds);
  }

  addIdentityAttributesToParticipant(participantId: string, identityAttributeIds: string[]) {
    return this.http.put(`${this.api_url}/sap-api/identity-attribute/assign-participant/${participantId}`, identityAttributeIds);
  }

  checkCredential(participantId: string) {
    return this.http.get(`${this.api_url}/identity-api/participant/${participantId}/credential-validity`);
  }

  renewCredentials(participantId: string) {
    return this.http.put<any>(`${this.api_url}/identity-api/certificate/renew/${participantId}`, {},
      {
        responseType: 'text' as 'json',
        observe: 'response'
      }
    );
  }
}
