import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ParticipantDetailComponent } from './participant-detail.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { ParticipantTypeEnum } from '@fe-simpl/api-types';
import { ActivatedRoute } from '@angular/router';
import { AppDomain, provideApiUrl, translocoTestConfiguration } from '@test-environment';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoTestingModule } from '@jsverse/transloco';
import enTranslations from "../../../../assets/i18n/en.json";
import { By } from '@angular/platform-browser';
import { MatDialog } from '@angular/material/dialog';
import { ParticipantDetailService } from './participant-detail.service';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '@fe-simpl/api';

describe('ParticipantDetailComponent', () => {
  let component: ParticipantDetailComponent;
  let fixture: ComponentFixture<ParticipantDetailComponent>;
  let httpClient: HttpClient;

  const mockActivatedRoute = {
    snapshot: {
      paramMap: {
        get: jest.fn().mockReturnValue('participantId')
      }
    },
    params: of({}),
    queryParams: of({})
  };

  const mockParticipantDetailService = {
    addIdentityAttributesToParticipant: jest.fn().mockReturnValue(of({})),
    removeIdentityMultipleAttributeFromParticipant: jest.fn().mockReturnValue(of({})),
    checkCredential: jest.fn().mockReturnValue(of({})),
    renewCredentials: jest.fn().mockReturnValue(of({
      headers: { get: () => 'attachment; filename="certificate.pem"' },
      body: "fakeContent"
    })),
  };

  const mockDialog = {
    open: jest.fn().mockReturnValue({
      afterClosed: jest.fn().mockReturnValue(of())
    }),
  };

  // Mock dei participant details
  const mockParticipantDetails = {
    id: "someId",
    organization: "Test Org",
    participantType: ParticipantTypeEnum.CONSUMER,
    updateTimestamp: "1995-12-17T04:24:00",
    expiryDate: "1995-12-17T05:24:00"
  };

  // Mock per ApiService usato da SearchPageComponent
  const mockApiService = {
    get: jest.fn().mockReturnValue(of({
      content: [], // deve essere un array
      page: { number: 0, totalPages: 1, totalElements: 0, size: 5 }
    }))
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ParticipantDetailComponent,
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
        NoopAnimationsModule
      ],
      providers: [
        provideApiUrl,
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        { provide: MatDialog, useValue: mockDialog },
        { provide: ParticipantDetailService, useValue: mockParticipantDetailService },
        { provide: ApiService, useValue: mockApiService }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ParticipantDetailComponent);
    component = fixture.componentInstance;
    httpClient = TestBed.inject(HttpClient);

    // Mock della get per participantDetails
    jest.spyOn(httpClient, 'get').mockReturnValue(of(mockParticipantDetails));
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should not display loader when not loading', () => {
    component.loading.set(false);
    fixture.detectChanges();
    const loaderElement = fixture.debugElement.query(By.css('lib-loader'));
    expect(loaderElement).toBeNull();
  });

  it('should render elements', fakeAsync(() => {
    component.ngOnInit();
    tick();
    fixture.detectChanges();

    const compiled = fixture.nativeElement as HTMLElement;
    const title = compiled.querySelector("h1.text-primary") as HTMLElement;
    expect(title).not.toBeNull();

    // Ci aspettiamo 5 campi: id, participantName, participantType, onboardingDate, expiryDate
    const titles = fixture.debugElement.queryAll(By.css('div.col.fw-semibold'));
    expect(titles.length).toBe(5);

    const expectedTitles: Array<string> = [
      enTranslations.administration.id,
      enTranslations.administration.participantName,
      enTranslations.administration.participant,
      enTranslations.administration.onboardingDate,
      enTranslations.administration.expiryDate
    ];

    titles.forEach((title, index) => {
      const cleanTitle = title.nativeElement.textContent.trim();
      expect(cleanTitle).toBe(expectedTitles[index]);
    });

    const addAttributeButton = compiled.querySelector("#addAttributeButton span.mdc-button__label") as HTMLElement;
    expect(addAttributeButton).not.toBeNull();
    expect(addAttributeButton?.textContent?.trim()).toBe(enTranslations.administration.addAttribute);

    const attributesList = fixture.debugElement.query(By.css('lib-search-page'));
    expect(attributesList).toBeTruthy();
  }));


  it('should trigger openIdAttrDialog when addAttributeButton is clicked', fakeAsync(() => {
    component.ngOnInit();
    tick();
    fixture.detectChanges();

    jest.spyOn(component, 'openIdAttrDialog');
    const button = fixture.debugElement.query(By.css('#addAttributeButton'));
    button.triggerEventHandler('click', null);
    expect(component.openIdAttrDialog).toHaveBeenCalled();
  }));

  it('should open dialog and call afterClosed when openIdAttrDialog is triggered', fakeAsync(() => {
    component.ngOnInit();
    tick();
    fixture.detectChanges();

    component.openIdAttrDialog();
    expect(mockDialog.open).toHaveBeenCalled();
    expect(mockDialog.open().afterClosed).toHaveBeenCalled();
  }));

  it('should handle when the participant details data request returns null', fakeAsync(() => {
    jest.spyOn(httpClient, 'get').mockReturnValue(of(null));
    component.ngOnInit();
    tick();
    fixture.detectChanges();

    const searchPageElement = fixture.debugElement.query(By.css('lib-search-page'));
    expect(searchPageElement).toBeFalsy();
  }));

  it('should open confirmation dialog when removeRemoveAttribute is called', () => {
    const mockRow = { code: 'TEST_CODE' };

    component.removeRemoveAttribute(mockRow);

    expect(mockDialog.open).toHaveBeenCalledWith(expect.any(Function), {
      data: {
        title: "simpleInput.confirmDeletion",
        message: expect.any(String),
        confirm: "simpleInput.delete",
        cancel: "simpleInput.cancel",
      },
    });
    expect(mockDialog.open().afterClosed).toHaveBeenCalled();
  });

  it('should handle participantDetails being null', fakeAsync(() => {
    jest.spyOn(httpClient, 'get').mockReturnValue(of(null));
    component.ngOnInit();
    tick();
    fixture.detectChanges();

    const searchPageElement = fixture.debugElement.query(By.css('lib-search-page'));
    expect(searchPageElement).toBeFalsy();
  }));

  it('should call participantDetailsService.removeIdentityMultipleAttributeFromParticipant and show banner when confirmDeleteIAAction is triggered', () => {
    const mockRow = { id: 'attr1' };
    jest.spyOn(component['banner'], 'show');

    component.confirmDeleteIAAction('participantId', mockRow);

    expect(mockParticipantDetailService.removeIdentityMultipleAttributeFromParticipant).toHaveBeenCalledWith('participantId', ['attr1']);
    expect(component['banner'].show).toHaveBeenCalledWith({
      type: 'info',
      message: expect.any(String),
    });
  });

  it('should call participantDetailsService.addIdentityAttributesToParticipant and show banner when addAllIdentityAttributes is called', () => {
    const mockIdAttrIds = ['attr1', 'attr2'];
    jest.spyOn(component['banner'], 'show');

    component.addAllIdentityAttributes('participantId', mockIdAttrIds);

    expect(mockParticipantDetailService.addIdentityAttributesToParticipant).toHaveBeenCalledWith('participantId', mockIdAttrIds);
    expect(component['banner'].show).toHaveBeenCalledWith({
      type: 'info',
      message: expect.any(String),
    });
  });

  it('should not call confirmDeleteIAAction when deletion is cancelled', () => {
    const mockRow = { code: 'TEST_CODE' };

    jest.spyOn(component, 'confirmDeleteIAAction');

    mockDialog.open.mockReturnValue({
      afterClosed: jest.fn().mockReturnValue(of(false))
    });

    component.removeRemoveAttribute(mockRow);

    expect(component.confirmDeleteIAAction).not.toHaveBeenCalled();
  });

  it('should not call confirmDeleteIAAction if participantDetails is null', () => {
    const mockRow = { code: 'TEST_CODE' };

    jest.spyOn(component, 'confirmDeleteIAAction');
    component.participantDetails.set(null);

    mockDialog.open.mockReturnValue({
      afterClosed: jest.fn().mockReturnValue(of(true))
    });

    component.removeRemoveAttribute(mockRow);
    expect(component.confirmDeleteIAAction).not.toHaveBeenCalled();
  });
});
