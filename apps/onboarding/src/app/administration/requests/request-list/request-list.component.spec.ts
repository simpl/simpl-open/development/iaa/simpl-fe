import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RequestListComponent } from './request-list.component';
import { ParticipantDto } from '@fe-simpl/api-types';
import { provideMockStore } from '@ngrx/store/testing';
import { RequestListService, RequestListStore } from '@fe-simpl/shared/requests-store';
import { provideHttpClient } from '@angular/common/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { AppDomain, provideApiUrl, translocoTestConfiguration } from 'test-environment';
import { RemoveUnderscorePipe } from "@fe-simpl/core/pipes";
import { DatePipe } from "@angular/common";
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import enTranslations from "../../../../assets/i18n/en.json";
import { ActivatedRoute } from '@angular/router';

describe('RequestListComponent', () => {
  let component: RequestListComponent;
  let fixture: ComponentFixture<RequestListComponent>;

  const mockList: Array<ParticipantDto> = [
    {
      id: '1',
      participantType: {
        id: 1,
        value: 'CONSUMER',
        label: 'Consumer',
      },
      status: {
        id: '1',
        value: 'IN_PROGRESS',
        label: 'IN PROGRESS',
      },
      organization: "organization",
      applicant: {
        username: 'testuser',
        firstName: 'Test',
        lastName: 'User',
        email: 'user@email.com',
      },
      rejectionCause: 'documents missing',
      creationTimestamp: new Date().toISOString(),
      updateTimestamp: new Date().toISOString(),
      outcomeUserEmail: 'user@email.com',
      expiryDate: new Date().toISOString(),
      lastStatusUpdateTimestamp: new Date().toISOString(),
      expirationTimeframe: 3000,
      documents: [],
      comments: []
    },
  ];

  const mockActivatedRoute = {
    params: of({ id: '123' }),
    queryParams: of({})
  };

  const mockRequestListService = {
    search: jest.fn().mockReturnValue(of({
      content: mockList,
      page: {
        number: 0,
        totalPages: 1,
        totalElements: mockList.length,
        size: 5
      }
    }))
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RequestListComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
        NoopAnimationsModule
      ],
      providers: [
        provideHttpClient(),
        provideApiUrl,
        provideMockStore({ initialState: { list: mockList, loading: false } }),
        {
          provide: RequestListService,
          useValue: mockRequestListService
        },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        RequestListStore
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(RequestListComponent);
    component = fixture.componentInstance;

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render elements', fakeAsync(() => {
    fixture.detectChanges();
    tick();
    const compiled = fixture.nativeElement as HTMLElement;
    const requestListFilters = compiled.querySelector("#requestListFilters") as HTMLElement;
    const requestListTable = compiled.querySelector("#requestListTable") as HTMLElement;

    expect(requestListFilters).not.toBeNull();
    expect(requestListTable).not.toBeNull();

    //table has all columns
    const columns = fixture.debugElement.queryAll(By.css('th'));
    const columnCount = columns.length;
    expect(columnCount).toBe(5);

    //table has all correct titles
    const expectedTitles: Array<string> = [
      enTranslations.administration.status,
      enTranslations.administration.email,
      enTranslations.administration.participant,
      enTranslations.administration.date,
      enTranslations.administration.lastDate
    ]

    columns.forEach((th, index) => {
      const title = th.nativeElement.textContent.trim();
      expect(title).toBe(expectedTitles[index]);
    })

  }));

  it('should load request list in request table correctly', fakeAsync(() => {
    fixture.detectChanges();
    tick();

    const bodyRows = fixture.debugElement.queryAll(By.css("tr[mat-row]"));
    expect(bodyRows.length).not.toBe(0);

    if (bodyRows.length > 0) {
      const resultCells = bodyRows[0].queryAll(By.css("td"));
      expect(resultCells[0]?.nativeElement?.textContent).toContain(new RemoveUnderscorePipe().transform(mockList[0].status.label));
      expect(resultCells[1]?.nativeElement?.textContent).toContain(mockList[0].applicant.email);
      expect(resultCells[2]?.nativeElement?.textContent).toContain(new RemoveUnderscorePipe().transform(mockList[0].participantType.label));
      expect(resultCells[3]?.nativeElement?.textContent).toContain(new DatePipe("en-en").transform(mockList[0].creationTimestamp));
      expect(resultCells[4]?.nativeElement?.textContent).toContain(new DatePipe("en-en").transform(mockList[0].updateTimestamp));
    }
  }));
});
