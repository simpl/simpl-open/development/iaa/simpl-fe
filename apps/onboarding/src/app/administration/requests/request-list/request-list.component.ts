import { Component, inject, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatTableModule } from "@angular/material/table";
import { MatButtonModule } from "@angular/material/button";
import { TranslocoDirective } from "@jsverse/transloco";
import { Subscription } from "rxjs";
import { MatSort, MatSortModule, Sort } from "@angular/material/sort";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatSelectModule } from "@angular/material/select";
import { LoaderComponent } from "@fe-simpl/loader";
import { MatExpansionModule } from "@angular/material/expansion";
import { FiltersListComponent } from "../filters-list/filters-list.component";
import { MatMenuModule } from "@angular/material/menu";
import { RouterModule } from "@angular/router";
import { RequestListStore } from "@fe-simpl/shared/requests-store";



@Component({
  selector: "app-request-list",
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule,
    MatButtonModule,
    MatExpansionModule,
    TranslocoDirective,
    FormsModule,
    MatPaginator,
    MatSortModule,
    MatSelectModule,
    ReactiveFormsModule,
    LoaderComponent,
    FiltersListComponent,
    MatMenuModule,
    RouterModule,
  ],
  templateUrl: "./request-list.component.html",
  styleUrl: "./request-list.component.css",
})
export class RequestListComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = [
    "status",
    "email",
    "participant",
    "creationTimestamp",
    "updateTimestamp",
  ];
  private subscription: Subscription | undefined;
  @ViewChild(MatSort) sort: MatSort;

  private requestListStore = inject(RequestListStore);
  onboardingData = this.requestListStore.list;
  currentPage = this.requestListStore.currentPage;
  pageSize = this.requestListStore.pageSize;
  totalElements = this.requestListStore.totalElements;
  filters = this.requestListStore.filters;

  loading = this.requestListStore.loading;

  ngOnInit() {
    this.fetchOnboardingData();
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  fetchOnboardingData() {
    this.requestListStore.getList();
  }

  onSortChange(sort: Sort) {
    if (sort.active && sort.direction) {
      this.requestListStore.updateFilters({
        sort: [`${sort.active},${sort.direction}`],
      });
    } else {
      this.requestListStore.updateFilters({ sort: [] });
    }
    this.fetchOnboardingData();
  }

  onPageChange(event: PageEvent) {
    if (event.pageSize) {
      this.requestListStore.updateFilters({ size: event.pageSize });
    }
    if (event.pageIndex || event.pageIndex === 0) {
      this.requestListStore.updateFilters({ page: event.pageIndex });
    }
    this.fetchOnboardingData();
  }

}
