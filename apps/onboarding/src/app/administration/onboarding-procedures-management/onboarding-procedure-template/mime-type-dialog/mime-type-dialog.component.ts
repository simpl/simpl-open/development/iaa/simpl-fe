import { Component, EventEmitter, inject, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatError, MatFormField, MatFormFieldModule, MatHint, MatLabel } from '@angular/material/form-field';
import { OnboardingProceduresManagementService } from '../../onboarding-procedures-management.service';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatButton } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { AlertBannerService } from '@fe-simpl/alert-banner';

@Component({
  selector: 'app-mime-type-dialog',
  standalone: true,
  imports: [
    FormsModule,
    MatFormField,
    MatDialogModule,
    MatHint,
    MatLabel,
    CommonModule,
    ReactiveFormsModule,
    TranslocoDirective,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatButton,
    MatFormField,
    MatHint,
    MatLabel
  ],
  templateUrl: './mime-type-dialog.component.html',
  styles: ``
})
export class MimeTypeDialogComponent implements OnInit {
  @Output() mimeListUpdated: EventEmitter<boolean> = new EventEmitter<boolean>()
  mimeTypeForm: FormGroup = new FormGroup({
    value: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required])
  })
  readonly dialogRef = inject(MatDialogRef<MimeTypeDialogComponent>);
  private alertBannerService = inject(AlertBannerService);
  private translocoService = inject(TranslocoService);

  constructor(
    private service: OnboardingProceduresManagementService
  ) {}

  get description() {
    return this.mimeTypeForm.get("description")?.value
  }

  ngOnInit(): void {
    this.dialogRef.updateSize('70%', '40%');
  }

  onSubmit(): void {
    if(this.mimeTypeForm.valid){
      this.service.addMIMEType(this.mimeTypeForm.value).subscribe({
        next: _ => {
          this.alertBannerService.show({
            message: this.translocoService.translate(
              "mimeType.insertSuccess"
            ),
            type: "info",
          })
          this.dialogRef.close(true)
        },
        error: err => {
          this.dialogRef.close(true);
        }
      })
    }
  }
}
