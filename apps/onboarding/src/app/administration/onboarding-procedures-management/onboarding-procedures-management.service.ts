import { inject, Injectable } from "@angular/core";
import { map, Observable } from "rxjs";
import { ApiService } from "@fe-simpl/api";
import { SearchDataSource } from "@fe-simpl/search-page";
import { MIMEType, OnboardingProcedureTemplate, ParticipantType } from "@fe-simpl/api-types";

@Injectable({ providedIn: "root" })
export class OnboardingProceduresManagementService {
  private readonly apiService = inject(ApiService);

  getOnboardingProcedureTemplate(name: string): Observable<OnboardingProcedureTemplate> {
    return this.apiService.get<OnboardingProcedureTemplate>(
      `/onboarding-api/onboarding-template/` + name
    );
  }

  getParticipantTypeList(): Observable<Array<ParticipantType>> {
    return this.apiService.get<Array<ParticipantType>>(
      `/onboarding-api/participant-type`
    );
  }

  getMIMETypeList(): Observable<Array<MIMEType>> {
    return this.apiService.get<SearchDataSource>(
      `/onboarding-api/mime-type`
    ).pipe(
      map(res => res.content as Array<MIMEType>)
    );
  }

  addMIMEType(mimeType: MIMEType){
    return this.apiService.post(
      `/onboarding-api/mime-type`, mimeType
    );
  }

  editOnboardingTemplate(onboardingTemplate: OnboardingProcedureTemplate, participantType: string){
    return this.apiService.put(
      `/onboarding-api/onboarding-template/` + participantType, onboardingTemplate
    );
  }
}
