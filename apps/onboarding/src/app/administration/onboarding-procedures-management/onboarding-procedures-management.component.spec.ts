import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { OnboardingProceduresManagementComponent } from './onboarding-procedures-management.component';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialogModule, MatDialog } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { AppDomain, provideApiUrl, translocoTestConfiguration } from '@test-environment';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { KeycloakService } from 'keycloak-angular';
import { OnboardingProcedureTemplate, ParticipantType, ParticipantTypeEnum } from '@fe-simpl/api-types';
import { of, throwError } from 'rxjs';
import { OnboardingProceduresManagementService } from './onboarding-procedures-management.service';
import { ConfirmDialogComponent } from '@fe-simpl/shared-ui/confirm-dialog';
import { ActivatedRoute } from '@angular/router';

describe('OnboardingProceduresManagementComponent', () => {
  let component: OnboardingProceduresManagementComponent;
  let fixture: ComponentFixture<OnboardingProceduresManagementComponent>;

  const mockTemplate: OnboardingProcedureTemplate = {
    id: "1",
    participantType: ParticipantTypeEnum.CONSUMER,
    description: "Participant ID",
    creationTimestamp: new Date().toISOString(),
    updateTimestamp: new Date().toISOString(),
    documentTemplates: [],
    expirationTimeframe: 22222
  };

  const mockParticipantTypeList: Array<ParticipantType> = [
    {
      id: "1",
      value: "CONSUMER",
      label: ""
    },
    {
      id: "2",
      value: "APPLICATION_PROVIDER",
      label: ""
    },
    {
      id: "3",
      value: "DATA_PROVIDER",
      label: ""
    },
    {
      id: "4",
      value: "INFRASTRUCTURE_PROVIDER",
      label: ""
    }
  ]

  const mockDialog = {
    open: jest.fn().mockReturnValue({
      afterClosed: jest.fn().mockReturnValue(of(true))
    })
  };

  const mockRequestListService = {
    getOnboardingProcedureTemplate: jest.fn().mockReturnValue(of(
      mockTemplate
    )),
    getParticipantTypeList: jest.fn().mockReturnValue(of(
      mockParticipantTypeList
    )),
  };

  const mockActivatedRoute = {
    snapshot: {
      queryParams: {
        tab: '1'
      }
    }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
        NoopAnimationsModule,
        MatDialogModule,
        OnboardingProceduresManagementComponent,
        ConfirmDialogComponent
      ],
      providers: [
        KeycloakService,
        { provide: MatDialogRef, useValue: { close: jest.fn() } },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        provideApiUrl,
        { provide: OnboardingProceduresManagementService, useValue: mockRequestListService },
        { provide: MatDialog, useValue: mockDialog },
        { provide: ActivatedRoute, useValue: mockActivatedRoute }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(OnboardingProceduresManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize properties correctly', fakeAsync(() => {
    expect(component.mode).toEqual("detail");
    tick();
    fixture?.detectChanges();
    expect(component.participantTypeList).toEqual(mockParticipantTypeList)
    expect(component.onboardingProcedureTemplate).toEqual(mockTemplate)
  }));

  it('should handle onEditTemplate correctly', () => {
    component.mode = 'detail';
    component.onEditTemplate();
    expect(component.mode).toBe('edit');
  });

  it('should handle onUpdatedTemplate correctly', () => {
    const getTemplateSpy = jest.spyOn(component, "getOnboardingProcedureTemplate").mockImplementation(()=>{});
    component.onUpdatedTemplate();
    expect(component.loading).toBeTruthy();
    expect(getTemplateSpy).toHaveBeenCalled();
  });

  it('should handle onAddTemplate correctly', fakeAsync(() => {
    component.onAddTemplate();
    expect(component.mode).toEqual("new");
  }));

  it('should handle tab click event', fakeAsync(() => {
    const mockTabId = 'tab1';
    jest.spyOn(component, 'clickTab').mockImplementation(()=>{});
    component.clickTab(mockTabId);
    tick();

    expect(component.clickTab).toHaveBeenCalledWith(mockTabId);
  }));

  it('should handle onBack correctly', () => {
    component.mode = 'detail';
    component.onBack();
    expect(component.mode).toBe('detail');
  });

  it('should display loader when loading is true', () => {
    component.loading = true;
    fixture.detectChanges();

    const loaderElement = fixture.debugElement.query(By.css('lib-loader'));
    expect(loaderElement).toBeTruthy();
  });

  it('should set loading to false on getParticipantTypeList error', fakeAsync(() => {
    mockRequestListService.getParticipantTypeList.mockReturnValue(throwError(() => new Error('Error')));
    fixture.detectChanges();
    tick();
    expect(component.loading).toBe(false);
  }));


  // it('should open dialog on clickTab if mode is edit or new', fakeAsync(() => {
  //   component.mode = 'edit';

  //   tick();
  //   fixture.detectChanges();

  //   if (!component.tabs || component.tabs.length === 0) {
  //     component.tabs = [
  //       { id: '1', label: 'Tab 1' },
  //       { id: '2', label: 'Tab 2' },
  //       { id: '3', label: 'Tab 3' }
  //     ];

  //     fixture.detectChanges();
  //   }

  //   const tabClicked = "3";
  //   const mockEvent = { id: tabClicked };
  //   mockDialog.open.mockReturnValue({
  //     afterClosed: jest.fn().mockReturnValue(of(true))
  //   } as any);

  //   const changeTabSpy = jest.spyOn(component, 'changeTab').mockImplementation(() => {});
  //   component.clickTab(mockEvent);

  //   expect(mockDialog.open).toHaveBeenCalledWith(expect.anything(), {
  //     data: {
  //       title: "onboardingProcedures.changeTab",
  //       message: expect.any(String),
  //       confirm: "onboardingProcedures.changeTabApprove",
  //       cancel: "onboardingProcedures.changeTabCancel"
  //     }
  //   });

  //   tick();
  //   expect(mockDialog.open().afterClosed).toHaveBeenCalled();
  //   expect(changeTabSpy).toHaveBeenCalledWith(tabClicked);
  //   changeTabSpy.mockRestore();
  // }));

  it('should change tab directly on clickTab if mode is not edit or new', fakeAsync(() => {
    component.mode = 'detail';

    tick();
    fixture.detectChanges();

    const mockEvent = { id: '3' };
    const tabChanged = jest.spyOn(component, 'changeTab');
    component.clickTab(mockEvent);

    expect(mockDialog.open).not.toHaveBeenCalled();
    expect(tabChanged).toHaveBeenCalledWith({"id": mockEvent.id});
  }));
});
