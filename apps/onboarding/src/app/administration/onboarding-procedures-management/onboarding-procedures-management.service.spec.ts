import { TestBed } from '@angular/core/testing';
import { MIMEType, OnboardingProcedureTemplate } from '@fe-simpl/api-types';
import { of } from 'rxjs';
import { provideHttpClient } from '@angular/common/http';
import { provideApiUrl } from '@test-environment';
import { ApiService } from '@fe-simpl/api';
import { OnboardingProceduresManagementService } from './onboarding-procedures-management.service';

describe('OnboardingProceduresManagementService', () => {
  let service: OnboardingProceduresManagementService;
  const apiServiceMock = {
    get: jest.fn(),
    put: jest.fn(),
    post: jest.fn()
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideHttpClient(),
        provideApiUrl,
        { provide: ApiService, useValue: apiServiceMock },
      ],
    });
    service = TestBed.inject(OnboardingProceduresManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call getOnboardingProcedureTemplate with correct name', () => {
    const name = 'name';
    apiServiceMock.get.mockReturnValue(of({}));
    service.getOnboardingProcedureTemplate(name).subscribe();
    expect(apiServiceMock.get).toHaveBeenCalledWith(
        `/onboarding-api/onboarding-template/` + name
    );
  });

  it('should call getParticipantTypeList correctly', () => {
    apiServiceMock.get.mockReturnValue(of({}));
    service.getParticipantTypeList().subscribe();
    expect(apiServiceMock.get).toHaveBeenCalledWith(
        `/onboarding-api/participant-type`
    );
  });

  it('should call getMIMETypeList correctly', () => {
    apiServiceMock.get.mockReturnValue(of({}));
    service.getMIMETypeList().subscribe();
    expect(apiServiceMock.get).toHaveBeenCalledWith(
        `/onboarding-api/mime-type`
    );
  });

  it('should call addMIMEType with correct mimeType', () => {
    const mimeType: MIMEType = {
        value: "image/gif",
        description: "GIF format"
    };
    apiServiceMock.post.mockReturnValue(of({}));
    service.addMIMEType(mimeType).subscribe();
    expect(apiServiceMock.post).toHaveBeenCalledWith(
      `/onboarding-api/mime-type`, mimeType
    );
  });

  it('should call editOnboardingTemplate with correct onboardingProcedureTemplate', () => {
    const onboardingProcedureTemplate: OnboardingProcedureTemplate = {
        participantType: "CONSUMER",
        description: "IDs",
        creationTimestamp: "",
        updateTimestamp: "",
        documentTemplates: [],
        expirationTimeframe: 3000
    };
    const participantType = "CONSUMER";
    apiServiceMock.put.mockReturnValue(of({}));
    service.editOnboardingTemplate(onboardingProcedureTemplate, participantType).subscribe();
    expect(apiServiceMock.put).toHaveBeenCalledWith(
      `/onboarding-api/onboarding-template/` + participantType, onboardingProcedureTemplate
    );
  });
});
