import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslocoDirective } from '@jsverse/transloco';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { Tab, TabsComponent } from '@fe-simpl/tabs';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { OnboardingProcedureTemplate } from '@fe-simpl/api-types';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-onboarding-procedure-template-detail',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoDirective,
    MatProgressSpinnerModule,
    MatButtonModule,
    TabsComponent,
    MatChipsModule,
    FormsModule
  ],
  templateUrl: './onboarding-procedure-template-detail.component.html',
  styleUrl: './onboarding-procedure-template-detail.component.css'
})
export class OnboardingProcedureTemplateDetailComponent {
  @Output() edit: EventEmitter<boolean> = new EventEmitter<boolean>(true);
  @Input() template: OnboardingProcedureTemplate
  tabs: Array<Tab> = [
    {
      id: 'DOCUMENT',
      label: 'documents',
    },
  ];

  onEdit(){
    this.edit.emit(true);
  }
}
