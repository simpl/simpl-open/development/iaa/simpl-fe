import { Component, Inject } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogContent,
  MatDialogRef,
  MatDialogTitle
} from '@angular/material/dialog';
import { MatFormField, MatHint, MatLabel } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { MatButton } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { TranslocoDirective } from '@jsverse/transloco';

export interface ConfirmDialogData {
  title: string;
  message: string;
  placeholder: string;
}

@Component({
  selector: 'app-confirm-dialog',
  template: `
    <ng-container *transloco="let t; prefix: 'administration'">
      <h1 mat-dialog-title>{{ data.title }}</h1>
      <div mat-dialog-content>
        <p>{{ data.message }}</p>
        <mat-form-field appearance="outline">
          <mat-label>{{ data.placeholder }}</mat-label>
          <textarea [(ngModel)]="inputValue" matInput [maxLength]="250" #rejectElem="ngModel" required></textarea>
          <mat-hint>{{ rejectElem.value.length + "/ 250" }}</mat-hint>
        </mat-form-field>
      </div>
      <div mat-dialog-actions>
        <button mat-button (click)="onCancel()">{{ t('cancelDialogReject') }}</button>
        <button mat-flat-button color="accent" (click)="onReject()" [disabled]="rejectElem.invalid">{{ t('confirmDialogReject') }}</button>
      </div>
    </ng-container>
  `,
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
    `
  ],
  imports: [
    MatDialogTitle,
    MatDialogContent,
    MatFormField,
    MatInput,
    MatDialogActions,
    MatButton,
    FormsModule,
    MatLabel,
    TranslocoDirective,
    MatHint
  ],
  standalone: true
})
export class RejectDialogComponent {
  inputValue: string = '';

  constructor(
    public dialogRef: MatDialogRef<RejectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogData
  ) {}

  onCancel(): void {
    this.dialogRef.close(null);
  }

  onReject(): void {
    this.dialogRef.close(this.inputValue);
  }
}
