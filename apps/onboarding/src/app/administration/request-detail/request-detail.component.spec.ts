import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RequestDetailComponent } from './request-detail.component';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { AppDomain, provideApiUrl, translocoTestConfiguration } from 'test-environment';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { KeycloakService } from 'keycloak-angular';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { ParticipantDto, ParticipantStatus } from '@fe-simpl/api-types';
import { provideMockStore } from '@ngrx/store/testing';
import { RequestListService } from '@fe-simpl/shared/requests-store';
import { By } from '@angular/platform-browser';
import enTranslations from "../../../assets/i18n/en.json";
import { RemoveUnderscorePipe } from "@fe-simpl/core/pipes";
import { DatePipe } from '@angular/common';

describe('RequestDetailComponent', () => {
  let component: RequestDetailComponent;
  let fixture: ComponentFixture<RequestDetailComponent>;
  let httpTestingController: HttpTestingController;

  const mockList: Array<ParticipantDto> = [
    {
      id: '1',
      participantType: {
        id: 1,
        value: 'CONSUMER',
        label: 'Consumer',
      },
      status: {
        id: '1',
        value: 'IN_PROGRESS',
        label: 'IN PROGRESS',
      },
      organization: "organization",
      applicant: {
        username: 'testuser',
        firstName: 'Test',
        lastName: 'User',
        email: 'user@email.com',
      },
      rejectionCause: 'documents missing',
      creationTimestamp: new Date().toISOString(),
      updateTimestamp: new Date().toISOString(),
      outcomeUserEmail: 'user@email.com',
      expiryDate: new Date().toISOString(),
      lastStatusUpdateTimestamp: new Date().toISOString(),
      expirationTimeframe: 3000,
      documents: [],
      comments: []
    },
  ];

  const mockActivatedRoute = {
    snapshot: {
      paramMap: {
        get: jest.fn().mockReturnValue('user@email.com')
      },
      queryParams: of({ tab: 'DOCUMENTS' })
    },
    params: of({}),
    queryParams: of({ tab: 'DOCUMENTS' })
  };

  const mockRequestListService = {
    search: jest.fn().mockReturnValue(of({
      content: mockList,
      page: {
        number: 0,
        totalPages: 1,
        totalElements: 1,
        size: 5
      }
    }))
  };

  const mockKeycloakService = {
    getKeycloakInstance: jest.fn().mockReturnValue({
      tokenParsed: {
        resourceAccess: {
          someClient: {
            roles: ['USER', 'ADMIN']
          }
        },
        name: 'Test User'
      }
    }),
    getUserRoles: jest.fn().mockReturnValue(['USER', 'ADMIN']),
  };

  const mockHttpClient = {
    get: jest.fn().mockReturnValue(of({})),
    post: jest.fn().mockReturnValue(of({})),
    patch: jest.fn().mockReturnValue(of({})),
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RequestDetailComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
        NoopAnimationsModule
      ],
      providers: [
        { provide: KeycloakService, useValue: mockKeycloakService },
        provideApiUrl,
        provideMockStore({ initialState: { list: mockList, loading: false } }),
        { provide: HttpClient, useValue: mockHttpClient },
        { provide: RequestListService, useValue: mockRequestListService },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(RequestDetailComponent);
    component = fixture.componentInstance;
    component.requestDetail = {
      id: '12345',
      applicant: {
        email: 'applicant@email.com',
      },
      status: {
        value: 'IN_PROGRESS',
        label: 'In Progress',
      },
      documents: [],
      comments: [],
    };
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render elements', fakeAsync(() => {
    component.ngOnInit();
    tick();
    expect(mockRequestListService.search).toHaveBeenCalled();
    fixture.detectChanges();

    const compiled = fixture.nativeElement as HTMLElement;
    const emailTitle = compiled.querySelector("#emailTitle") as HTMLElement;
    const detailFields = compiled.querySelector("#detailFields") as HTMLElement;

    expect(emailTitle).not.toBeNull();
    expect(detailFields).not.toBeNull();

    const titles = fixture.debugElement.queryAll(By.css('.fieldTitle'));
    const fields = fixture.debugElement.queryAll(By.css('.fieldContent'));
    expect(titles.length).toBe(5);
    expect(fields.length).toBe(5);

    const expectedTitles: Array<string> = [
      enTranslations.administration.status,
      enTranslations.administration.rejectionCause,
      enTranslations.administration.email,
      enTranslations.administration.participant,
      enTranslations.administration.date
    ]

    titles.forEach((title, index) => {
      const cleanTitle = title.nativeElement.textContent.trim();
      expect(cleanTitle).toBe(expectedTitles[index]);
    })

    const approveButton = compiled.querySelector("#approveButton") as HTMLElement;
    const rejectButton = compiled.querySelector("#rejectButton") as HTMLElement;
    expect(approveButton).not.toBeNull();
    expect(rejectButton).not.toBeNull();

    expect(approveButton?.textContent?.trim()).toBe(enTranslations.administration.approve);
    expect(rejectButton?.textContent?.trim()).toBe(enTranslations.administration.reject);

    const statusFieldBadge = fixture.debugElement.nativeElement.querySelector("#statusFieldBadge");
    expect(statusFieldBadge).not.toBeNull;

    if(component.requestDetail.status.value === ParticipantStatus.REJECTED){
      expect(
        statusFieldBadge.firstChild.firstChild.classList
      ).toContain("bg-danger");
    } else if(component.requestDetail.status.value === ParticipantStatus.APPROVED){
      expect(statusFieldBadge.firstChild.firstChild.classList
      ).toContain("bg-success");
    }
  }));

  it('should load data correctly', fakeAsync(() => {
    tick();
    fixture.detectChanges();
    const fields = fixture.debugElement.queryAll(By.css('.fieldContent'));

    const statusFieldBadge = fields[0];
    expect(statusFieldBadge.nativeElement.textContent?.trim()).toContain(
      new RemoveUnderscorePipe().transform(mockList[0].status.value)
    );

    const rejectionCause = fields[1];
    expect(rejectionCause?.nativeElement?.textContent?.trim()).toBe('documents missing');

    const emailField = fields[2];
    expect(emailField?.nativeElement?.textContent?.trim()).toBe('user@email.com');

    expect(fields[4]?.nativeElement?.textContent?.trim()).toEqual(
      new DatePipe("en-en").transform(mockList[0]?.creationTimestamp?.trim())
    );
  }));

  it('should download a file when downloadFile is called', () => {
    const downloadSpy = jest.spyOn(component, 'download').mockImplementation();
    const mockResponse = { content: 'mock-content', mimeType: { value: 'application/pdf' } };

    jest.spyOn(component['httpClient'], 'get').mockReturnValue(of(mockResponse));

    component.requestDetail = { id: 'request-id' };
    component.downloadFile('file-id');

    expect(component['httpClient'].get).toHaveBeenCalledWith(
      `${component['api_url']}/onboarding-api/onboarding-request/request-id/document/file-id`
    );
    expect(downloadSpy).toHaveBeenCalledWith(mockResponse);
  });

  it('should update idAttr when onChangeCheckboxSelection is called', () => {
    const mockEvent = ['attr1', 'attr2', 'attr3', 'attr4'];

    component.onChangeCheckboxSelection(mockEvent);

    expect(component.checkboxToCheck).toEqual(['attr1', 'attr2', 'attr3', 'attr4']);
  });

  it('should update the selected tab when clickTab is called', () => {
    const mockEvent = { id: 'COMMENTS' };

    component.clickTab(mockEvent);

    expect(component.tab()).toBe('COMMENTS');
  });

  it('should open approve dialog and call approve service on confirm', () => {
    const dialogSpy = jest.spyOn(component['dialog'], 'open').mockReturnValue({
      afterClosed: () => of(true)
    } as any);
    jest.spyOn(component['httpClient'], 'post').mockReturnValue(of({}));
    const alertSpy = jest.spyOn(component['alertBannerService'], 'show');

    component.requestDetail = { id: 'request-id', applicant: { email: 'test@email.com' } };
    component.openDialogApprove();

    expect(dialogSpy).toHaveBeenCalled();
    expect(component['httpClient'].post).toHaveBeenCalledWith(
      `${component['api_url']}/onboarding-api/onboarding-request/request-id/approve`,
      { identityAttributes: [] }
    );
    expect(alertSpy).toHaveBeenCalledWith(
      expect.objectContaining({ message: expect.any(String), type: 'info' })
    );
  });

  it('should open reject dialog and call reject service on confirm', () => {
    const dialogSpy = jest.spyOn(component['dialog'], 'open').mockReturnValue({
      afterClosed: () => of('Reason for rejection')
    } as any);
    jest.spyOn(component['httpClient'], 'post').mockReturnValue(of({}));
    const alertSpy = jest.spyOn(component['alertBannerService'], 'show');

    component.requestDetail = { id: 'request-id' };
    component.openDialogReject();

    expect(dialogSpy).toHaveBeenCalled();
    expect(component['httpClient'].post).toHaveBeenCalledWith(
      `${component['api_url']}/onboarding-api/onboarding-request/request-id/reject`,
      { rejectionCause: 'Reason for rejection' }
    );
    expect(alertSpy).toHaveBeenCalledWith(
      expect.objectContaining({ message: expect.any(String), type: 'info' })
    );
  });

  it('should retrieve request details and update checkboxToCheck', fakeAsync(() => {
    jest.spyOn(component['requestListService'], 'search').mockReturnValue(
      of({
        content: mockList,
        page: {
          size: 1,
          number: 1,
          totalElements: 1,
          totalPages: 1,
        }
      })
    );
    jest.spyOn(component['httpClient'], 'get').mockReturnValue(
      of({ content: [{ id: 'attr1' }, { id: 'attr2' }] })
    );

    component.retrieveRequestDetails();
    tick();

    expect(component.requestDetail).toEqual(
      expect.objectContaining({ id: '1' })
    );
    expect(component.checkboxToCheck).toEqual([{ id: 'attr1' }, { id: 'attr2' }]);
  }));

  it('should send a comment and update requestDetail.comments', () => {
    const mockComment = { author: 'Test User', content: 'Test comment' };
    jest.spyOn(component['httpClient'], 'patch').mockReturnValue(of(mockComment));
    const alertSpy = jest.spyOn(component['alertBannerService'], 'show');

    component.user = 'Test User';
    component.requestDetail = { id: 'request-id', comments: [] };
    component.sendPostComment('Test comment');

    expect(component['httpClient'].patch).toHaveBeenCalledWith(
      `${component['api_url']}/onboarding-api/onboarding-request/request-id/comment`,
      { author: 'Test User', content: 'Test comment' }
    );
    expect(component.requestDetail.comments).toContainEqual(mockComment);
    expect(alertSpy).toHaveBeenCalledWith(
      expect.objectContaining({ type: 'success' })
    );
  });

  it('should open new document dialog and add document on confirm', () => {
    const dialogSpy = jest.spyOn(component['dialog'], 'open').mockReturnValue({
      afterClosed: () => of({ mimeType: { id: 'mime1' }, description: 'New Document' })
    } as any);
    jest.spyOn(component['httpClient'], 'post').mockReturnValue(of({}));
    const alertSpy = jest.spyOn(component['alertBannerService'], 'show');

    component.requestDetail = { id: 'request-id', documents: [] };
    component.requestNewDocument();

    expect(dialogSpy).toHaveBeenCalled();
    expect(component['httpClient'].post).toHaveBeenCalledWith(
      `${component['api_url']}/onboarding-api/onboarding-request/request-id/document`,
      { mimeType: { id: 'mime1' }, description: 'New Document' }
    );
    expect(alertSpy).toHaveBeenCalledWith(
      expect.objectContaining({ type: 'success' })
    );
  });

  it('should open revision dialog and update request status on confirm', () => {
    const dialogSpy = jest.spyOn(component['dialog'], 'open').mockReturnValue({
      afterClosed: () => of(true)
    } as any);
    jest.spyOn(component['httpClient'], 'post').mockReturnValue(of({}));
    const alertSpy = jest.spyOn(component['alertBannerService'], 'show');

    component.requestDetail = { id: 'request-id', status: { value: '', label: '' } };
    component.RequestRevision();

    expect(dialogSpy).toHaveBeenCalled();
    expect(component['httpClient'].post).toHaveBeenCalledWith(
      `${component['api_url']}/onboarding-api/onboarding-request/request-id/request-revision`,
      {}
    );
    expect(component.requestDetail.status).toEqual(
      expect.objectContaining({ value: 'IN_PROGRESS', label: 'IN PROGRESS' })
    );
    expect(alertSpy).toHaveBeenCalledWith(
      expect.objectContaining({ type: 'success' })
    );
  });
});
