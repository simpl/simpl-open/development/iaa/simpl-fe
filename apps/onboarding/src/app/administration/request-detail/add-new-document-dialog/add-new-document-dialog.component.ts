import { Component, Inject, OnInit } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogContent,
  MatDialogRef,
  MatDialogTitle
} from '@angular/material/dialog';
import { MatFormField, MatHint, MatLabel } from '@angular/material/form-field';
import { MatError, MatInput } from '@angular/material/input';
import { MatButton } from '@angular/material/button';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { TranslocoDirective } from '@jsverse/transloco';
import { NgIf } from '@angular/common';
import {
  OnboardingProceduresManagementService
} from '../../onboarding-procedures-management/onboarding-procedures-management.service';
import { MatAutocomplete, MatAutocompleteTrigger, MatOption } from '@angular/material/autocomplete';
import { MIMEType } from '@fe-simpl/api-types';

export interface ConfirmDialogNewDocumentData {
  title: string;
  message: string;
}

@Component({
  selector: 'app-confirm-dialog',
  template: `
    <ng-container *transloco="let t; prefix: 'administration'">
      <h1 mat-dialog-title>{{ data.title }}</h1>
      <div mat-dialog-content>
        <p>{{ data.message }}</p>
      </div>
      <form [formGroup]="form" class="p-4 d-flex flex-row gap-2">

        <mat-form-field appearance="outline">
          <mat-label>Description</mat-label>
          <input matInput formControlName="description" required/>
          <mat-error *ngIf="form.get('description')?.hasError('required')">
            {{ t('descriptionRequired') }}
          </mat-error>
        </mat-form-field>

        <div class="w-26">
          <mat-form-field appearance="outline">
            <mat-label>{{ t('mimeType') }}</mat-label>
            <input type="text" placeholder="{{ t('mimeType') }}"
                   matInput formControlName="mimeType" [matAutocomplete]="auto"
                   (input)="filterMimeTypes($event)" (focus)="filterMimeTypes()" required>
            <mat-autocomplete requireSelection #auto="matAutocomplete" [displayWith]="getMimeValue">
              @for (filteredMime of filteredMimeTypes; track filteredMime) {
                <mat-option [value]="filteredMime">{{filteredMime.value}}</mat-option>
              }
            </mat-autocomplete>
            <mat-error *ngIf="form.get('mimeType')?.hasError('required')">
              {{ t('mimeTypeRequired') }}
            </mat-error>
          </mat-form-field>
        </div>

      </form>

      <div mat-dialog-actions class="px-4">
        <button mat-button (click)="onCancel()">{{ t('cancelDialogNewDocument') }}</button>
        <button mat-flat-button color="accent" (click)="onSave()" [disabled]="form.invalid">{{ t('confirmDialogNewDocument') }}</button>
      </div>
    </ng-container>
  `,
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
    `
  ],
  imports: [
    MatDialogTitle,
    MatDialogContent,
    MatFormField,
    MatInput,
    MatDialogActions,
    MatButton,
    FormsModule,
    MatLabel,
    TranslocoDirective,
    NgIf,
    ReactiveFormsModule,
    MatError,
    MatAutocomplete,
    MatAutocompleteTrigger,
    MatOption
  ],
  standalone: true
})
export class AddNewDocumentDialogComponent implements OnInit {
  form!: FormGroup;
  filteredMimeTypes: Array<MIMEType> = [];
  mimeTypes:Array<MIMEType> =[];

  constructor(
    public dialogRef: MatDialogRef<AddNewDocumentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogNewDocumentData,
    private _formBuilder: FormBuilder,
    private _onboardingProceduresManagementService: OnboardingProceduresManagementService
  ) {}

  ngOnInit(): void {

    this._onboardingProceduresManagementService.getMIMETypeList().subscribe({
      next: list => {
        this.mimeTypes = list;
      }
    })

    this.form = this._formBuilder.group({
      description: ['', Validators.required],
      mandatory: [true],
      mimeType: ['', Validators.required],
    });
  }

  onCancel(): void {
    this.dialogRef.close(null);
  }

  onSave() {
    this.dialogRef.close(this.form.getRawValue())
  }

  filterMimeTypes(event?: any) {
    if (event) {
      const filter = event.target?.value?.toLowerCase() ?? ""
      this.filteredMimeTypes = this.mimeTypes.filter(mimeType => mimeType.value.toLowerCase().startsWith(filter));
    } else {
      this.filteredMimeTypes = this.mimeTypes
    }
  }

  getMimeValue(event: any) : string{
    return event?.value ?? ''
  }
}
