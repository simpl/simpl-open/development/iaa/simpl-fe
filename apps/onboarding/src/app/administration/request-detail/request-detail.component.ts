import { Component, inject, OnInit, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslocoDirective, TranslocoService } from '@jsverse/transloco';
import { MatTableModule } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { RequestListProperties } from '@fe-simpl/api-types';
import { LoaderComponent } from '@fe-simpl/loader';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '@fe-simpl/shared-ui/confirm-dialog';
import { RequestListService } from '@fe-simpl/shared/requests-store';
import { AlertBannerService } from '@fe-simpl/alert-banner';
import { SearchPageComponent, SearchPageInfo } from '@fe-simpl/search-page';
import { MatButtonModule } from '@angular/material/button';
import { CommentsComponent } from '@fe-simpl/comments';
import { KeycloakService } from 'keycloak-angular';
import { HttpClient } from '@angular/common/http';
import { map, of, switchMap, tap } from 'rxjs';
import { API_URL } from '@fe-simpl/api';
import { ConfirmDialogData, RejectDialogComponent } from './reject-dialog/reject-dialog.component';
import { Tab, TabsComponent } from '@fe-simpl/tabs';
import { MatChip, MatChipSet } from '@angular/material/chips';
import { MatIcon } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import {
  AddNewDocumentDialogComponent,
  ConfirmDialogNewDocumentData
} from './add-new-document-dialog/add-new-document-dialog.component';
import { OnboardingRequestDTO } from '../../models/onboarding.types';
import { ToDatePipe } from '../../override-date.pipe';
import { MatDivider } from '@angular/material/divider';

@Component({
  selector: 'app-request-detail',
  standalone: true,
  imports: [
    CommonModule,
    TranslocoDirective,
    MatTableModule,
    LoaderComponent,
    SearchPageComponent,
    MatButtonModule,
    CommentsComponent,
    TabsComponent,
    MatChip,
    MatChipSet,
    MatIcon,
    ReactiveFormsModule,
    ToDatePipe,
  ],
  templateUrl: './request-detail.component.html',
  styleUrl: './request-detail.component.css',
})
export class RequestDetailComponent implements OnInit {
  private readonly activatedRoute: ActivatedRoute = inject(ActivatedRoute);
  private readonly translocoService = inject(TranslocoService);
  private readonly requestListService = inject(RequestListService);
  private readonly alertBannerService = inject(AlertBannerService);
  private readonly dialog = inject(MatDialog);
  private readonly keycloakService = inject(KeycloakService);
  private readonly httpClient = inject(HttpClient);
  private readonly api_url = inject(API_URL);

  user: string;
  requestDetail: any;
  roles: string[] = [];
  loading = signal<boolean>(false);
  today = new Date();
  checkboxToCheck: any[] = [];
  enabledIdentityAttributesTablepageInfo: SearchPageInfo = {
    endpoint: '/sap-api/identity-attribute/search?page=0&size=1000',
    columns: {
      id: {
        header: 'administration.id',
      },
      code: {
        header: 'administration.code',
      },
      assignableToRoles: {
        header: 'administration.assignableToRoles',
        mapper: (item: boolean) => {
          if (typeof item === 'boolean') {
            return item
              ? this.translocoService.translate('common.yes')
              : this.translocoService.translate('common.no');
          }
          return item;
        }
      },
      enabled: {
        header: 'checkbox',
        isCheckbox: true,
      },
    },
  };
  tab = signal('DOCUMENTS');
  tabs: Array<Tab> = [
    {
      id: 'DOCUMENTS',
      label: 'Documents',
      primary: true,
    },
    {
      id: 'COMMENTS',
      label: 'Comments',
    },
    {
      id: 'IDATTR',
      label: 'Identity attributes',
    },
  ];
  displayedColumns: string[] = ['id', 'code', 'assignableToRoles'];
  dataSource: any[] = [];
  ngOnInit() {
    this.retrieveRequestDetails();
    this.user =
      this.keycloakService.getKeycloakInstance()?.tokenParsed?.['name'];
    this.roles = this.keycloakService.getUserRoles();
    this.tab.set(
      this.activatedRoute.snapshot.queryParams['tab'] ?? 'DOCUMENTS'
    );
  }

  openDialogApprove() {

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'administration.approve-title',
        message: this.translocoService.translate(
          'administration.approve-request-detail-modal-message',
          { email: this.requestDetail.applicant.email }
        ),
        idAttrMessage1: 'administration.idAttrMessage1',
        idAttrMessage2: 'administration.idAttrMessage2',
        confirm: 'administration.approve',
        cancel: 'administration.cancel',
        data: this.checkboxToCheck,
        extraMessage: true
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const body = {
          identityAttributes: this.checkboxToCheck.map((el) => el.id),
        };
        this.loading.set(true);
        this.httpClient
          .post<OnboardingRequestDTO>(
            `${this.api_url}/onboarding-api/onboarding-request/${this.requestDetail.id}/approve`,
            body
          )
          .pipe()
          .subscribe({
            next: (result) => {
              this.requestDetail = result;
              this.dataSource = [...this.checkboxToCheck];
              this.alertBannerService.show({
                message: this.translocoService.translate(
                  'administration.approveSuccess'
                ),
                type: 'info',
                autoDismissable: true,
              });
            },
            error: (error) => {
              this.alertBannerService.show({
                message: this.translocoService.translate(
                  'administration.errorApprove'
                ),
                type: 'danger',
                autoDismissable: true,
              });
            },
            complete: () => {
              this.loading.set(false);
            },
          });
      }
    });
  }

  openDialogReject() {
    const dialogData: ConfirmDialogData = {
      title: this.translocoService.translate('administration.titleReject'),
      message: this.translocoService.translate('administration.messageReject'),
      placeholder: this.translocoService.translate(
        'administration.placeholderInputReject'
      ),
    };

    const dialogRef = this.dialog.open(RejectDialogComponent, {
      width: '600px',
      data: dialogData,
      hasBackdrop: false,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result !== null) {
        const body = {
          rejectionCause: result,
        };
        this.loading.set(true);
        this.httpClient
          .post<OnboardingRequestDTO>(
            `${this.api_url}/onboarding-api/onboarding-request/${this.requestDetail.id}/reject`,
            body
          )
          .pipe()
          .subscribe({
            next: (result) => {
              this.requestDetail = result;
              this.alertBannerService.show({
                message: this.translocoService.translate(
                  'administration.rejectSuccess'
                ),
                type: 'info',
                autoDismissable: true,
              });
            },
            error: (error) => {
              this.alertBannerService.show({
                message: this.translocoService.translate(
                  'administration.errorReject'
                ),
                type: 'danger',
                autoDismissable: true,
              });
            },
            complete: () => {
              this.loading.set(false);
            },
          });
      }
    });
  }

  retrieveRequestDetails() {
    const email = this.activatedRoute.snapshot.paramMap.get('email')!;
    const params: Partial<RequestListProperties> = {
      email,
      size: 1,
    };
    this.loading.set(true);
    this.requestListService
      .search(params as RequestListProperties)
      .pipe(
        switchMap((data) => {
          if (data.content?.length && data.content.length > 0) {
            this.requestDetail = data.content[0];
            if (this.requestDetail.status.value !== 'APPROVED') {
              return this.httpClient
                .get(
                  `${this.api_url}/sap-api/identity-attribute/search?page=0&size=1000&participantTypeIn=${this.requestDetail.participantType.value}`
                )
                .pipe(
                  map((result: any) => {
                    return result.content;
                  })
                );
            } else {
              return this.httpClient
                .get(
                  `${this.api_url}/sap-api/identity-attribute/search?page=0&size=1000&participantIdIn=${this.requestDetail.participantId}`
                )
                .pipe(
                  map((result: any) => {
                    this.dataSource = [...result.content];
                    return result.content;
                  })
                );
            }
          }
          return of([]);
        })
      )
      .subscribe({
        next: (res: any) => {
          if (res) {
            this.checkboxToCheck = [...res];
          }
        },
        complete: () => this.loading.set(false),
      });
  }

  onChangeCheckboxSelection($event: string[] | any) {
    this.checkboxToCheck = [...$event];
  }

  downloadFile(idFile: string) {
    this.httpClient
      .get(
        `${this.api_url}/onboarding-api/onboarding-request/${this.requestDetail?.id}/document/${idFile}`
      )
      .pipe(
        tap((data) => {
          this.download(data);
        })
      )
      .subscribe();
  }

  download(documentToDownload: any) {
    const base64Content = documentToDownload.content;
    const mimeType = documentToDownload.documentTemplate
      ? documentToDownload.documentTemplate.mimeType
      : documentToDownload.mimeType;

    const byteCharacters = atob(base64Content);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);

    const blob = new Blob([byteArray], { type: mimeType.value });

    const objectURL = URL.createObjectURL(blob);
    const fileLink = document.createElement('a');
    fileLink.href = objectURL;
    const description = documentToDownload.documentTemplate
      ? documentToDownload.documentTemplate.description
      : documentToDownload.description;
    fileLink.download = `${description}.${mimeType.value.split('/')[1]}`;
    fileLink.click();

    URL.revokeObjectURL(objectURL);
  }

  sendPostComment(event: string) {
    const bodyComment = {
      author: this.user,
      content: event,
    };
    this.httpClient
      .patch(
        `${this.api_url}/onboarding-api/onboarding-request/${this.requestDetail.id}/comment`,
        bodyComment
      )
      .pipe(
        tap((comment) => {
          this.requestDetail.comments = [
            ...(this.requestDetail.comments ?? []),
            bodyComment,
          ];
        })
      )
      .subscribe({
        next: () => {
          this.alertBannerService.show({
            message: this.translocoService.translate(
              'administration.leaveCommment'
            ),
            type: 'success',
            autoDismissable: true,
          });
        },
        error: (error) => {
          this.alertBannerService.show({
            message: this.translocoService.translate(
              'administration.errorLeaveCommment'
            ),
            type: 'danger',
            autoDismissable: true,
          });
        },
      });
  }

  clickTab(event: any) {
    this.tab.set(event.id);
  }

  requestNewDocument() {
    const dialogData: ConfirmDialogNewDocumentData = {
      title: this.translocoService.translate('administration.titleNewDocument'),
      message: this.translocoService.translate(
        'administration.messageNewDocument'
      ),
    };

    const dialogRef = this.dialog.open(AddNewDocumentDialogComponent, {
      width: '800px',
      data: dialogData,
      hasBackdrop: false,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result !== null) {
        const newDocument = {
          mimeType: {
            id: result.mimeType.id,
          },
          description: result.description,
        };
        this.httpClient
          .post<any>(
            `${this.api_url}/onboarding-api/onboarding-request/${this.requestDetail.id}/document`,
            newDocument
          )
          .pipe(
            tap((document) => {
              this.requestDetail.documents = [
                ...(this.requestDetail.documents ?? []),
                newDocument,
              ];
            })
          )
          .subscribe({
            next: () => {
              this.alertBannerService.show({
                message: this.translocoService.translate(
                  'administration.savedNewDocument'
                ),
                type: 'success',
                autoDismissable: true,
              });
            },
            error: (error) => {
              this.alertBannerService.show({
                message: this.translocoService.translate(
                  'administration.errorNewDocument'
                ),
                type: 'danger',
                autoDismissable: true,
              });
            },
          });
      }
    });
  }

  RequestRevision() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'administration.requestRevisionTitle',
        message: 'administration.requestRevisionMessage',
        confirm: 'administration.requestRevisionApprove',
        cancel: 'administration.requestRevisionCancel',
      },
      width: '800px',
      hasBackdrop: false,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.httpClient
          .post(
            `${this.api_url}/onboarding-api/onboarding-request/${this.requestDetail.id}/request-revision`,
            {}
          )
          .pipe()
          .subscribe({
            next: () => {
              this.requestDetail.status.value = 'IN_PROGRESS';
              this.requestDetail.status.label = 'IN PROGRESS';
              this.alertBannerService.show({
                message: this.translocoService.translate(
                  'administration.requestInformationOK'
                ),
                type: 'success',
                autoDismissable: true,
              });
            },
            error: (error) => {
              this.alertBannerService.show({
                message: this.translocoService.translate(
                  'administration.requestInformationError'
                ),
                type: 'danger',
                autoDismissable: true,
              });
            },
          });
      }
    });
  }
}
