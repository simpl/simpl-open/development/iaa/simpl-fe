import { ComponentFixture, TestBed } from "@angular/core/testing";
import { InfoPageComponent } from "./info-page.component";
import { TranslocoTestingModule } from "@jsverse/transloco";
import { AppDomain, translocoTestConfiguration } from "@test-environment";
import { ActivatedRoute, Router } from '@angular/router';

const mockActivatedRoute = {
  snapshot: jest.fn(),
};

describe("InfoApplicationComponent", () => {
  let component: InfoPageComponent;
  let fixture: ComponentFixture<InfoPageComponent>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        InfoPageComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
      ],
      providers: [
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(InfoPageComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should navigate to /application/request", () => {
    const navigateSpy = jest.spyOn(router, "navigate");
    component.navigateToRequest();
    expect(navigateSpy).toHaveBeenCalledWith(["/application/request"], { relativeTo: mockActivatedRoute });
  });
});
