import { inject, Injectable } from "@angular/core";
import { ApiService } from "@fe-simpl/api";
import { OnboardingData } from '../../models/onboarding.types';

@Injectable({ providedIn: "root" })
export class OnboardingStatusService {
  private apiService = inject(ApiService);

  fetchOnboardingStatus() {
    return this.apiService.get<OnboardingData>("/onboarding-api/onboarding-request");
  }
}
