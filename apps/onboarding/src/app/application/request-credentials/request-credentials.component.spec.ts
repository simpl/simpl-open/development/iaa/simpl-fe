import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RequestCredentialsComponent } from "./request-credentials.component";
import { TranslocoTestingModule } from "@jsverse/transloco";
import { provideHttpClient } from "@angular/common/http";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import {
  AppDomain,
  provideApiUrl,
  translocoTestConfiguration,
} from "@test-environment";
import { ActivatedRoute } from '@angular/router';

describe("RequestApplicationComponent", () => {
  let component: RequestCredentialsComponent;
  let fixture: ComponentFixture<RequestCredentialsComponent>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RequestCredentialsComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.ONBOARDING)
        ),
        NoopAnimationsModule,
      ],
      providers: [provideApiUrl, provideHttpClient(),
        { provide: ActivatedRoute, useValue: { snapshot: {} } },],
    }).compileComponents();

    fixture = TestBed.createComponent(RequestCredentialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("ApplicationForm should be initialized and rendered correctly", () => {
    const applicationFormElement =
      fixture.debugElement.nativeElement.querySelector("#applicationForm");

    // Getting HTML Elements
    const emailElement: HTMLInputElement =
      applicationFormElement.querySelector("#email");
    const organizationElement: HTMLInputElement =
      applicationFormElement.querySelector("#organization");
    const participantTypeElement =
      applicationFormElement.querySelector("#participantType");
    const surnameElement: HTMLInputElement =
      applicationFormElement.querySelector("#surname");
    const nameElement: HTMLInputElement =
      applicationFormElement.querySelector("#name");
    const usernameElement: HTMLInputElement =
      applicationFormElement.querySelector("#username");
    const passwordElement: HTMLInputElement =
      applicationFormElement.querySelector("#password");
    const confirmPasswordElement: HTMLInputElement =
      applicationFormElement.querySelector("#confirmPassword");

    fixture.detectChanges();
    expect(applicationFormElement).not.toBeNull();
    // Checking if all UI Elements are rendered
    expect(emailElement).not.toBeNull();
    expect(organizationElement).not.toBeNull();
    expect(participantTypeElement).not.toBeNull();
    expect(surnameElement).not.toBeNull();
    expect(nameElement).not.toBeNull();
    expect(usernameElement).not.toBeNull();
    expect(passwordElement).not.toBeNull();
    expect(confirmPasswordElement).not.toBeNull();
  });

  it("Password and Confirm Password fields should have the same value", () => {
    // Getting HTML Element
    const applicationFormElement =
      fixture.debugElement.nativeElement.querySelector("#applicationForm");
    const passwordElement: HTMLInputElement =
      applicationFormElement.querySelector("#password");
    const confirmPasswordElement: HTMLInputElement =
      applicationFormElement.querySelector("#confirmPassword");

    // Setting the same value for both input fields
    passwordElement.value = "Asdf1234$$";
    passwordElement.dispatchEvent(new Event("input"));
    confirmPasswordElement.value = "Asdf1234$$";
    confirmPasswordElement.dispatchEvent(new Event("input"));

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      // Checking that there isn't a noMatch error on the form
      expect(component.applicationForm.errors).toBeNull();
      expect(component.applicationForm.errors?.["noMatch"]).toBeFalsy();
    });
    // Setting a wrong value
    passwordElement.value = "Asdf1234$$";
    passwordElement.dispatchEvent(new Event("input"));
    confirmPasswordElement.value = "$$4321fdsA";
    confirmPasswordElement.dispatchEvent(new Event("input"));

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      // Checking that there is a noMatch error on the form
      expect(component.applicationForm.errors).not.toBeNull();
      expect(component.applicationForm.errors?.["noMatch"]).toBeTruthy();
    });
  });

  it("Password field should satisfy all the requirements", () => {
    const applicationFormElement =
      fixture.debugElement.nativeElement.querySelector("#applicationForm");

    // Setting up values for forbidden validation
    component.applicationForm.setValue({
      applicant: {
        username: "",
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirmPassword: ""
      },
      participantType: {
        id: ""
      },
      organization: ""
    });

    // Getting password html element and form control
    const passwordElement: HTMLInputElement =
      applicationFormElement.querySelector("#password");
    const passwordFormControl = component.applicationForm.get("applicant.password");

    // Assert password minimum length
    passwordElement.value = "a";
    passwordElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    expect(passwordFormControl?.valid).toBeFalsy();
    expect(passwordFormControl?.errors?.["minLength"]).toBeTruthy();

    // Assert password maximum Length
    passwordElement.value =
      "ababababababababababababababababababababababababababababababababab";
    passwordElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    expect(passwordFormControl?.valid).toBeFalsy();
    expect(passwordFormControl?.errors?.["maxLength"]).toBeTruthy();

    // Assert password has at least one uppercase
    passwordElement.value = "abababa";
    passwordElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    expect(passwordFormControl?.valid).toBeFalsy();
    expect(passwordFormControl?.errors?.["uppercase"]).toBeTruthy();

    // Assert password has at least one lowercase
    passwordElement.value = "ABABABA";
    passwordElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    expect(passwordFormControl?.valid).toBeFalsy();
    expect(passwordFormControl?.errors?.["lowercase"]).toBeTruthy();

    // Assert password has at least one specialChar
    passwordElement.value = "ABABABA";
    passwordElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    expect(passwordFormControl?.valid).toBeFalsy();
    expect(passwordFormControl?.errors?.["specialChar"]).toBeTruthy();

    // Assert password is valid
    passwordElement.value = "Asdf1234!!";
    passwordElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    expect(passwordFormControl?.valid).toBeTruthy();
  });

  it("should do submit logic correctly", () => {
    const applicationFormElement =
      fixture.debugElement.nativeElement.querySelector("#applicationForm");

    // Getting HTML Elements
    const emailElement: HTMLInputElement =
      applicationFormElement.querySelector("#email");
    const organizationElement: HTMLInputElement =
      applicationFormElement.querySelector("#organization");
    const surnameElement: HTMLInputElement =
      applicationFormElement.querySelector("#surname");
    const nameElement: HTMLInputElement =
      applicationFormElement.querySelector("#name");
    const usernameElement: HTMLInputElement =
      applicationFormElement.querySelector("#username");
    const passwordElement: HTMLInputElement =
      applicationFormElement.querySelector("#password");
    const confirmPasswordElement: HTMLInputElement =
      applicationFormElement.querySelector("#confirmPassword");
    const submitButton: HTMLInputElement =
      applicationFormElement.querySelector("#submitButton");

    // Setting a correct value
    emailElement.value = "simpl.participant_agent-authority@gmail.com";
    emailElement.dispatchEvent(new Event("input"));

    fixture.detectChanges();
    // Checking for email restrictions to be satisfied
    expect(component.applicationForm.get("applicant.email")?.valid).toBeTruthy();
    organizationElement.value = "MarioRossi Org";
    organizationElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    // TODO Simulate user input on select
    component.applicationForm.get("participantType.id")?.patchValue("2");
    surnameElement.value = "Mario";
    surnameElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    nameElement.value = "Rossi";
    nameElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    usernameElement.value = "Mario_Rossi";
    usernameElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    passwordElement.value = "Asdf1234!!";
    passwordElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    confirmPasswordElement.value = "Asdf1234!!";
    confirmPasswordElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    const submitSpy = jest.spyOn(component, "onSubmit");
    submitButton.click();
    fixture.detectChanges();

    expect(component.applicationForm.valid).toBeTruthy();
    expect(submitSpy).toHaveBeenCalled();
  });

  it("Email field should be validated", () => {
    const applicationFormElement =
      fixture.debugElement.nativeElement.querySelector("#applicationForm");
    // Getting email html element and form control
    const emailElement: HTMLInputElement =
      applicationFormElement.querySelector("#email");
    const emailFormControl = component.applicationForm.get("applicant.email");

    // Setting a wrong value
    emailElement.value = "email#gmailcom";
    emailElement.dispatchEvent(new Event("input"));
    fixture.detectChanges();
    // Checking for email restrictions to be not satisfied
    expect(emailFormControl?.valid).toBeFalsy();
    // Setting a crrrect value
    emailElement.value = "simpl.participant_agent-authority@gmail.com";
    emailElement.dispatchEvent(new Event("input"));

    fixture.detectChanges();
    // Checking for email restrictions to be satisfied
    expect(emailFormControl?.valid).toBeTruthy();
    expect(emailFormControl?.errors?.["email"]).toBeFalsy();
  });
});
