import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  standalone: true,
  name: 'toDate'
})
export class ToDatePipe implements PipeTransform {
  transform(value: string): Date | null {
    return new Date(value);
  }
}
