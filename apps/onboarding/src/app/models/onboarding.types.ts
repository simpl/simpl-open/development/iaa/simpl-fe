export type OnboardingStatusValue = "IN_PROGRESS" | "IN_REVIEW" | "REJECTED" | "APPROVED";

export type DateAsString = string;

export interface OnboardingRequestDTO {
  id: string;
  participantId: string;
  status: OnboardingStatusDTO;
  applicant: OnboardingApplicantDTO;
  participantType: ParticipantTypeDTO;
  organization: string;
  documents: DocumentDTO[];
  comments: CommentDTO[];
  rejectionCause: string;
  expirationTimeframe: number;
  creationTimestamp: DateAsString;
  updateTimestamp: DateAsString;
  lastStatusUpdateTimestamp: DateAsString;
}

export interface OnboardingStatusDTO {
  id: number;
  label: string;
  value: OnboardingStatusValue;
}

export interface OnboardingApplicantDTO {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface ParticipantTypeDTO {
  id: number;
  value: string;
  label: string;
}

export interface DocumentDTO extends DocumentModel {
  id: string;
  documentTemplate: DocumentTemplateDTO;
  mimeType: MimeTypeDTO;
  description: string;
  filename: string;
  creationTimestamp: DateAsString;
  updateTimestamp: DateAsString;
  content: string;
}

export interface CommentDTO {
  id?: string;
  author: string;
  content: string;
  creationTimestamp?: DateAsString;
  updateTimestamp?: DateAsString;
}

export interface DocumentTemplateDTO {
  id: string;
  description: string;
  mandatory: boolean;
  mimeType: MimeTypeDTO;
  creationTimestamp: DateAsString;
  updateTimestamp: DateAsString;
}

export interface MimeTypeDTO {
  id: number;
  value: string;
  description: string;
}

export interface DocumentModel {
  hasContent: boolean;
  filesize: number;
}

export interface PageDetails {
  size: number;
  number: number;
  totalElements: number;
  totalPages: number;
}

export interface OnboardingData {
  content: OnboardingRequestDTO[];
  page: PageDetails;
  empty: boolean;
}

