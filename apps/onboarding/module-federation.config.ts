import { ModuleFederationConfig } from '@nx/module-federation';

const config: ModuleFederationConfig = {
  name: 'onboarding',
  exposes: {
    './Routes': 'apps/onboarding/src/app/remote-entry/entry.routes.ts',
  },
  shared: (libraryName, defaultConfig) => {
    // Returning false means the library is not shared.
    return false;
  },
};

export default config;
