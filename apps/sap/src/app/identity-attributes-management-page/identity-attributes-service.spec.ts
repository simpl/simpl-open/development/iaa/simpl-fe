import { TestBed } from "@angular/core/testing";
import {
  IdentityAttributesService,
  ParticipantTypeEnum,
} from "./identity-attributes-service";
import { ApiService } from "@fe-simpl/api";
import { provideHttpClient } from "@angular/common/http";
import { of, throwError } from "rxjs";
import { provideApiUrl } from "@test-environment";

describe("IdentityAttributesService", () => {
  let service: IdentityAttributesService;
  let apiService: ApiService;
  const mockDetail = {
    id: "id",
    code: "code",
    name: "name",
    description: "description",
    assignableToRoles: false,
    enabled: false,
    creationTimestamp: "1995-12-17T03:24:00",
    updateTimestamp: "1995-12-17T04:24:00",
    participantTypes: [],
  };
  const mockCreate = {
    code: "code",
    name: "name",
    description: "description",
    assignableToRoles: false,
    enabled: false,
    participantTypes: [],
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideApiUrl],
    });
    service = TestBed.inject(IdentityAttributesService);
    apiService = TestBed.inject(ApiService);
  });

  it("should create", () => {
    expect(service).toBeTruthy();
  });

  it("should get detail", () => {
    const apiServiceSpy = jest
      .spyOn(apiService, "get")
      .mockReturnValue(of(mockDetail));
    service.detail("id").subscribe((response) => {
      expect(response).toEqual(mockDetail);
      expect(apiServiceSpy).toHaveBeenCalledWith(
        `/sap-api/identity-attribute/${encodeURI("id")}`
      );
    });
  });

  it("should handle error when getting detail", () => {
    const apiServiceSpy = jest
      .spyOn(apiService, "get")
      .mockReturnValue(throwError(() => new Error("Error!")));
    service.detail("id").subscribe({
      error: (err) => {
        expect(err).toEqual(new Error("Error!"));
      },
    });
    expect(apiServiceSpy).toHaveBeenCalledWith(
      `/sap-api/identity-attribute/${encodeURI("id")}`
    );
  });

  it("should update", () => {
    const apiServiceSpy = jest.spyOn(apiService, "put").mockReturnValue(of(void 0));
    service.update("id", mockDetail).subscribe(() => {
      expect(apiServiceSpy).toHaveBeenCalledWith(
        `/sap-api/identity-attribute/${encodeURI("id")}`,
        mockDetail
      );
    });
  });

  it("should handle error when updating", () => {
    const apiServiceSpy = jest
      .spyOn(apiService, "put")
      .mockReturnValue(throwError(() => new Error("Update Error")));
    service.update("id", mockDetail).subscribe({
      error: (err) => {
        expect(err).toEqual(new Error("Update Error"));
      },
    });
    expect(apiServiceSpy).toHaveBeenCalledWith(
      `/sap-api/identity-attribute/${encodeURI("id")}`,
      mockDetail
    );
  });

  it("should delete", () => {
    const apiServiceSpy = jest.spyOn(apiService, "delete").mockReturnValue(of(void 0));
    service.delete("id").subscribe(() => {
      expect(apiServiceSpy).toHaveBeenCalledWith(
        `/sap-api/identity-attribute/${encodeURI("id")}`
      );
    });
  });

  it("should handle error when deleting", () => {
    const apiServiceSpy = jest
      .spyOn(apiService, "delete")
      .mockReturnValue(throwError(() => new Error("Delete Error")));
    service.delete("id").subscribe({
      error: (err) => {
        expect(err).toEqual(new Error("Delete Error"));
      },
    });
    expect(apiServiceSpy).toHaveBeenCalledWith(
      `/sap-api/identity-attribute/${encodeURI("id")}`
    );
  });

  it("should create new attribute", () => {
    const apiServiceSpy = jest.spyOn(apiService, "post").mockReturnValue(of(mockDetail));
    service.create(mockCreate).subscribe(() => {
      expect(apiServiceSpy).toHaveBeenCalledWith(
        `/sap-api/identity-attribute`,
        mockCreate
      );
    });
  });

  it("should handle error when creating new attribute", () => {
    const apiServiceSpy = jest
      .spyOn(apiService, "post")
      .mockReturnValue(throwError(() => new Error("Create Error")));
    service.create(mockCreate).subscribe({
      error: (err) => {
        expect(err).toEqual(new Error("Create Error"));
      },
    });
    expect(apiServiceSpy).toHaveBeenCalledWith(
      `/sap-api/identity-attribute`,
      mockCreate
    );
  });

  it("should add to participant type", () => {
    const apiServiceSpy = jest.spyOn(apiService, "put").mockReturnValue(of(void 0));
    service
      .addToParticipantType(ParticipantTypeEnum.CONSUMER, ["id1", "id2"])
      .subscribe(() => {
        expect(apiServiceSpy).toHaveBeenCalledWith(
          `/sap-api/identity-attribute/add-participant-type/${encodeURI(
            ParticipantTypeEnum.CONSUMER
          )}`,
          ["id1", "id2"]
        );
      });
  });

  it("should handle error when adding to participant type", () => {
    const apiServiceSpy = jest
      .spyOn(apiService, "put")
      .mockReturnValue(throwError(() => new Error("Add Error")));
    service
      .addToParticipantType(ParticipantTypeEnum.CONSUMER, ["id1", "id2"])
      .subscribe({
        error: (err) => {
          expect(err).toEqual(new Error("Add Error"));
        },
      });
    expect(apiServiceSpy).toHaveBeenCalledWith(
      `/sap-api/identity-attribute/add-participant-type/${encodeURI(
        ParticipantTypeEnum.CONSUMER
      )}`,
      ["id1", "id2"]
    );
  });
});
