import { Component, OnInit, inject, signal } from '@angular/core';
import { CommonModule } from "@angular/common";
import { TabsComponent, Tab, TabsService } from "@fe-simpl/tabs";
import { TranslocoDirective } from "@jsverse/transloco";
import { SearchPageComponent, SearchPageInfo } from '@fe-simpl/search-page';
import { MatIcon } from "@angular/material/icon";
import { RouterModule } from "@angular/router";
import { ParticipantTypeService } from "./participant-types.service";
import { LoaderComponent } from "@fe-simpl/loader";

import { MatButtonModule } from "@angular/material/button";
import { ParticipantTypeEnum } from '../identity-attributes-management-page/identity-attributes-service';

@Component({
  selector: "app-participant-types",
  standalone: true,
  imports: [
    CommonModule,
    TabsComponent,
    TranslocoDirective,
    MatIcon,
    RouterModule,
    SearchPageComponent,
    LoaderComponent,
    MatButtonModule,
  ],
  templateUrl: "./participant-types.component.html",
  styleUrl: "./participant-types.component.css",
})
export class ParticipantTypesComponent implements OnInit {
  participantTypesService = inject(ParticipantTypeService);

  tab = signal(ParticipantTypeEnum.APPLICATION_PROVIDER);

  tabs = signal<Tab[]>([
    {
      id: ParticipantTypeEnum.APPLICATION_PROVIDER,
      label: "participantType.applicationProvider",
      primary: true,
    },
    {
      id: ParticipantTypeEnum.DATA_PROVIDER,
      label: "participantType.dataProvider",
    },
    {
      id: ParticipantTypeEnum.INFRASTRUCTURE_PROVIDER,
      label: "participantType.infrastructureProvider",
    },
    { id: ParticipantTypeEnum.CONSUMER, label: "participantType.consumer" },
  ]);

  tabsService = inject(TabsService);
  pageInfo: SearchPageInfo | undefined;


  ngOnInit(): void {
    const tab = this.tabsService.getTabId();
    if (tab) {
      this.tab.set(tab as ParticipantTypeEnum);
      this.pageInfo = this.participantTypesService.participantTypeSearchInfo(this.tab())
    } else {
      this.tab.set(ParticipantTypeEnum.APPLICATION_PROVIDER)
      this.pageInfo = this.participantTypesService.participantTypeSearchInfo(this.tab())
    }
  }

  clickTab(event: any) {
    this.tab.set(event.id);
    this.pageInfo = this.participantTypesService.participantTypeSearchInfo(this.tab())
  }
}
