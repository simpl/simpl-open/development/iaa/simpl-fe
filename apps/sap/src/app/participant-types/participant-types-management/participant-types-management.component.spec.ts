import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { ParticipantTypesManagementComponent } from './participant-types-management.component';
import { TranslocoDirective, TranslocoTestingModule } from '@jsverse/transloco';
import { SearchPageComponent } from '@fe-simpl/search-page';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { By } from '@angular/platform-browser';
import { ParticipantTypeEnum } from '../../identity-attributes-management-page/identity-attributes-service';
import { AppDomain, translocoTestConfiguration } from '@test-environment';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ApiService } from '@fe-simpl/api';
import { of } from 'rxjs';
import enTranslations from '../../../assets/i18n/en.json'

const mockApiService = {
  get: jest.fn().mockReturnValue(of([])),
  uploadFile: jest.fn().mockReturnValue(of({})),
};

describe('ParticipantTypesManagementComponent', () => {
  let component: ParticipantTypesManagementComponent;
  let fixture: ComponentFixture<ParticipantTypesManagementComponent>;
  let dialogRefSpy: jest.Mocked<MatDialogRef<ParticipantTypesManagementComponent>>;

  const mockParticipantType: ParticipantTypeEnum = ParticipantTypeEnum.APPLICATION_PROVIDER;

  beforeEach(async () => {
    dialogRefSpy = {
      close: jest.fn()
    } as unknown as jest.Mocked<MatDialogRef<ParticipantTypesManagementComponent>>;

    await TestBed.configureTestingModule({
      imports: [
        CommonModule,
        SearchPageComponent,
        TranslocoDirective,
        MatButtonModule,
        ParticipantTypesManagementComponent,
        MatDialogModule,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.SAP)
        ),
        NoopAnimationsModule,
        HttpClientTestingModule
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: { participantType: mockParticipantType } },
        { provide: MatDialogRef, useValue: dialogRefSpy },
        { provide: ApiService, useValue: mockApiService}
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantTypesManagementComponent);
    component = fixture.componentInstance;
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize filter with participantType', () => {
    expect(component.filter).toEqual({
      name: 'participantTypeNotIn',
      value: mockParticipantType,
    });
  });

  it('should initialize pageInfo correctly', () => {
    expect(component.pageInfo.endpoint).toBe('/sap-api/identity-attribute/search');
    expect(component?.pageInfo?.columns?.['id'].header).toBe('participantType.id');
  });


  it('should call dialogRef.close with idAttrIds when add button is clicked', fakeAsync(() => {

    tick();
    fixture.detectChanges();

    const buttonDebugElement = fixture.debugElement.query(By.css('#buttonAdd'));
    expect(buttonDebugElement).not.toBeNull();

    const button = buttonDebugElement.nativeElement;

    component.idAttrIds = ['123', '456'];
    button.click();

    tick();
    fixture.detectChanges();

    expect(dialogRefSpy.close).toHaveBeenCalledWith(['123', '456']);
  }));


  it('should translate text using TranslocoDirective', fakeAsync(() => {
    tick();
    fixture.detectChanges();
    const title = fixture.debugElement.query(By.css('h2')).nativeElement;
    expect(title.textContent).toContain(enTranslations.participantType.addAttribute);
  }));
});
