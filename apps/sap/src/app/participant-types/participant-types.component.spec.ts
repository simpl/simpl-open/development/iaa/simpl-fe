import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ParticipantTypesComponent } from './participant-types.component';
import { ParticipantTypeService } from './participant-types.service';
import { TabsService } from '@fe-simpl/tabs';
import { ParticipantTypeEnum } from '../identity-attributes-management-page/identity-attributes-service';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppDomain, provideApiUrl, translocoTestConfiguration } from '../../../../../test-environment';
import { provideHttpClient } from '@angular/common/http';
import enTranslations from '../../../../sap/src/assets/i18n/en.json';

describe('ParticipantTypesComponent', () => {
  let component: ParticipantTypesComponent;
  let fixture: ComponentFixture<ParticipantTypesComponent>;
  let participantTypeServiceMock: { loading: jest.Mock, participantTypeSearchInfo: jest.Mock, openIdAttrDialog: jest.Mock };
  let tabsServiceMock: { getTabId: jest.Mock };

  beforeEach(async () => {
    participantTypeServiceMock = {
      loading: jest.fn().mockReturnValue(false),
      participantTypeSearchInfo: jest.fn().mockReturnValue({}),
      openIdAttrDialog: jest.fn(),
    };

    tabsServiceMock = {
      getTabId: jest.fn().mockReturnValue(null),
    };

    await TestBed.configureTestingModule({
      imports: [ParticipantTypesComponent, TranslocoTestingModule.forRoot(
        translocoTestConfiguration(AppDomain.SAP)
      ),
        NoopAnimationsModule,],
      providers: [
        { provide: ParticipantTypeService, useValue: participantTypeServiceMock },
        { provide: TabsService, useValue: tabsServiceMock },
        provideHttpClient(),
        provideApiUrl
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should set default tab and pageInfo on init', () => {
    component.ngOnInit();
    expect(component.tab()).toBe(ParticipantTypeEnum.APPLICATION_PROVIDER);
    expect(participantTypeServiceMock.participantTypeSearchInfo).toHaveBeenCalledWith(ParticipantTypeEnum.APPLICATION_PROVIDER);
  });

  it('should set tab and pageInfo on tab click', () => {
    const event = { id: ParticipantTypeEnum.DATA_PROVIDER };
    component.clickTab(event);
    expect(component.tab()).toBe(ParticipantTypeEnum.DATA_PROVIDER);
    expect(participantTypeServiceMock.participantTypeSearchInfo).toHaveBeenCalledWith(ParticipantTypeEnum.DATA_PROVIDER);
  });

  it('should call openIdAttrDialog when button is clicked', () => {
    const spy = jest.spyOn(participantTypeServiceMock, 'openIdAttrDialog');
    component.participantTypesService.openIdAttrDialog(component.tab());
    expect(spy).toHaveBeenCalledWith(ParticipantTypeEnum.APPLICATION_PROVIDER);
  });

  it('should show loader when service is loading', () => {
    participantTypeServiceMock.loading.mockReturnValue(true);
    fixture.detectChanges();
    const loader = fixture.nativeElement.querySelector("#mainLoader");
    expect(loader).toBeTruthy();
  });

  it('should show the correct title in the template', () => {
    const title = fixture.nativeElement.querySelector('h1');
    expect(title.textContent).toContain(enTranslations.participantType.title);
  });

  it('should set default tab when no tab id is returned from TabsService', () => {
    tabsServiceMock.getTabId.mockReturnValue(null);
    component.ngOnInit();
    expect(component.tab()).toBe(ParticipantTypeEnum.APPLICATION_PROVIDER);
    expect(participantTypeServiceMock.participantTypeSearchInfo).toHaveBeenCalledWith(ParticipantTypeEnum.APPLICATION_PROVIDER);
  });

  it('should not show loader when service is not loading', () => {
    participantTypeServiceMock.loading.mockReturnValue(false);
    fixture.detectChanges();
    const loader = fixture.nativeElement.querySelector("#mainLoader");
    expect(loader).toBeNull();
  });

  it('should display the "Add Attribute" button and handle click', () => {
    const button = fixture.nativeElement.querySelector('button[mat-flat-button]');
    expect(button).toBeTruthy();
    button.click();
    expect(participantTypeServiceMock.openIdAttrDialog).toHaveBeenCalledWith(ParticipantTypeEnum.APPLICATION_PROVIDER);
  });

});
