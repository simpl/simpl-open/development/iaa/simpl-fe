import { inject, Injectable, signal } from "@angular/core";
import {
  SearchPageInfo,
  ButtonActions,
  PilotType,
} from "@fe-simpl/search-page";
import { MatDialog } from "@angular/material/dialog";
import { AlertBannerService } from "@fe-simpl/alert-banner";
import { ConfirmDialogComponent } from "@fe-simpl/shared-ui/confirm-dialog";
import { TranslocoService } from "@jsverse/transloco";
import { finalize, switchMap } from "rxjs";
import {
  IdentityAttributesService,
  ParticipantTypeEnum,
} from "../identity-attributes-management-page/identity-attributes-service";
import { ParticipantTypesManagementComponent } from "./participant-types-management/participant-types-management.component";

@Injectable({ providedIn: "root" })
export class ParticipantTypeService {
  private dialog = inject(MatDialog);
  private banner = inject(AlertBannerService);
  private translocoService = inject(TranslocoService);
  private identityAttributesService = inject(IdentityAttributesService);

  loading = signal(false);

  pilot: PilotType = {};
  participantTypeSearchInfo(id: ParticipantTypeEnum): SearchPageInfo {
    return {
      endpoint: `/sap-api/identity-attribute/search?participantTypeIn=${id}`,
      columns: {
        id: {
          header: "participantType.id",
        },
        code: {
          header: "participantType.code",
        },
        name: {
          header: "participantType.name",
        },
        assignableToRoles: {
          header: "participantType.assignableToRoles",
        },
        enabled: {
          header: "participantType.enabled",
        } /* ,
        outcomeUserEmail: {
          header: "participantType.outcomeUserEmail",
        } */,
        creationTimestamp: {
          header: "participantType.creationTimestamp",
          mapper: (v) => (v ? new Date(v) : ""),
        },
        updateTimestamp: {
          header: "participantType.updateTimestamp",
          mapper: (v) => (v ? new Date(v) : ""),
        },
        actions: {
          header: "simpleInput.actions",
          mapper: () =>
            new ButtonActions([
              {
                icon: "delete",
                theme: "secondary",
                action: (row) => this.deleteAction(row, id),
              },
            ]),
        },
      },
    };
  }

  deleteAction(row: any, partType: ParticipantTypeEnum) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: "simpleInput.confirmDeletion",
        message: this.translocoService.translate(
          "participantType.confirmDeletionMessage",
          { code: row.code }
        ),
        confirm: "simpleInput.delete",
        cancel: "simpleInput.cancel",
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.confirmDeleteIdAttrFromPartType(row, partType);
      }
    });
  }
  confirmDeleteIdAttrFromPartType(row: any, partType: ParticipantTypeEnum) {
    this.loading.set(true);
    this.identityAttributesService
      .detail(row.id)
      .pipe(
        finalize(() => this.loading.set(false)),
        switchMap((detail) =>
          this.identityAttributesService.update(detail.id, {
            ...detail,
            participantTypes: detail.participantTypes.filter(
              (item: any) => item !== partType
            ),
          })
        )
      )
      .subscribe({
        next: () => {
          this.banner.show({
            type: "info",
            message: this.translocoService.translate(
              "participantType.deleteSuccess"
            ),
          });
          this.pilot?.["reload"]();
        },
      });
  }

  openIdAttrDialog(participantType: ParticipantTypeEnum) {
    this.dialog
      .open(ParticipantTypesManagementComponent, {
        data: {
          participantType: participantType,
        },
      })
      .afterClosed()
      .subscribe((idAttrIds: string[]) => {
        if (idAttrIds?.length) {
          this.addIdAttrToParticipantType(participantType, idAttrIds);
        }
      });
  }

  addIdAttrToParticipantType(
    participantType: ParticipantTypeEnum,
    idAttrIds: string[]
  ) {
    this.loading.set(true);
    this.identityAttributesService
      .addToParticipantType(participantType, idAttrIds)
      .pipe(finalize(() => this.loading.set(false)))
      .subscribe({
        next: () => {
          this.banner.show({
            type: "info",
            message: this.translocoService.translate(
              "participantType.addSuccess"
            ),
          });
          this.pilot?.["reload"]();
        },
      });
  }
}
