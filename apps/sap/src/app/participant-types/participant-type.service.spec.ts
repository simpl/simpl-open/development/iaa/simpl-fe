import { TestBed } from "@angular/core/testing";
import { MatDialog } from "@angular/material/dialog";
import { of } from "rxjs";
import { AlertBannerService } from "@fe-simpl/alert-banner";
import { TranslocoService } from "@jsverse/transloco";
import {
  IdentityAttributesService,
  ParticipantTypeEnum
} from '../identity-attributes-management-page/identity-attributes-service';
import { ConfirmDialogComponent } from "@fe-simpl/shared-ui/confirm-dialog";
import { ParticipantTypeService } from './participant-types.service';

describe('ParticipantTypeService', () => {
  let service: ParticipantTypeService;
  let dialogMock: { open:  jest.Mock };
  let bannerMock: { show:  jest.Mock };
  let translocoMock: { translate:  jest.Mock };
  let identityAttributesServiceMock: {
    detail:  jest.Mock,
    update: jest.Mock,
    addToParticipantType: jest.Mock
  };

  beforeEach(() => {
    dialogMock = {
      open: jest.fn().mockReturnValue({
        afterClosed: jest.fn().mockReturnValue(of(true))
      })
    };

    bannerMock = {
      show: jest.fn()
    };

    translocoMock = {
      translate: jest.fn().mockReturnValue('translated_message')
    };

    identityAttributesServiceMock = {
      detail: jest.fn().mockReturnValue(of({ id: 'detailId', participantTypes: ['partType1', 'partType2'] })),
      update: jest.fn().mockReturnValue(of({})),
      addToParticipantType: jest.fn().mockReturnValue(of({}))
    };

    TestBed.configureTestingModule({
      providers: [
        ParticipantTypeService,
        { provide: MatDialog, useValue: dialogMock },
        { provide: AlertBannerService, useValue: bannerMock },
        { provide: TranslocoService, useValue: translocoMock },
        { provide: IdentityAttributesService, useValue: identityAttributesServiceMock }
      ]
    });

    service = TestBed.inject(ParticipantTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('participantTypeSearchInfo', () => {
    it('should return correct SearchPageInfo', () => {
      const participantType = ParticipantTypeEnum.APPLICATION_PROVIDER;
      const searchPageInfo = service.participantTypeSearchInfo(participantType);

      expect(searchPageInfo.endpoint).toBe(`/sap-api/identity-attribute/search?participantTypeIn=${participantType}`);
      expect(searchPageInfo?.columns?.['id']?.header).toBe('participantType.id');
    });
  });

  describe('deleteAction', () => {
    it('should open a confirmation dialog', () => {
      const row = { code: 'testCode' };
      const participantType = ParticipantTypeEnum.APPLICATION_PROVIDER;;

      service.deleteAction(row, participantType);

      expect(dialogMock.open).toHaveBeenCalledWith(ConfirmDialogComponent, {
        data: {
          title: "simpleInput.confirmDeletion",
          message: "translated_message",
          confirm: "simpleInput.delete",
          cancel: "simpleInput.cancel",
        },
      });
    });

    it('should call confirmDeleteIdAttrFromPartType on confirm', () => {
      const row = { id: 'rowId', code: 'testCode' };
      const participantType = ParticipantTypeEnum.APPLICATION_PROVIDER;;
      jest.spyOn(service, 'confirmDeleteIdAttrFromPartType').mockImplementation();

      service.deleteAction(row, participantType);

      expect(service.confirmDeleteIdAttrFromPartType).toHaveBeenCalledWith(row, participantType);
    });
  });

  describe('confirmDeleteIdAttrFromPartType', () => {
    it('should call identityAttributesService and show success banner', () => {
      const row = { id: 'rowId' };
      const participantType = ParticipantTypeEnum.APPLICATION_PROVIDER;

      service.confirmDeleteIdAttrFromPartType(row, participantType);

      expect(service.loading()).toBe(false);
      expect(identityAttributesServiceMock.detail).toHaveBeenCalledWith(row.id);
      expect(identityAttributesServiceMock.update).toHaveBeenCalledWith('detailId', {
        id: 'detailId',
        participantTypes: ['partType1', 'partType2'].filter(item => item !== participantType)
      });
      expect(bannerMock.show).toHaveBeenCalledWith({
        type: 'info',
        message: 'translated_message'
      });
    });
  });

  describe('addIdAttrToParticipantType', () => {
    it('should call identityAttributesService.addToParticipantType and show success banner', () => {
      const participantType = ParticipantTypeEnum.APPLICATION_PROVIDER;;
      const idAttrIds = ['id1', 'id2'];

      service.addIdAttrToParticipantType(participantType, idAttrIds);

      expect(service.loading()).toBe(false);
      expect(identityAttributesServiceMock.addToParticipantType).toHaveBeenCalledWith(participantType, idAttrIds);
      expect(bannerMock.show).toHaveBeenCalledWith({
        type: 'info',
        message: 'translated_message'
      });
    });
  });

  describe('openIdAttrDialog', () => {
    it('should open the dialog and call addIdAttrToParticipantType on close with selected ids', () => {
      const participantType = ParticipantTypeEnum.APPLICATION_PROVIDER;
      jest.spyOn(service, 'addIdAttrToParticipantType');
      dialogMock.open.mockReturnValueOnce({
        afterClosed: jest.fn().mockReturnValue(of(['id1', 'id2']))
      });

      service.openIdAttrDialog(participantType);

      expect(dialogMock.open).toHaveBeenCalled();
      expect(service.addIdAttrToParticipantType).toHaveBeenCalledWith(participantType, ['id1', 'id2']);
    });
  });

  it('should correctly map the creationTimestamp to a Date object', () => {
    const searchPageInfo = service.participantTypeSearchInfo(ParticipantTypeEnum.APPLICATION_PROVIDER);
    const mappedValue = searchPageInfo.columns?.['creationTimestamp']?.mapper
      ? searchPageInfo.columns['creationTimestamp'].mapper('2024-01-01T00:00:00Z')
      : undefined;
    expect(mappedValue).toEqual(new Date('2024-01-01T00:00:00Z'));
  });

  it('should set loading to true when addIdAttrToParticipantType is called', () => {
    service.addIdAttrToParticipantType(ParticipantTypeEnum.APPLICATION_PROVIDER, ['id1', 'id2']);
    expect(service.loading()).toBe(false);
  });

});
