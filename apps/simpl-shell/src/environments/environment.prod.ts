export const environment = {
  production: true,
  // @ts-ignore
  api_Url: window["env"]["api_Url"] || "default",
  // @ts-ignore
  keycloakConfig_url: window["env"]["keycloakConfig_url"],
  // @ts-ignore
  keycloakConfig_realm: window["env"]["keycloakConfig_realm"],
  // @ts-ignore
  keycloakConfig_clientId: window["env"]["keycloakConfig_clientId"],
};
