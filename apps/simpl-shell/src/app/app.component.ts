import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NxWelcomeComponent } from './nx-welcome.component';
import { MatToolbar } from '@angular/material/toolbar';
import { MatButton, MatIconButton } from '@angular/material/button';
import { MatMenu, MatMenuItem, MatMenuTrigger } from '@angular/material/menu';
import { MatIcon } from '@angular/material/icon';
import { NgClass, NgForOf, NgIf } from '@angular/common';
import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { KeycloakService } from 'keycloak-angular';

interface NavItem {
  label: string;
  route: string;
  roles: string[];
  children?: NavItem[];
}

@Component({
  standalone: true,
  imports: [NxWelcomeComponent, RouterModule, MatToolbar, MatIconButton, MatMenuTrigger, MatIcon, MatMenu, MatMenuItem, NgForOf, NgClass, NgIf, MatButton, MatProgressSpinner],
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  title = 'simpl-shell';
  isLoggedIn = false;
  rules: string[] = [];
  constructor(private readonly _keycloakService: KeycloakService) {
  }

  ngOnInit() {
    this.isLoggedIn = this._keycloakService.isLoggedIn()
    this.rules = this._keycloakService.getUserRoles()
  }

  navItems: NavItem[] = [
    {
      label: 'Onboarding',
      route: '',
      roles: ['NOTARY', 'T2IAA_M', 'IATTR_M'],
      children: [
        {
          label: 'Onboarding status',
          route: '/onboarding/application/additional-request',
          roles: ['']
        },
        {
          label: 'Onboarding administration',
          route: '/onboarding/administration',
          roles: ['NOTARY']
        },
        {
          label: 'Manage onboarding procedures',
          route: '/onboarding/administration/management/onboarding-procedures',
          roles: ['T2IAA_M']
        },
        {
          label: 'Manage participants',
          route: '/onboarding/administration/management/participant',
          roles: ['IATTR_M']
        }
      ]
    },
    {
      label: 'Sap',
      route: '',
      roles: ['IATTR_M'],
      children: [
        {
          label: 'Participant types',
          route: '/sap/participant-types',
          roles: ['IATTR_M']
        },
        {
          label: 'Identity Attributes',
          route: '/sap/identity-attributes',
          roles: ['IATTR_M']
        },
      ]
    },
    {
      label: 'Users and Roles',
      route: '',
      roles: ['T1UAR_M'],
      children: [
        {
          label: 'Identity attributes info',
          route: '/users-roles/identity-attributes-info',
          roles: ['T1UAR_M']
        },
        {
          label: 'Users',
          route: '/users-roles/users',
          roles: ['T1UAR_M']
        },
        {
          label: 'Roles',
          route: '/users-roles/roles',
          roles: ['T1UAR_M']
        }
      ]
    },
  ];

  logOut() {
    this._keycloakService.logout(`${document.baseURI}`).then(() => {
    });
  }

  logIn() {
  this._keycloakService.login().then((r) => {
  })
  }

  isVisible(itemRoles: string[]): boolean {
    return !itemRoles.length || itemRoles.some(role => this.rules.includes(role));
  }
}
