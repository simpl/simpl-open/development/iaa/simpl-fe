import { ModuleFederationConfig } from '@nx/module-federation';

const config: ModuleFederationConfig = {
  name: 'simpl-shell',
  /**
   * To use a remote that does not exist in your current Nx Workspace
   * You can use the tuple-syntax to define your remote
   *
   * remotes: [['my-external-remote', 'https://nx-angular-remote.netlify.app']]
   *
   * You _may_ need to add a `remotes.d.ts` file to your `src/` folder declaring the external remote for tsc, with the
   * following content:
   *
   * declare module 'my-external-remote';
   *
   */
  remotes: [
   /* [ 'onboarding', '/microservices/onboarding/remoteEntry.mjs' ],
    [ 'sap', '/microservices/sap/remoteEntry.mjs' ],
    [ 'usersandroles', '/microservices/users-roles/remoteEntry.mjs' ],*/
    'onboarding',
    'sap',
    'usersandroles',
    //[ 'participant-utility', '/microservices/participant-utility/remoteEntry.mjs' ],
  ],
  shared: (name, config) => {
    if (name === '@jsverse/transloco') {
      return { singleton: true, strictVersion: true }; // Example shared config
    }
  },
};

export default config;
