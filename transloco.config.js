module.exports = {
  rootTranslationsPath: 'apps/onboarding/src/assets/i18n/',
  langs: ['en', 'es'],
  keysManager: {}
};
