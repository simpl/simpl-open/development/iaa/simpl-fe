# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - 2025-02-02

### Fixed
- the onboarding status page is now only visible if you have the role of APPLICANT

## [1.0.1] - 2025-01-02

### Fixed
- disable the button when sending the onboarding request
- Show file name
- added participant type
- fix identity attributes tab
- Label filename and filesize for further documents

## [1.0.0] - 2024-12-23

### Added
- unit tests
- sonarQube connected
- coverage 80% coverage
- shell micro frontend

### Changed
- Microfrontend standalone recovered

### Fixed
- bug fix

## [0.8.0] - 2024-12-02

### Added
- Dataspace Governace Authority request more documents for an `onboarding request`
- Dataspace Governace Authority `write a rejection cause` in a rejected onboarding request
- Applicant dataspace participant `see rejection cause` in the reject onboarding request
- Users information page - creating a `new user`
- Participant agent settings page - `public/private keypair generation`
- Participant agent settings page - `public/private keypair import`
- Participant agent settings page - `CSR generation`
- Participant detail page - `renew credentials`
- Dataspace participant `installs` the newer `credential`
- Applicant `submits` onboarding request for review

### Changed
- Dataspace participant downloads credentials

### Fixed


## [0.7.0] - 2024-11-11

### Added
- Onboarding request summary page
- Dataspace Governace Authority write comments
- Dataspace Governance Authority creates an onboarding procedure template for a given partipant type
- Dataspace Governance Authority revokes access to dataspace participant
- Applicant dataspace participant creates and submits an onboarding request
- Applicant dataspace participant view all the documents attached to an onboarding request

### Changed

### Fixed
- Cross-site Scripting @pkg:npm/webpack@5.90.3
- Update Vulnerable and Outdated Components @pkg:npm/async@<3.2.6
- Regular Expression Denial of Service (ReDoS) @pkg:npm/path-to-regexp@<8.0
- Asymmetric Resource Consumption @pkg:npm/body-parser@<1.20.3
- Cross-site Scripting (XSS) @pkg:npm/express, npm/send & npm/serve-static
- Regular Expression Denial of Service @pkg:npm/micromatch@4.0.5
- Update vulnerable and outdated components @pkg:npm/path-to-regexp@<8.0.0
- Update vulnerable and outdated components @pkg:npm/vite@5.1.7
