import {
  Component,
  inject,
  input,
  OnInit,
  output,
  signal,
} from "@angular/core";
import { CommonModule } from "@angular/common";
import { TranslocoDirective } from "@jsverse/transloco";
import { NavigationEnd, Router } from "@angular/router";
export interface Tab {
  id: string;
  label: string;
  primary?: boolean;
}

@Component({
  selector: "lib-tabs",
  standalone: true,
  imports: [CommonModule, TranslocoDirective],
  templateUrl: "./tabs.component.html",
  styleUrl: "./tabs.component.css",
})
export class TabsComponent implements OnInit {
  router = inject(Router);

  tabs = input<Tab[]>([]);
  key = input<string>("tab");

  clickTab = output<Tab>();

  activeTab = signal<string>("");

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.updateActiveTab();
      }
    });

    // Imposta l'activeTab quando il componente viene inizializzato
    this.updateActiveTab();
  }

  updateActiveTab() {
    const url = this.router.parseUrl(this.router.url);
    const tabId = url.queryParams[this.key()];
    this.activeTab.set(tabId || this.getDefaultTabId());
  }

  getDefaultTabId(): string | number {
    if(this.tabs()){
      return (
        this.tabs()?.find((tab) => tab.primary)?.id ?? this.tabs()[0]?.id ?? ""
      );
    } else {
      return ""
    }
    
  }

  changePage(tab: Tab) {
    const url = this.router.parseUrl(this.router.url);
    url.queryParams[this.key()] = tab.id;
    this.router.navigateByUrl(url.toString());
    this.clickTab.emit(tab);
  }

  isPrimary(tab: Tab): boolean {
    return tab.id === this.activeTab();
  }
}
