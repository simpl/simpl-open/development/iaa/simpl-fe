import { Injectable, inject } from "@angular/core";
import { Router } from "@angular/router";

@Injectable({ providedIn: "root" })
export class TabsService {
  private router = inject(Router);
  // Dovrà avere un url che io setto sull'istanza che gli fornisco nel test e devo verificare il ritorno in base all'url
  getTabId(id?: string): string | undefined {
    const url = this.router.parseUrl(this.router.url);
    return url.queryParams[id ?? "tab"];
  }
}
