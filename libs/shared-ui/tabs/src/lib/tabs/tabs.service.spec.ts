import { TestBed } from "@angular/core/testing";
import { provideRouter, Router } from "@angular/router";
import { TabsService } from "@fe-simpl/tabs";
describe("TabsService", () => {
  let service: TabsService;
  let router: Router;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideRouter([])],
    });
    service = TestBed.inject(TabsService);
    router = TestBed.inject(Router);
  });

  it("should create", () => {
    expect(service).toBeTruthy();
  });
  it("should getTabId with no input parameter", () => {
    jest.spyOn(router, "url", "get").mockReturnValue("route/?tab=1");
    expect(service.getTabId()).toEqual("1");
  });

  it("should getTabId with tab id as input parameter", () => {
    jest.spyOn(router, "url", "get").mockReturnValue("route/?key=3");
    expect(service.getTabId("key")).toEqual("3");
  });

  it("should return undefined when getTabId is used with no inputs and router url queryParams hasn't tab", () => {
    jest.spyOn(router, "url", "get").mockReturnValue("route");
    expect(service.getTabId()).toEqual(undefined);
  });
});
