import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Tab, TabsComponent } from "./tabs.component";
import { TranslocoTestingModule } from "@jsverse/transloco";
import { translocoTestConfiguration } from "@test-environment";
import { Router } from "@angular/router";
describe("TabsComponent", () => {
  let component: TabsComponent;
  let fixture: ComponentFixture<TabsComponent>;
  let router: Router;
  const tabMock: Tab[] = [
    {
      id: "0",
      label: "Label tab 1",
      primary: true,
    },
    {
      id: "1",
      label: "Label tab 2",
    },
    {
      id: "2",
      label: "Label tab 3",
    },
  ];
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TabsComponent,
        TranslocoTestingModule.forRoot(translocoTestConfiguration()),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TabsComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
  });

  it("should create", () => {
    fixture.componentRef.setInput("tabs", tabMock);
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it("should render tabs correclty", () => {
    const updateActiveTabSpy = jest.spyOn(component, "updateActiveTab");
    fixture.componentRef.setInput("tabs", tabMock);
    fixture.detectChanges();
    // Getting tabs HTML elements
    const tab1 = fixture.debugElement.nativeElement.querySelector(
      "#tab-" + tabMock[0].id
    );
    const tab2 = fixture.debugElement.nativeElement.querySelector(
      "#tab-" + tabMock[1].id
    );
    const tab3 = fixture.debugElement.nativeElement.querySelector(
      "#tab-" + tabMock[2].id
    );

    // Assert initialization logic has occured
    expect(updateActiveTabSpy).toHaveBeenCalled();
    expect(component.tabs().length).toBeTruthy();

    // Assert tabs are rendered
    expect(tab1).not.toBeNull();
    expect(tab2).not.toBeNull();
    expect(tab3).not.toBeNull();
    // Assert activeTab is the one with primary = true
    expect(component.activeTab()).toEqual(tabMock[0].id);
    // Assert label inside tabs are rendered correctly
    expect(tab1.textContent).toEqual(tabMock[0].label);
    expect(tab2.textContent).toEqual(tabMock[1].label);
    expect(tab3.textContent).toEqual(tabMock[2].label);
  });

  it("should render correctly when no input tabs are given", () => {
    const updateActiveTabSpy = jest.spyOn(component, "updateActiveTab");
    const getDefaultTabIdSpy = jest.spyOn(component, "getDefaultTabId");
    fixture.detectChanges();

    // Assert default logic has been executed
    expect(updateActiveTabSpy).toHaveBeenCalled();
    expect(getDefaultTabIdSpy).toHaveBeenCalled();
    expect(component.tabs().length).toBeFalsy();
    expect(component.activeTab()).toEqual("");
  });

  it("should render correctly when no tab has isPrimary = true", () => {
    const updateActiveTabSpy = jest.spyOn(component, "updateActiveTab");
    const getDefaultTabIdSpy = jest.spyOn(component, "getDefaultTabId");
    fixture.componentRef.setInput("tabs", [
      { ...tabMock[0], isPrimary: false },
      tabMock[1],
      tabMock[2],
    ]);
    fixture.detectChanges();

    // Assert default logic when no tab has isPrimary = true
    expect(updateActiveTabSpy).toHaveBeenCalled();
    expect(getDefaultTabIdSpy).toHaveBeenCalled();
    expect(component.tabs().length).toBeTruthy();
    expect(component.activeTab()).toEqual(tabMock[0].id);
  });

  it("should change tab on click", () => {
    //const updateActiveTabSpy = jest.spyOn(component, "updateActiveTab");
    const routerSpy = jest.spyOn(router, "navigateByUrl");
    const changePageSpy = jest.spyOn(component, "changePage");
    const clickTab = jest.spyOn(component.clickTab, "emit");
    fixture.componentRef.setInput("tabs", tabMock);
    fixture.detectChanges();

    // Getting tabs HTML elements
    const tab1 = fixture.debugElement.nativeElement.querySelector(
      "#tab-" + tabMock[0].id
    );
    const tab2 = fixture.debugElement.nativeElement.querySelector(
      "#tab-" + tabMock[1].id
    );
    const tab3 = fixture.debugElement.nativeElement.querySelector(
      "#tab-" + tabMock[2].id
    );

    // Assert tabs are rendered
    expect(tab1).not.toBeNull();
    expect(tab2).not.toBeNull();
    expect(tab3).not.toBeNull();

    // Click Simulation
    tab2.firstChild.click();
    fixture.detectChanges();

    // Assert changePage has been executed
    expect(changePageSpy).toHaveBeenCalled();
    // Assert active tab is the one clicked
    expect(component.activeTab()).toEqual(tabMock[1].id);
    // Assert clickTab output emitted the right value
    expect(clickTab).toHaveBeenCalledWith(tabMock[1]);
    // Assert navigateByUrl occured with the right parameter
    expect(routerSpy).toHaveBeenCalledWith("/?tab=1");
  });
});
