import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { TranslocoDirective } from '@jsverse/transloco';

export interface DialogData {
  title: string,
  cancel: string,
  confirm: string,
  message?: string,
  message2?: string,
  idAttrMessage1?: string,
  idAttrMessage2?: string,
  noCloseButtons?: boolean,
  data?: any[],
  extraMessage?: string,
}

@Component({
  selector: 'lib-confirm-dialog',
  standalone: true,
  imports: [[MatDialogModule, MatButtonModule,
    CommonModule,
    TranslocoDirective]],
  templateUrl: './confirm-dialog.component.html',
  styleUrl: './confirm-dialog.component.css',
})
export class ConfirmDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {
    const defaultData: DialogData = {
      title: 'title',
      cancel: 'cancel',
      confirm: 'confirm',
      noCloseButtons: false,
      idAttrMessage1: '',
      idAttrMessage2: ''
    };
    Object.assign(defaultData, data);
    this.data = defaultData;
  }

}
