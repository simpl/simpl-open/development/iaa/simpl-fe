import {
  Component,
  computed,
  inject,
  input,
  output,
  signal,
} from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatButton } from "@angular/material/button";
import { MatFormField, MatFormFieldModule } from "@angular/material/form-field";
import { MatIcon } from "@angular/material/icon";
import { MatInput, MatInputModule } from "@angular/material/input";
import { MatMenu, MatMenuModule } from "@angular/material/menu";
import { MatOption } from "@angular/material/autocomplete";
import { MatSelect } from "@angular/material/select";
import { MatDatepickerModule } from "@angular/material/datepicker";
import {
  FormControl,
  FormsModule,
  ReactiveFormsModule,
} from "@angular/forms";
import { OnboardingRequestStatus } from "@fe-simpl/api-types";
import { TranslocoDirective, TranslocoService } from "@jsverse/transloco";
import { provideNativeDateAdapter } from "@angular/material/core";

export type FilterValueType = string | Date | undefined | MultipleData<any>;

export type FilterGroup<T = FilterValueType> = { [key: string]: Filter<T> };

export interface OptionParameter {
  value: string;
  label: string;
}

export abstract class Filter<T = FilterValueType> {
  readonly name: string;
  readonly label: string;
  constructor(name: string, label: string) {
    this.name = name;
    this.label = label;
  }

  abstract value(): T | undefined;
  abstract reset(): void;
}

export class FormControlFilter<T> extends Filter<T> {
  readonly controller = new FormControl();

  override value(): T {
    return this.controller.value;
  }
  override reset(): void {
    this.controller.reset();
  }
}

export class TextFilter extends FormControlFilter<string> {}

export class DateFilter extends FormControlFilter<Date> {}

export class MultipleData<T extends { [key: string]: unknown }> {
  readonly data: T;
  constructor(data: T) {
    this.data = data;
  }
}

export class DateRangeFilter extends Filter<
  MultipleData<{ [key: string]: Date }>
> {
  readonly nameFrom: string;
  readonly nameTo: string;
  readonly controllerFrom = new FormControl();
  readonly controllerTo = new FormControl();

  constructor(name: string, label: string, nameFrom: string, nameTo: string) {
    super(name, label);
    this.nameFrom = nameFrom;
    this.nameTo = nameTo;
  }

  override value(): MultipleData<{ [key: string]: Date }> | undefined {
    if (this.controllerFrom.value) {
      const from = this.controllerFrom.value;
      const to = this.controllerTo.value
        ? new Date(this.controllerTo.value)
        : undefined;
      if (to) {
        to.setDate(to.getDate() + 1);
      }
      return new MultipleData({
        [this.nameFrom]: from,
        ...(to ? { [this.nameTo]: to } : {}),
      });
    } else {
      return undefined;
    }
  }

  override reset(): void {
    this.controllerFrom.reset();
    this.controllerTo.reset();
  }
}

export class OptionFilter extends FormControlFilter<string> {
  readonly options: OptionParameter[];

  constructor(name: string, label: string, options: OptionParameter[]) {
    super(name, label);
    this.options = options;
  }
}

export interface SubmitData {
  [key: string]: FilterValueType;
}

@Component({
  selector: "lib-filter",
  standalone: true,
  providers: [provideNativeDateAdapter()],
  imports: [
    CommonModule,
    MatButton,
    FormsModule,
    MatFormField,
    MatIcon,
    MatInput,
    MatMenu,
    MatOption,
    MatSelect,
    MatMenuModule,
    ReactiveFormsModule,
    TranslocoDirective,
    MatButton,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  templateUrl: "./filter.component.html",
  styleUrl: "./filter.component.css",
})
export class FilterComponent {
  private readonly translocoService = inject(TranslocoService);
  filters = input<FilterGroup>({});

  submit = output<SubmitData>();

  selectedFiltersLabel: string = "";

  inputSelected = computed(() => {
    const v = this.filterValue();
    return v ? { name: v, input: this.filters()[v] } : null;
  });

  statuses = Object.keys(OnboardingRequestStatus).map((key) => ({
    value: key,
    label: OnboardingRequestStatus[key as keyof typeof OnboardingRequestStatus],
  }));

  options = computed(() => {
    return Object.entries(this.filters()).map(([k, obj]) => ({
      label: obj.label,
      value: k,
    }));
  });

  protected filledFieldsCount = 0;

  get filledFields(): number {
    return this.filledFieldsCount;
  }

  filterValue = signal<"email" | "status" | null>(null);

  filter() {
    const values = Object.entries(this.filters()).reduce(
      (acc, [key, filter]) => {
        const value = filter.value();
        if (value instanceof MultipleData) {
          return { ...acc, ...value.data };
        } else {
          return { ...acc, ...{ [key]: value } };
        }
      },
      {}
    );
    this.submit.emit(values);
    this.selectedFiltersUpdate(values);
  }

  reset() {
    this.filterValue.set(null);
    Object.values(this.filters()).forEach((filter) => filter.reset());
    this.inputChange();
    this.filter();
  }

  selectedFiltersUpdate(values: SubmitData){
    let verboseFilters = "";
    if(values) {
      Object.entries(values).forEach(([key,value]) => {
        if(value && value !== "") {
          const fieldLabel = this.options().find(option => option.value === key)?.label ?? "";
          const translatedFieldValue = this.translocoService.translate(fieldLabel) || "-"
          verboseFilters += translatedFieldValue +": ";
          if(value instanceof Date){
            verboseFilters += value.toDateString() +"; ";
          } else {
            verboseFilters += value +"; ";
          }
        }
      })
    }
    this.filledFieldsCount = Object.values(values).filter(value => value && value !== "").length;
    this.selectedFiltersLabel = verboseFilters;
  }

  isInputText(input: Filter | null): TextFilter | null {
    return input && input instanceof TextFilter ? input : null;
  }

  isInputOption(input: Filter | null): OptionFilter | null {
    return input && input instanceof OptionFilter
      ? (input as OptionFilter)
      : null;
  }

  isDateFilter(input: Filter | null): DateFilter | null {
    return input && input instanceof DateFilter ? (input as DateFilter) : null;
  }

  isDateRangeFilter(input: Filter | null): DateRangeFilter | null {
    return input && input instanceof DateRangeFilter
      ? (input as DateRangeFilter)
      : null;
  }

  inputChange() {
    const vm = this.filters();
    const counter = Object.values(vm).reduce(
      (acc, filter) => acc + (filter.value() ? 1 : 0),
      0
    );
    this.filledFieldsCount = counter;
  }
}
