import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  standalone: true,
  name: 'removePlaceholderTemp'
})
export class RemovePlaceholderTempPipe implements PipeTransform {
  transform(value: any): string {
    if (typeof value !== 'string') {
      return value;
    }
    if (!value) return value;
    return value.replace(/^\$\{(.+)}$/, '$1');
  }
}
