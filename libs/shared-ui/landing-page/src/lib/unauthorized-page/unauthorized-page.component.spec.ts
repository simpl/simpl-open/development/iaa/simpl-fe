import { TranslocoTestingModule } from '@jsverse/transloco';
import { render } from '@testing-library/angular';
import { screen } from '@testing-library/dom';
import { AppDomain, translocoTestConfiguration } from 'test-environment';
import { UnauthorizedPageComponent } from './unauthorized-page.component';

beforeEach(() => {
  jest.resetAllMocks();
});

describe('UnauthorizedPageComponent', () => {
  it('should create', async () => {
    await render(UnauthorizedPageComponent, {
      imports: [
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(AppDomain.PARTICIPANT_UTILITY)
        ),
      ]
    });


    screen.getByText(/restricted/i);
    screen.getByText(/Access denied/i);
  });
});
