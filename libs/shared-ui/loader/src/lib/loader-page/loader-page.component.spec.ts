import { ComponentFixture, TestBed } from "@angular/core/testing";
import { LoaderPageComponent } from "./loader-page.component";

describe("LoaderPageComponent", () => {
  let component: LoaderPageComponent;
  let fixture: ComponentFixture<LoaderPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LoaderPageComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(LoaderPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should render loader", () => {
    const loader = fixture.debugElement.nativeElement.querySelector("#loader");
    expect(loader).not.toBeNull();
  });
});
