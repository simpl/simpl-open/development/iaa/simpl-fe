import { ComponentFixture, TestBed } from "@angular/core/testing";
import { LoaderComponent } from "@fe-simpl/loader";

describe("LoaderComponent", () => {
  let component: LoaderComponent;
  let fixture: ComponentFixture<LoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LoaderComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(LoaderComponent);
    component = fixture.componentInstance;
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should render loader with default size", () => {
    fixture.detectChanges();
    // Getting loader HTML element
    const loader = fixture.debugElement.nativeElement.querySelector("#loader");
    // Asserting loader is rendered
    expect(loader).not.toBeNull();
    // Asserting loader has default size
    expect(loader.style.width).toEqual("150px");
    expect(loader.style.height).toEqual("150px");
  });

  it("should render loader with input size", () => {
    // Setting input size
    component.size = 200;
    fixture.detectChanges();
    // Getting loader HTML element
    const loader = fixture.debugElement.nativeElement.querySelector("#loader");
    // Asserting loader is rendered
    expect(loader).not.toBeNull();
    // Asserting loader has input size
    expect(loader.style.width).toEqual("200px");
    expect(loader.style.height).toEqual("200px");
  });
});
