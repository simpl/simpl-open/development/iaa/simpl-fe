import { ComponentFixture, TestBed } from "@angular/core/testing";
import {
  BooleanDetail,
  DateDetail,
  DetailsPageComponent,
  TextDetail,
} from "./details-page.component";
import { TranslocoTestingModule } from "@jsverse/transloco";
import { translocoTestConfiguration } from "@test-environment";

describe("DetailsPageComponent", () => {
  let component: DetailsPageComponent;
  let fixture: ComponentFixture<DetailsPageComponent>;
  const customTranslations = {
    details: {
      text: "Text",
      bool: "Boolean",
      date: "Date",
    },
  };

  const mock = {
    text: new TextDetail("details.text", "value", {
      editable: true,
      required: true,
    }),
    bool: new BooleanDetail("details.bool", false, {
      editable: true,
      required: true,
    }),
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        DetailsPageComponent,
        TranslocoTestingModule.forRoot(
          translocoTestConfiguration(undefined, customTranslations)
        ),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsPageComponent);
    component = fixture.componentInstance;
  });

  it("should create", () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it("should render details", () => {
    const mockWithDate = {
      ...mock,
      date: new DateDetail("details.date", "1995-12-17T04:24:00"),
    };
    fixture.componentRef.setInput("entity", mockWithDate);
    fixture.componentRef.setInput("customClass", "pt-5");
    fixture.detectChanges();
    expect(component.entity()).toBeTruthy();

    // Getting details HTML elements
    const textDetailLabel =
      fixture.debugElement.nativeElement.querySelector("#label-text");
    const boolDetailLabel =
      fixture.debugElement.nativeElement.querySelector("#label-bool");
    const dateDetailLabel =
      fixture.debugElement.nativeElement.querySelector("#label-date");
    const textDetailValue =
      fixture.debugElement.nativeElement.querySelector("#value-text");
    const boolDetailValue =
      fixture.debugElement.nativeElement.querySelector("#value-bool");
    const dateDetailValue =
      fixture.debugElement.nativeElement.querySelector("#value-date");

    // Assert elements are rendered
    expect(textDetailLabel).not.toBeNull();
    expect(boolDetailLabel).not.toBeNull();
    expect(dateDetailLabel).not.toBeNull();
    expect(textDetailValue).not.toBeNull();
    expect(boolDetailValue).not.toBeNull();
    expect(dateDetailValue).not.toBeNull();

    // Assert label and values are correct
    expect(textDetailLabel.textContent).toEqual(
      customTranslations.details.text
    );
    expect(boolDetailLabel.textContent).toEqual(
      customTranslations.details.bool
    );
    expect(dateDetailLabel.textContent).toEqual(
      customTranslations.details.date
    );
    expect(textDetailValue.textContent).toEqual(mock.text.value);
    expect(boolDetailValue.textContent).toEqual(mock.bool.value.toString());
  });

  it("should allow editing details through edit button", () => {
    const saveSpy = jest.spyOn(component, "saveAction");
    const saveOutputSpy = jest.spyOn(component.save, "emit");
  
    fixture.componentRef.setInput("showEditButton", true);
    fixture.componentRef.setInput("entity", mock);
    fixture.componentRef.setInput("editForm", mock);
    fixture.detectChanges();
    
    expect(component.entity()).toBeTruthy();
  
    const editButton =
      fixture.debugElement.nativeElement.querySelector("#editButton");
    expect(editButton).not.toBeNull();
  
    editButton.click();
    fixture.detectChanges();
  
    const editForm =
      fixture.debugElement.nativeElement.querySelector("#editForm");
    expect(editForm).not.toBeNull();
  
    const textLabelHTML = editForm.querySelector("#label-text");
    const textInputHTML = editForm.querySelector("#input-text");
    const boolLabelHTML = editForm.querySelector("#label-bool");
    const boolInputHTML = editForm.querySelector("#input-bool");
  
    expect(textLabelHTML).not.toBeNull();
    expect(textInputHTML).not.toBeNull();
    expect(boolLabelHTML).not.toBeNull();
    expect(boolInputHTML).not.toBeNull();
  
    textInputHTML.value = "text";
    textInputHTML.dispatchEvent(new Event("input"));
    boolInputHTML.click();
    fixture.detectChanges();
  
    expect(component.formGroup()?.get("text")?.value).toEqual("text");
    expect(component.formGroup()?.get("bool")?.value).toBeTruthy();
  
    const submitButton = editForm.querySelector("#submitButton");
    submitButton.click();
    fixture.detectChanges();
  
    expect(saveSpy).toHaveBeenCalled();
    expect(saveOutputSpy).toHaveBeenCalledWith({
      text: "text",
      bool: true,
    });
  });

  it("should allow editing details through input property editMode", () => {
    const saveSpy = jest.spyOn(component, "saveAction");
  
    fixture.componentRef.setInput("entity", mock);
    fixture.componentRef.setInput("editForm", mock); 
    fixture.componentRef.setInput("editMode", true);
  
    fixture.detectChanges();
    expect(component.entity()).toBeTruthy();
    const editForm =
      fixture.debugElement.nativeElement.querySelector("#editForm");
    expect(editForm).not.toBeNull();
  
    const textLabelHTML = editForm.querySelector("#label-text");
    const textInputHTML = editForm.querySelector("#input-text");
    const boolLabelHTML = editForm.querySelector("#label-bool");
    const boolInputHTML = editForm.querySelector("#input-bool");
    
    expect(textLabelHTML).not.toBeNull();
    expect(textInputHTML).not.toBeNull();
    expect(boolLabelHTML).not.toBeNull();
    expect(boolInputHTML).not.toBeNull();
  
    textInputHTML.value = "text";
    textInputHTML.dispatchEvent(new Event("input"));
    boolInputHTML.click();
    fixture.detectChanges();
    expect(component.formGroup()?.get("text")?.value).toEqual("text");
    expect(component.formGroup()?.get("bool")?.value).toBeTruthy();
  
    const submitButton = editForm.querySelector("#submitButton");
    submitButton.click();
    fixture.detectChanges();
    expect(saveSpy).toHaveBeenCalled();
  });

  it("should allow canceling edit", () => {
    const cancelSpy = jest.spyOn(component, "cancelAction");
    fixture.componentRef.setInput("editMode", true);
    fixture.componentRef.setInput("entity", mock);
    fixture.detectChanges();
    expect(component.entity()).toBeTruthy();

    // Getting form HTML element and asserting is rendered
    const editForm =
      fixture.debugElement.nativeElement.querySelector("#editForm");
    expect(editForm).not.toBeNull();

    // Getting cancel button HTML element and asserting is rendered
    const cancelButton =
      fixture.debugElement.nativeElement.querySelector("#cancelButton");
    expect(cancelButton).not.toBeNull();

    cancelButton.click();
    fixture.detectChanges();
    expect(cancelSpy).toHaveBeenCalled();
    expect(component.edit()).toBeFalsy();
  });
});
