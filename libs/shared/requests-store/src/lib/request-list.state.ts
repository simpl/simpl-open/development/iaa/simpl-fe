import {
  PagedParticipantDto,
  ParticipantDto,
  RequestListProperties,
} from "@fe-simpl/api-types";

export interface RequestListState {
  loading: boolean;
  data: PagedParticipantDto | null;
  list: ParticipantDto[] | null;
  totalElements: number | undefined;
  currentPage: number | undefined;
  pageSize: number | undefined;
  totalPages: number | undefined;
  filters: RequestListProperties;
  completed: boolean;
}

export const requestListInitialState: RequestListState = {
  completed: false,
  loading: false,
  list: null,
  data: null,
  totalElements: 0,
  currentPage: 0,
  pageSize: 0,
  totalPages: 0,
  filters: {
    sort: [],
    email: null,
    status: null,
  },
};
