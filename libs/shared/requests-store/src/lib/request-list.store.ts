import { patchState, signalStore, withMethods, withState } from "@ngrx/signals";
import { inject } from "@angular/core";
import { rxMethod } from "@ngrx/signals/rxjs-interop";
import { pipe, switchMap, tap } from "rxjs";
import { RequestListProperties } from "@fe-simpl/api-types";
import {
  requestListInitialState,
  RequestListState,
} from "./request-list.state";
import { RequestListService } from "./request-list.service";

export const RequestListStore = signalStore(
  { providedIn: "root" },
  withState<RequestListState>(requestListInitialState),
  withMethods((store, requestListService = inject(RequestListService)) => ({
    getList: rxMethod<void>(
      pipe(
        tap(() =>
          patchState(store, {loading: true, list: [], completed: false })
        ),
        switchMap(() =>
          requestListService.search(store.filters()).pipe(
            tap({
              next: ({ content, page }) => {
                patchState(store, {
                  data: { content, page },
                  currentPage: page.number,
                  totalPages: page.totalPages,
                  totalElements: page.totalElements,
                  pageSize: page.size,
                  ...(content && { list: content }),
                });
              },
              error: () => patchState(store, { loading: false }),
              complete: () =>
                patchState(store, { loading: false, completed: true }),
            })
          )
        )
      )
    ),
    updateFilters: (myFilter: Partial<RequestListProperties>) => {
      patchState(store, (state) => ({
        filters: { ...state.filters, ...myFilter },
      }));
    },
    reset: () => {
      patchState(store, (state) => ({
        filters: { ...state.filters, email: null, status: null, applicantRepresentative: null, onboardingStatusValue: null },
      }));
    },
    setLoader: (loader: boolean) => {
      patchState(store, { loading: loader });
    },
  }))
);
