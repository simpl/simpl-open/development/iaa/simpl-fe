import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  PagedParticipantDto,
  RequestListProperties
} from '@fe-simpl/api-types';
import { API_URL, ApiService } from '@fe-simpl/api';

@Injectable({ providedIn: 'root' })
export class RequestListService {
  private readonly apiService = inject(ApiService);
  private readonly _httpClient = inject(HttpClient);
  private readonly api_url = inject(API_URL);
  /**
   * @param params as RequestListProperties
   */
  public search(
    params?: RequestListProperties
  ): Observable<PagedParticipantDto> {
    let queryParameters = new HttpParams();
    if (params) {
      const { status, page, sort, size, email } = params;

    if (email !== undefined && email !== null) {
      queryParameters = queryParameters.set('email', email);
    }
    if (status !== undefined && status !== null) {
      queryParameters = queryParameters.set('status', status);
    }

    queryParameters = queryParameters.set('page', page || 0);

    queryParameters = queryParameters.set('size', size || 5);

    if (sort && sort.length > 0) {
      sort.forEach((element) => {
        queryParameters = queryParameters.append('sort', element);
      });
    }
  }

    return this.apiService.get<PagedParticipantDto>(
      `/onboarding-api/onboarding-request`,
      queryParameters
    );
  }

  public approveParticipant(
    requestId: string,
    approved: boolean,
    idAttrList: string[],
    rejectionCause: string
  ): Observable<any> {

    const participantIdUrlEncoded = encodeURI(requestId);

    const body = {
      value: approved ? 'APPROVED' : 'REJECTED'
    };

    let params = new HttpParams();
    if (idAttrList.length > 0) {
      params = params.append('identityAttributesExcluded', idAttrList.join(','));
    }
    if (rejectionCause) {
      params = params.append('rejectionCause', rejectionCause);
    }

    const URL = `${this.api_url}/onboarding-api/onboarding-request/${participantIdUrlEncoded}/status`;

    return this._httpClient.patch(
      URL,
      body,
      { params }
    );
  }
}
