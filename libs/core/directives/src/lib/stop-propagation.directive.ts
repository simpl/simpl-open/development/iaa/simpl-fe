import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[libStopPropagation]',
  standalone: true
})
export class StopPropagationDirective {
  @HostListener('click', ['$event'])
  @HostListener('keydown', ['$event'])
  stopPropagation(event: Event): void {
    event.stopPropagation();
  }
}
