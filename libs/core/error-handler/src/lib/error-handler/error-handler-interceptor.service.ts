import { HttpErrorResponse, HttpEvent, HttpHandlerFn, HttpRequest } from '@angular/common/http';
import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AlertBanner, AlertBannerService } from '@fe-simpl/alert-banner'

const errorRoutesMap = new Map<string, string[]>([
  ['404', ['/certificate', '/credential-validity']],
  ['409', ['/onboarding-request']],
]);

export const errorHandlingInterceptor = (
  request: HttpRequest<any>,
  next: HttpHandlerFn,
): Observable<HttpEvent<any>> => {
  const alertBannerService = inject(AlertBannerService);
  const router = inject(Router);

  return next(request).pipe(
    catchError((error) => {
      const errorsRedirect = [
        400, 401, 403,
        404, 500, 501,
        502, 503,  504,
        505,  508,  511
      ]
      if (error instanceof HttpErrorResponse) {

        if (shouldThrowError(error.status, request.url)) {
          return throwError(() => error);
        }

        if ( errorsRedirect.includes(error.status) || (error.status >= 500 && error.status < 600)) {
          const errorInfo = {
            status: error.status,
            statusText: error.statusText,
            name: error.name,
            message: error.message,
            url: error.url,
            error: error.error
          };

          router.navigate(['/error'], {
            state: { error: errorInfo }
          });
        }
        else {
          alertBannerService.show(bannerFromError(error));
        }
      }
      return throwError(() => error);
    }),
  );
};

function shouldThrowError(status: number, url: string): boolean {
  const routes = errorRoutesMap.get(status.toString());
  return routes ? routes.some(route => url.includes(route)) : false;
}

function bannerFromError(error: HttpErrorResponse): AlertBanner {
  return {
    message: error.message,
    type: "danger",
    strong: "Attention!",
    autoDismissable: true,
    timeToDismissed: 3000
  }
}
