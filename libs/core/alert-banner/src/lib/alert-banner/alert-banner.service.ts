import { Injectable, inject } from '@angular/core';
import { AlertBannerStore } from './alert-banner.store';
import { tap, timer } from 'rxjs';

export interface AlertBannerStoreData {
  banners: AlertBanner[],
}

type AlertBannerType = "info" | "danger" | "warning" | "success";

export interface AlertBanner {
  type: AlertBannerType,
  message: string,
  strong?: string,
  autoDismissable?: boolean,
  timeToDismissed?: number,
}

@Injectable({
  providedIn: 'root',
})
export class AlertBannerService {
  alertBannerStore = inject(AlertBannerStore);

  show(banner: AlertBanner) {
    this.alertBannerStore.showAlert(banner)

    if (banner.autoDismissable) {
      timer(banner.timeToDismissed || 3000).pipe(
        tap(() => this.alertBannerStore.dismiss(banner))
      ).subscribe();
    }
  }
}


