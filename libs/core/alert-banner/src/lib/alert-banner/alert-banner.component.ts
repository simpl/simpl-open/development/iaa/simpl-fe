import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertBannerStore } from './alert-banner.store';
import { AlertBanner } from './alert-banner.service';

@Component({
  selector: 'lib-alert-banner',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './alert-banner.component.html',
  styleUrl: './alert-banner.component.css',
})
export class AlertBannerComponent {
  private readonly alertBannerStore = inject(AlertBannerStore);

  $banners = this.alertBannerStore.banners;

  dismiss (banner: AlertBanner) {
    this.alertBannerStore.dismiss(banner);
  }
}
