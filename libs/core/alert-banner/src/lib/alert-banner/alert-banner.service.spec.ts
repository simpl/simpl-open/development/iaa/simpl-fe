import { TestBed } from "@angular/core/testing";
import {
  AlertBanner,
  AlertBannerService,
} from "./alert-banner.service";
import { AlertBannerStore } from "./alert-banner.store";
import { timer } from "rxjs";
describe("AlertBannerService", () => {
  let service: AlertBannerService;
  let store: any;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlertBannerStore],
    });
    service = TestBed.inject(AlertBannerService);
    store = TestBed.inject(AlertBannerStore);
  });

  it("should create", () => {
    expect(service).toBeTruthy();
  });

  it("should show", () => {
    const spyOnStoreShowAlert = jest.spyOn(store, "showAlert");
    service.show({ type: "info", message: "message" });
    expect(spyOnStoreShowAlert).toHaveBeenCalled();
  });

  it("should show with delay", () => {
    const spyOnStoreShowAlert = jest.spyOn(store, "showAlert");
    const spyOnStoreDismiss = jest.spyOn(store, "dismiss");

    let banner: AlertBanner = { type: "info", message: "message" };
    service.show(banner);
    expect(spyOnStoreShowAlert).toHaveBeenCalledWith(banner);
    timer(3000).subscribe(() => {
      expect(spyOnStoreDismiss).toHaveBeenCalledWith(banner);
    });

    banner = { ...banner, timeToDismissed: 1500 };
    service.show(banner);
    expect(spyOnStoreShowAlert).toHaveBeenCalledWith(banner);
    timer(1500).subscribe(() => {
      expect(spyOnStoreDismiss).toHaveBeenCalledWith(banner);
    });
  });

  it("should call dismiss on autoDismissable banners", (done) => {
    const spyOnStoreDismiss = jest.spyOn(store, "dismiss");
    const banner: AlertBanner = {
      type: "success",
      message: "Auto-dismissable message",
      autoDismissable: true,
      timeToDismissed: 1000,
    };

    service.show(banner);

    setTimeout(() => {
      expect(spyOnStoreDismiss).toHaveBeenCalledWith(banner);
      done();
    }, 1000);
  });

  it("should support banners with strong text", () => {
    const spyOnStoreShowAlert = jest.spyOn(store, "showAlert");
    const banner: AlertBanner = {
      type: "info",
      message: "Message with strong text",
      strong: "Important",
    };

    service.show(banner);

    expect(spyOnStoreShowAlert).toHaveBeenCalledWith(banner);
  });
});
