export interface RequestListProperties {
  email: string | undefined | null;
  status: string | undefined | null;
  page?: number;
  size?: number;
  sort?: Array<string>;
}
