export interface PagedParticipantDto {
  content: ParticipantDto[];
  page: Page;
}

export interface RequestDocument {
  id: string;
  content: string;
  documentTemplate: DocumentTemplate
}

export interface ParticipantDto {
  id: string;
  participantId?: string;
  participantType: {
    id: number,
    value:string;
    label: string;
  },
  organization: string;
  applicant: {
    username: string;
    firstName: string;
    lastName: string;
    email: string;
  };
  status: {
    id: string;
    value: string;
    label: string;
  };
  rejectionCause: string;
  onboardingStatusValue?: string;
  applicantRepresentative?: string;
  creationTimestamp: string;
  updateTimestamp: string;
  outcomeUserEmail: string;
  expiryDate: string;
  lastStatusUpdateTimestamp: string;
  expirationTimeframe: number;
  documents: Array<any>;
  comments: Array<any>;
}

export enum ParticipantStatus {
  INSERTED = "INSERTED",
  IN_PROGRESS = "IN_PROGRESS",
  APPROVED = "APPROVED",
  REJECTED = "REJECTED",
}

export interface Page {
  size: number;
  number: number;
  totalElements: number;
  totalPages: number;
}

export interface ParticipantType {
  id: string,
  value: string,
  label: string
}

export interface DocumentTemplate {
  id: string,
  name: string,
  description: string,
  mandatory: boolean,
  mimeType: MIMEType,
  creationTimestamp: string,
  updateTimestamp: string
}

export interface OnboardingProcedureTemplate {
  id?: string,
  participantType: string,
  description: string,
  creationTimestamp: string,
  updateTimestamp: string,
  documentTemplates: Array<DocumentTemplate>
  expirationTimeframe: number
}

export interface MIMEType {
  id?: number,
  value: string,
  description: string
}
