export interface NewUserResponse {
  username: string;
  password: string;
  loginLink: string;
}

export enum ParticipantTypeEnum {
  CONSUMER = "Consumer",
  APPLICATION_PROVIDER = "Application Provider",
  DATA_PROVIDER = "Data Provider",
  INFRASTRUCTURE_PROVIDER = "Infrastructure Provider",
}

export interface ApplicationRequestProperties {
  participantType: ParticipantTypeEnum;
  organization: string;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export enum OnboardingRequestStatus {
  IN_REVIEW = "In review",
  IN_PROGRESS = "In progress",
  APPROVED = "Approved",
  REJECTED = "Rejected",
}

export enum OnboardingRequestStatusEnum {
  IN_REVIEW = "IN_REVIEW",
  IN_PROGRESS = "IN_PROGRESS",
  APPROVED = "APPROVED",
  REJECTED = "REJECTED",
}
